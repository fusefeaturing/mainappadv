<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogCarTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ชนิดรถ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มชนิดรถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model)
                {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<h4>' . $model->name . ' </h4>';
                        $htmlcard .= '<p>ลำดับจัดเรียง : ' . $model->index_sort . '</p>';
                        $htmlcard .= '<p>ประเภทรถ : ' . $model->name . '</p>';
                        $htmlcard .= '<p>ลักษณะบรรทุก : ' . $model->description . '</p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        $htmlcard .= Html::img($model->photoViewer,['class'=>'img-responsive','style'=>'max-width:200px;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= '<div class="row">';
                        
                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('ดูรายละเอียดเพิ่มเติม', 
                        ['car-type/view', 'id' => $model->car_type_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('แก้ไข', 
                        ['update', 'id' => $model->car_type_id], 
                        ['class' => 'btn btn-primary glyphicon glyphicon-pencil', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('ลบ', ['car-type/delete', 'id' => $model->car_type_id], 
                        [
                            'class' => 'btn btn-danger glyphicon glyphicon-remove', 'style' => 'width: 100%;',
                            'data' => 
                            [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                        $htmlcard .= '</div>';

                        $htmlcard .= '</div>';
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]); ?>
</div>
