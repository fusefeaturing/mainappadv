<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogCarType */

$this->title = 'Update Rog Car Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rog Car Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->car_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rog-car-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
