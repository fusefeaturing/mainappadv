<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogCarType */

$this->title = 'Create Rog Car Type';
$this->params['breadcrumbs'][] = ['label' => 'Rog Car Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
