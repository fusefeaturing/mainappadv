<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RogCarType */

$this->title = $model->typeWheel->type_wheel_name;
$this->params['breadcrumbs'][] = ['label' => 'ประเภทรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_description']['content'] = $model->carDes->car_des_name;
Yii::$app->params['og_image']['content'] = $model->getPhotoViewer();
Yii::$app->params['og_url']['content'] = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<div class="rog-car-type-view">

<?php
    if(false) //adminaccess
    {
        echo '<p>';
        echo Html::a('Update', ['update', 'id' => $model->car_type_id], ['class' => 'btn btn-primary']);
        echo Html::a('Delete', ['delete', 'id' => $model->car_type_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]);
        echo '</p>';
    }
?>

    <div class="card2">
        <div class="card-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <h1><?= Html::img($model->photoViewer,['class'=>'img-thumbnail','style'=>'height:200px;'])?></h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>ประเภทรถ <strong><?= $model->typeWheel->type_wheel_name ?></strong></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>ลักษณะบรรทุก <strong><?= $model->carDes->car_des_name ?></strong></p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>ค่าขนส่งเฉลี่ยต่อกิโลเมตร <strong> <?= $model->fee?> </strong>บาท/กิโลเมตร</p>
                </div>


            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p></p>
    </div>

    <div class="card2">
        <div class="card-body">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a('ฉันมีรถขนส่งชนิดนี้ ให้บริการ', 
                ['car/my-car'], 
                ['class' => 'btn btn-info btn-block btn-lg', 'style' => '']) ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a('ต้องการใช้รถขนส่งชนิดนี้', 
                ['car/index', 'RogUserCarSearch[car_type][]' => $model->car_type_id], 
                ['class' => 'btn btn-warning btn-block btn-lg', 'style' => '']) ?>
            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <p></p>
    </div>


    <?php /* echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_type_id',
            'name',
            'description',
            'fee',
            'wheel',
            'picture',
            [
                'format'=>'raw',
                'attribute'=>'picture',
                'value'=>Html::img($model->photoViewer,['class'=>'img-thumbnail','style'=>'height:200px;'])
            ],
            'created_date',
            'updated_date',
        ],
    ])*/ ?>

</div>
