<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RogUserCar */

$this->title = $model->user->company_name;
Yii::$app->params['og_title']['content'] = $model->cardes->typewheel->type_wheel_name . ' - ' . $model->cardes->car_des_name;
Yii::$app->params['og_description']['content'] = $model->user->company_name . ' - ' . $model->user->tel_1;
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURLP();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$this->params['breadcrumbs'][] = ['label' => 'รถขนส่งให้บริการของฉัน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$carid = Yii::$app->getRequest()->getQueryParam('id');
//$userid = yii::$app->user->identity->id;
?>


<style>
    .contact {
        background-color: #fff;
        padding: 15px;
        max-width: 200px;
        margin: 30px auto;
    }

    .icon {
        width: 16px;
        height: 16px;
        padding: 0;
        margin: 0;
        vertical-align: middle;
    }
</style>


<div class="rog-user-car-view">


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <div class="">

        <div class="row">
            <div class="col-lg-9">

                <div class="card2">
                    <div class="card-body">
                        <div class="mt-4">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">

                                    <?php
                                    $imagesexplode = explode(",", $model->pic_car);
                                    $photosImage = $model->pic_car;

                                    foreach ($imagesexplode as $key => $loopImage) { ?>

                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>

                                    <?php } ?>

                                </ol>

                                <div class="carousel-inner">

                                    <?php $count = 0;

                                    foreach ($imagesexplode as $key => $loopImage) {
                                        $count++ ?>
                                        <div class="carousel-item <?= ($count == 1) ? 'active' : '' ?>">
                                            <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/' . $loopImage ?> " class=" img-fluid rounded mx-auto d-block gImg car-display-mobile">
                                        </div>

                                        <div id="myModal" class="modal">
                                            <span class="close">×</span>
                                            <img class="modal-content" id="img01">
                                            <div id="caption"></div>
                                        </div>
                                    <?php } ?>

                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p></p>
                            </div>

                            <div class="row" style="margin-left:10px;">
                                <!-- Your share button code -->
                                <div class="fb-share-button" style="margin-right:10px;" data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-layout="button_count" data-size="large">
                                    <a target="_blank" href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="fb-xfbml-parse-ignore">แชร์
                                    </a>
                                </div>

                                <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a" data-ver="3" data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?> data-color="default" data-size="large" style="display: none; ">
                                </div>

                                <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy " data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                                    คัดลอก URL <i class="fas fa-copy"></i>
                                </button>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p></p>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <?php

                                    echo '<p>';
                                    echo Html::a(
                                        '<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(),
                                        ($model->user->tel_1() != '')
                                            ? 'tel:' . $model->user->tel_1()
                                            : ['user/view', 'id' => $model->user_id],
                                        ['class' => 'btn btn-primary btn-block btn-sm rounded-pill']
                                    );
                                    //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                                    echo '</p>';

                                    ?>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->user->line_id ?>" class="wpfront-button text-center btn-block btn-sm rounded-pill btn-sm" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->user->line_id() ?></a></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->

                    </div>
                </div>



                <div class="my-4">

                    <div class="card2">
                        <div class="card-body">


                            <div class="col-xs-12 col-sm-12 col-md-12">

                                <h3><strong> รายละเอียดรถ</strong></h3>
                                <?= '<p><strong>ประเภทรถ</strong> : ' . $model->cardes->typewheel->type_wheel_name . '</p>'; ?>
                                <?= '<p><strong>ลักษณะบรรทุก</strong> : ' . $model->cardes->car_des_name . '</p>'; ?>
                                <?= '<p><strong>คำอธิบายรถ</strong> : ' . nl2br($model->car_explanation) . '</p>'; ?>
                            </div>
                            <!--<div class="col-xs-12 col-sm-6 col-md-6">
            <p><strong>เบอร์โทรศัพท์</strong> : <?= $model->user->tel_1 ?></p>
        </div>-->

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>ทะเบียนรถ</strong> : <?= $model->plate_number ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>ทะเบียนจังหวัด</strong> : <?= $model->plate() ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>ยี่ห้อ</strong></strong> : <?= $model->manufacturer_txt ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>สี</strong> : <?= Html::button('', ['class' => 'btn btn-default btn-lg', 'style' => 'background-color:' . $model->color . ';']) ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>น้ำหนักบรรทุก (ตัน)</strong> : <?= $model->carry_weight ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>ประกันภัยสินค้า</strong> : <?= ($model->is_insured == 1) ? 'มี' : 'ไม่มี' ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>รายละเอียดการให้บริการ</strong> : <?= $model->car_services ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>เงื่อนไขการให้บริการ เช่น การขึ้น/ลงสินค้า และ ราคา</strong> : <?= $model->car_service_tos ?></p>
                            </div>


                            <div class="row" style="margin-left:10px;">
                                <!-- Your share button code -->
                                <div class="fb-share-button" style="margin-right:10px;" data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-layout="button_count" data-size="large">
                                    <a target="_blank" href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="fb-xfbml-parse-ignore">แชร์
                                    </a>
                                </div>

                                <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a" data-ver="3" data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?> data-color="default" data-size="large" style="display: none; ">
                                </div>

                                <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                                    คัดลอก URL <i class="fas fa-copy"></i>
                                </button>
                            </div>




                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p>

                        </p>
                    </div>


                    <div class="card2">
                        <div class="card-body">


                            <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <?php
                                    $imagesexplodeuser = explode(",", $model->user->company_pic);
                                    $photosImageuser = $model->user->company_pic;

                                    foreach ($imagesexplodeuser as $key => $loopImage) { ?>
                                        <li data-target="#carouselExampleIndicators1" data-slide-to="0"></li>
                                    <?php }
                                    ?>
                                </ol>

                                <div class="carousel-inner">

                                    <?php $count = 0;

                                    foreach ($imagesexplodeuser as $key => $loopImagee) {
                                        $count++ ?>
                                        <div class="carousel-item <?= ($count == 1) ? 'active' : '' ?>">
                                            <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/' . $loopImagee ?> " class=" img-thumbnail rounded mx-auto d-block gImg commany-display-mobile">
                                        </div>

                                        <div id="myModal" class="modal">
                                            <span class="close">×</span>
                                            <img class="modal-content" id="img01">
                                            <div id="caption"></div>
                                        </div>
                                    <?php } ?>

                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p>
                                    <h4><strong><?= $model->user->company_name ?></strong> </h4>
                                </p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>คำอธิบายกิจการ</strong> : <?= nl2br($model->user->company_explanation) ?></p>
                            </div>

                            <div class="row">

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <?= Html::beginForm(['car/carcompany', 'id' => $model->user->user_id], 'POST'); ?>
                                        <?= Html::hiddenInput('user_id', $model->user->user_id); ?>
                                        <div class="form-group">
                                            <?= Html::submitButton('ดูกิจการและรถขนส่งทั้งหมด', ['class' => 'btn btn-primary btn-block btn-sm']); ?>
                                        </div>
                                        <?= Html::endForm(); ?>

                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <?php

                                    echo '<p>';
                                    echo Html::a(
                                        '<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(),
                                        ($model->user->tel_1() != '')
                                            ? 'tel:' . $model->user->tel_1()
                                            : ['user/view', 'id' => $model->user_id],
                                        ['class' => 'btn btn-primary btn-block rounded-pill btn-sm']
                                    );
                                    //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                                    echo '</p>';

                                    ?>
                                </div>


                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->user->line_id ?>" class="wpfront-button text-center btn-sm rounded-pill btn-sm btn-block" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->user->line_id() ?></a></p>
                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>Facebook Fanpage</strong> :
                                    <br>
                                    <div class="fb-page " data-href="<?= $model->user->facebook_token ?>" data-tabs="" data-width="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                        <blockquote cite="<?= $model->user->facebook_token ?>" class="fb-xfbml-parse-ignore">
                                            <a href="<?= $model->user->facebook_token ?>">
                                                Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                            </a>
                                        </blockquote>
                                    </div>
                                </p>
                            </div>



                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>Email</strong> : <?= $model->user->email_contact ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>Website</strong> : <?= $model->user->website ?></p>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <p><strong>ที่อยู่กิจการ</strong> : ต. <?= $model->user->district() ?> อ. <?= $model->user->deliver() ?> จ. <?= $model->user->pickup() ?> <?= $model->user->zipcode() ?></p>
                            </div>



                            <!--<div class="col-xs-12 col-sm-12 col-md-12" >

               <?= '' /*Html::a(
'<i class="fab fa-facebook-square"></i> แฟนเพจ Rogistic.com',
'https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC',
['class' => 'btn btn-primary  btn-block btn-lg']

)*/
                ?>

            </div>-->

                        </div>
                    </div>
                </div>
                <!-- /.card -->

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>

                <div class="card2 d-none d-sm-block d-md-none d-lg-block">
                    <div class="card-body">
                        <div align="center">
                            <div class="fb-group" style="margin-bottom:10px;" data-href="https://www.facebook.com/groups/376534166106364/" data-width="280" data-show-social-context="true" data-show-metadata="true">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>

            </div>
            <!-- /.col-lg-9 -->

            <div class="col-lg-3">

                <div class="card2">
                    <div class="card-body">

                        <h3 class="my-4"></h3>

                        <div class="list-group">
                            <div class="d-lg-none d-md-none d-sm-none">
                                <?php
                                if ($owner) {
                                    echo '<div class="">';

                                    echo '<div class="">';
                                    echo '<div class="form-group">';
                                    echo Html::a('<i class="fas fa-edit"></i> แก้ไข', ['update', 'id' => $model->car_id], ['class' => 'btn btn-primary btn-block']);
                                    echo '</div>';
                                    echo '</div>';

                                    echo '<div class="">';
                                    echo '<div class="form-group">';
                                    echo Html::a('<i class="fas fa-trash-alt"></i> ลบ', ['delete', 'id' => $model->car_id], [
                                        'class' => 'btn btn-danger btn-block',
                                        'data' => [
                                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                            'method' => 'post',
                                        ],
                                    ]);

                                    echo '</div>';
                                    echo '</div>';

                                    echo '</div>';
                                }
                                ?>

                                <?= Html::a('<i class="fas fa-th-list"></i> ดูกิจการทั้งหมด', ['car/carcompany', 'id' => $model->user->user_id], ['class' => 'btn btn-primary btn-block']); ?>

                            </div>

                            <div class="d-none d-sm-block">

                                <?php
                                if ($owner) {
                                    echo '<div class="">';

                                    echo '<div class="">';
                                    echo '<div class="list-group-item">';
                                    echo Html::a('<i class="fas fa-edit"></i> แก้ไข', ['update', 'id' => $model->car_id], ['class' => '', 'style' => 'text-decoration: none;']);
                                    echo '</div>';
                                    echo '</div>';

                                    echo '<div class="">';
                                    echo '<div class="list-group-item">';
                                    echo Html::a('<i class="fas fa-trash-alt"></i> ลบ', ['delete', 'id' => $model->car_id], [
                                        'class' => '',
                                        'style' => 'text-decoration: none;',
                                        'data' => [
                                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                            'method' => 'post',
                                        ],
                                    ]);

                                    echo '</div>';
                                    echo '</div>';

                                    echo '</div>';
                                }
                                ?>

                                <?= Html::a('<i class="fas fa-th-list"></i> ดูกิจการทั้งหมด', ['car/carcompany', 'id' => $model->user->user_id], ['class' => 'list-group-item', 'style' => 'text-decoration: none;']); ?>
                            </div>
                        </div>

                        <div class="" style="padding-top: 10px;">
                            <?php

                            echo '<p>';
                            echo Html::a(
                                '<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(),
                                ($model->user->tel_1() != '')
                                    ? 'tel:' . $model->user->tel_1()
                                    : ['user/view', 'id' => $model->user_id],
                                ['class' => 'btn btn-primary btn-block rounded-pill btn-sm']
                            );
                            //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                            echo '</p>';

                            ?>
                        </div>


                        <div class="">
                            <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->user->line_id ?>" class="wpfront-button text-center btn-sm rounded-pill btn-sm btn-block" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->user->line_id() ?></a></p>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p></p>
                        </div>

                        <div class=" d-lg-none d-xl-block " align="center">
                            <div class="fb-group d-xl-none" style="margin-top:10px;" data-href="https://www.facebook.com/groups/376534166106364/" data-width="250" data-show-social-context="true" data-show-metadata="true">
                            </div>
                        </div>

                    </div>
                    <!-- /.col-lg-3 -->

                </div>


            </div>

        </div>

    </div>
    <!-- /.container -->

    <script>
        // COPY TO CLIPBOARD
        // Attempts to use .execCommand('copy') on a created text field
        // Falls back to a selectable alert if not supported
        // Attempts to display status in Bootstrap tooltip
        // ------------------------------------------------------------------------------

        function copyToClipboard(text, el) {
            var copyTest = document.queryCommandSupported('copy');
            var elOriginalText = el.attr('data-original-title');

            if (copyTest === true) {
                var copyTextArea = document.createElement("textarea");
                copyTextArea.value = text;
                document.body.appendChild(copyTextArea);
                copyTextArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'Copied!' : 'Whoops, not copied!';
                    el.attr('data-original-title', msg).tooltip('show');
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(copyTextArea);
                el.attr('data-original-title', elOriginalText);
            } else {
                // Fallback if browser doesn't support .execCommand('copy')
                window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
            }
        }

        $(document).ready(function() {
            // Initialize
            // ---------------------------------------------------------------------

            // Tooltips
            // Requires Bootstrap 3 for functionality
            $('.js-tooltip').tooltip();

            // Copy to clipboard
            // Grab any text in the attribute 'data-copy' and pass it to the
            // copy function
            $('.js-copy').click(function() {
                var text = $(this).attr('data-copy');
                var el = $(this);
                copyToClipboard(text, el);
            });
        });
    </script>

</div>