<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->user->company_name;
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_description']['content'] = $model->company_explanation;
Yii::$app->params['og_image']['content'] = $model->user->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<div class="rog-user-car-index">

        <div class="card2">
            <div class="card-body">



        <?php          
              if($owner)
              {     
                echo '<div class="row">';

                echo '<div class="col-lg-2 col-md-2 col-sm-2 col-sx-2">';
                echo '<div class="form-group">';
                echo Html::a('แก้ไข', ['user/update-service', 'id' => $model->user_id], ['class' => 'btn btn-primary btn-block']); 
                echo '</div>';
                echo '</div>';

                echo '</div>';
                  
                  
                  
              }
        ?>



            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">

                        <?php
                            $imagesexplode = explode(",", $model->company_pic);
                            $photosImage = $model->company_pic;

                        foreach ($imagesexplode as $key => $loopImage) { ?>

                                <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>

                        <?php } ?>
                        
                </ol>

                <div class="carousel-inner">

                    <?php $count=0;

                    foreach ($imagesexplode as $key => $loopImage) { $count++ ?>
                        <div class="carousel-item <?= ($count==1)?'active':'' ?>">
                            <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/'. $loopImage ?> " 
                            class=" img-thumbnail rounded mx-auto d-block gImg commany-display-mobile">
                        </div>

                        <div id="myModal" class="modal">
                            <span class="close">×</span>
                                <img class="modal-content" id="img01">
                            <div id="caption"></div>
                        </div>
                    <?php } ?>
                    
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>

            </div>
        </div>    
        
        <div class="card2">
            <div class="card-body">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><h4><strong><?=$model->company_name ?></strong></h4></p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>คำอธิบายกิจการ</strong> : <?= nl2br($model->company_explanation) ?></p>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?php

                    echo '<p>';
                    echo Html::a('<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(), 
                    ($model->user->tel_1() != '') 
                    ? 'tel:'.$model->user->tel_1() 
                    : ['user/view', 'id' => $model->user_id] , ['class' => 'btn btn-primary btn-sm btn-block rounded-pill']);
                    //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                    echo '</p>';             
                        
                ?>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4">
                <p><a href="http://line.me/ti/p/~<?=$model->user->line_id ?>" class="wpfront-button text-center btn-sm rounded-pill btn-block btn-sm" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->user->line_id() ?></a></p>
            </div>   
        </div>

            <div div class="col-xs-12 col-sm-12 col-md-12" > 
                <p><strong>Facebook Fanpage</strong> : 
                        <br>
                    <div class="fb-page "  
                            data-href="<?= $model->user->facebook_token ?>" 
                            data-tabs="" 
                            data-width="280"         
                            data-small-header="false" 
                            data-adapt-container-width="false" 
                            data-hide-cover="false" 
                            data-show-facepile="true">
                            <blockquote cite="<?= $model->user->facebook_token ?>" 
                            class="fb-xfbml-parse-ignore">
                                <a href="<?= $model->user->facebook_token ?>">
                                    Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                </a>
                            </blockquote>
                    </div>
                </p>
            </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Email</strong> : <?=$model->user->email_contact ?></p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Website</strong> : <?=$model->user->website ?></p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ที่อยู่กิจการ</strong> : ต. <?= $model->user->district() ?> อ. <?= $model->user->deliver() ?> จ. <?= $model->user->pickup() ?> <?= $model->user->zipcode() ?></p>
        </div>

        <div class="row" style="margin-left:10px;">   
            <!-- Your share button code -->
            <div class="fb-share-button" style="padding-right:10px;" 
                data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                data-layout="button_count" 
                data-size="large">
                    <a target="_blank" href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                        class="fb-xfbml-parse-ignore">แชร์
                    </a>
            </div>
            
            <div class="line-it-button" 
                data-lang="th" 
                data-type="share-a" 
                data-ver="3" 
                data-url=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                data-color="default" 
                data-size="large"     
                style="display: none; ">
            </div>

            <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
            คัดลอก URL <i class="fas fa-copy"></i>
            </button>

        </div>

        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <p></p>
    </div>
    
    <div class="container">
        <div class=" row row-cols-1 row-cols-md-3">
                
          <?php foreach ($modelsLimits as $model) { ?>
        
            <?= Html::a('<div class="card-group">
                              <div class="card card-mobile" >
                                <div class="cardd"> 
                                ' . Html::img('../'. ($model->getFirstPhotoURLP()), ['class' => 'img-responsive rounded mx-auto d-block car-display-loop']) . '                      
                                </div>        
                                <div class="card-body card-body-list-car">
                                  <h6 class="card-title">' . $model->cardes->typewheel->type_wheel_name . ' ' . $model->cardes() . ' </h6>
                                  <p class="card-text"> ' . $model->user->company_name . ' </p>
                                  <p class="card-text"><i class="fas fa-map-marker-alt"></i> ' . $model->user->pickup() . ' ' . $model->user->deliver() . ' </p>
                                </div>       
                              </div>
                            </div>', ['car/view', 'id' => $model->car_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
            ?>
    
          <?php } ?>
          
        </div>
        <br>
    </div>

    <?php if (!empty($modelsLimits)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่พบข้อมูล !!"; ?>
                </div>
    <?php endif; ?>





    <div class="card2">
        <div class="card-body">



    <div class="" >

        <!--<div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <p>เพื่อให้รถขนส่งติดต่อมาหาคุณ</p>
        </div>-->

        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <?='' /*Html::a(
                'ลงประกาศ "หารถขนส่ง"',
                ['job/create'],
                ['class' => 'btn btn-info btn-block btn-lg', 'style' => '']
            )*/ ?>
        </div>-->
        

        <!--<div class="col-xs-12 col-sm-12 col-md-12" > 
               
               <?='' /*Html::a(
                   '<i class="fab fa-facebook-square"></i> แฟนเพจ Rogistic.com',
                   'https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC',
                   ['class' => 'btn btn-primary  btn-block btn-lg']
   
                   )*/
               ?>
   
       </div>

       <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

       <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 100px;">                
               <?='' /*Html::a(
                   '<i class="fab fa-facebook-square"></i> เข้าร่วมกลุ่ม',
                   'https://www.facebook.com/groups/376534166106364/',
                   ['class' => 'btn btn-primary  btn-block btn-lg']
   
                   )*/
               ?>   
       </div>-->

            <div align="center">
                <div class="fb-group" style="margin-bottom:10px;" 
                    data-href="https://www.facebook.com/groups/376534166106364/" 
                    data-width="280" 
                    data-show-social-context="true" 
                    data-show-metadata="true"
                    data-adapt-container-width="true">
                    
                </div> 
            </div>

    </div>

        </div>
    </div>


    <script>

    // COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
  var copyTest = document.queryCommandSupported('copy');
  var elOriginalText = el.attr('data-original-title');

  if (copyTest === true) {
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = text;
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'Copied!' : 'Whoops, not copied!';
      el.attr('data-original-title', msg).tooltip('show');
    } catch (err) {
      console.log('Oops, unable to copy');
    }
    document.body.removeChild(copyTextArea);
    el.attr('data-original-title', elOriginalText);
  } else {
    // Fallback if browser doesn't support .execCommand('copy')
    window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
  }
}

$(document).ready(function() {
  // Initialize
  // ---------------------------------------------------------------------

  // Tooltips
  // Requires Bootstrap 3 for functionality
  $('.js-tooltip').tooltip();

  // Copy to clipboard
  // Grab any text in the attribute 'data-copy' and pass it to the 
  // copy function
  $('.js-copy').click(function() {
    var text = $(this).attr('data-copy');
    var el = $(this);
    copyToClipboard(text, el);
  });
});
</script>

</div>