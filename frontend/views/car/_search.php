<?php

use common\models\Amphures;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Provinces;
use common\models\RogCarDescription;
use common\models\RogTypeWheel;
use yii\helpers\Url;

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\RogUserCarSearch */
/* @var $form yii\widgets\ActiveForm */
$typewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$rogtypewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$rogcardes = ArrayHelper::map(RogCarDescription::find()->asArray()->all(), 'car_des_id', 'car_des_name');
$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
$dprovinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
$amphures = ArrayHelper::map(Amphures::find()->asArray()->all(), 'id', 'name_th');
$carsearch = $this->context->getCarSearch();

?>


<script type="text/javascript">
    $(document).ready(function() {

        function showTextStatus(status) {
            return status == 'true' ? "(แตะเพื่อย่อ)" : "(แตะเพื่อขยาย)";
        }

        function loopCheckedValue() {
            var selectedLanguage = []
            $('input[type="checkbox"]:checked').each(function() {
                

                var text = $('span#boxitem' + this.value).html();
                selectedLanguage.push(text);
            });
            return selectedLanguage;
        }

        function showCollapsHead(selected, textStat) {
            $('#divResult').html(selected.length + "\n" + "รายการ / ประเภทรถที่เลือก: " + selected + textStat);
        }

        function event(type) {
            var stat = $("#divResult[aria-expanded]").attr('aria-expanded') == "true";
            var stringStat = type == "exp" ? showTextStatus(""+!stat+"") : showTextStatus(""+stat+"");
            var selected = loopCheckedValue();
            
            if (type == "init") {
                if (selected.length != 0)  { showCollapsHead(selected, stringStat); } 
                else { return; }
            } else { showCollapsHead(selected, stringStat); }  
        }

        $("#divResult[aria-expanded]").click(function() {
            event("exp")
        });

        $('input[type="checkbox"]').click(function() {
            event("chk")
        });
        event("init")
        
    });

</script>






<div class="rog-user-car-search">

    <?php

    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>



    <!--<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div id="w1" class="panel-group collapse in" aria-expanded="true" style="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <a class="collapse-toggle" href="#w1-collapse1" data-toggle="collapse" data-parent="#w1" aria-expanded="false"  class="divResult" id="divResult">เลือกประเภทรถขนส่ง (แตะเพื่อขยาย)</a>


                        </h4>
                    </div>

                    <div id="w1-collapse1" class="panel-collapse collapse" style="">
                        <div class="panel-body" id="divR"> 
                            <?=

                                $form
                                    ->field($model, 'car_type')
                                    ->checkBoxList(
                                        $this->context->getCarTypes(),
                                        [
                                            'separator' => '<br>',
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $checked = $checked ? 'checked' : '';
                                                return '<div> <span id="boxitem' . $value . '"> <input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</span></div>';
                                            }
                                        ]
                                    )
                                    ->hint(Html::a('คลิกดูประเภทรถ', ['car-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                                    ->label(false)
                                    ->hint('เลือกได้หลายรายการ')

                            ?>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>-->


    <div class="card2"> 

        <h1 style="padding-left:20px; padding-top:20px;">รถร่วมขนส่ง</h1>    

        <div class="row card-body">     


            <div class="col-md-4">  

            <?=     
                    $form
                        ->field($model, 'car_des')
                        ->widget(Select2::classname(), [
                            'data' => $carsearch,
                            'model' => $model,
                            'attribute' => 'car_des',
                            'language' => 'th',
                            'options' => ['placeholder' => 'เลือกประเภทรถ'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                //'multiple' => true,
                            ],                    
                    ])    
                    ->label(false);                  
                ?>
            </div>  
                        

            <!--<div class="col-md-6">
                <?=
                    $form
                        ->field($model, 'type_wheel')
                        ->label('ชนิดรถ', ['class' => ''])
                        ->dropdownList($this->context->getwheelTypes(), [
                            'id' => 'typewheel',
                            'prompt' => 'เลือกชนิดรถ'
                        ])
                ?>
            </div>  

            <div class="col-md-6">
                <?=
                    $form
                        ->field($model, 'car_des')
                        ->widget(DepDrop::classname(), [
                            'options' => ['id' => 'cardes'],
                            'data' => $cardes,
                            'pluginOptions' => [
                                'depends' => ['typewheel'],
                                'placeholder' => 'เลือกทั้งหมด',
                                'url' => Url::to(['/car/get-cardes'])
                            ]
                        ]);
                ?>
            </div>   -->    

                <div class="col-md-4">
                    <?php echo $form
                        ->field($model, 'pickup_province')
                        ->dropdownList($provinces, ['prompt' => 'เลือกจังหวัดต้นทาง']) 
                        ->label(false);
                        ?>
                </div>  

                <div class="col-md-4">
                    <?php echo $form
                        ->field($model, 'deliver_province')
                        ->dropdownList($dprovinces, ['prompt' => 'เลือกจังหวัดปลายทาง']) 
                        ->label(false);
                        ?>
                </div>  
                            
                            
                            

            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>    

                </div>
            </div>  

            <div class="col-sm-6">
                <div class="form-group">
                            
                    <?= Html::a(
                        'รีเซ็ตค่าค้นหา',
                        ['car/index'],
                        ['class' => 'btn btn-danger btn-block', 'style' => '']
                    ) ?>
                </div>
            </div>
                        <hr>    
                    

                <?php ActiveForm::end();
                ?>  

            <!--<div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="form-group">
                    <?='' /*Html::a(
                        'ลงประกาศ "หารถขนส่ง"',
                        ['job/create'],
                        ['class' => 'btn btn-info btn-block btn-lg', 'style' => '']
                    ) */?>
                </div>
            </div>-->   
                    

            </div>  

        </div>
</div>
