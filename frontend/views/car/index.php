<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รถขนส่งพร้อมบริการ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ลงทะเบียนรถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'car_id',
            //'user_id',
            //'car_type',
            [
                'attribute'=>'car_type',
                'value'=> function($model) use ($cartypes)
                {
                    return (array_key_exists($model->car_type, $cartypes)) ? $cartypes[$model->car_type] : 'ประเภทรถไม่ถูกต้อง';
                },
            ],
            'plate_number',
            'carry_weight',
            //'color',
            //'manufacturer',
            //'chassis_number',
            //'engine_number',
            //'created_date',
            //'updated_date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
