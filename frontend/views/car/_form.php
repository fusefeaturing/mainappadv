<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\RogCarType;
use kartik\color\ColorInput;
use common\models\Provinces;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use common\models\RogTypeWheel;
use kartik\select2\Select2;
use yii\helpers\Url;

$rogcartype = ArrayHelper::map(RogCarType::find()->asArray()->all(), 'car_type_id', 'name');
$typewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$provinces = ArrayHelper::map(Provinces::find()->orderBy('name_th')->asArray()->all(), 'id', 'name_th');
$car_des = $this->context->getCarSearch();
$id = yii::$app->user->identity->id;
/*
$listData = [];
 $options['options' => []];$form->field($model, 'car_type')->dropDownList($listData,$options)
$options[] = ['options' => []];

$fit_list = RogCarType::find()->all();

foreach($fit_list as $item)
{
    $listData[$item['car_type_id']] = 
    ($item['name'] == $item['description']) 
        ? $item['name'] 
        : $item['name'].' - '.$item['description'];
    
    $options['options'][$item['car_type_id']] = ['data-image' => $item->photoViewer];

$form->field($model, 'chassis_number')->textInput(['maxlength' => true])
$form->field($model, 'engine_number')->textInput(['maxlength' => true])

}*/
?>

<div class="rog-user-car-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card2">
        <div class="card-body">

        <?= 
            $form
                ->field($model, 'car_search_level')
                ->hiddenInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                //->input('', ['placeholder' => ""])
                ->label(false) 
        ?>

        <?= $form->field($model, 'user_id')
            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
            ->label(false)
        ?>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?=     
                    $form
                        ->field($model, 'car_des')
                        ->widget(Select2::classname(), [
                            'data' => $car_des,
                            'model' => $model,
                            'attribute' => 'car_des',
                            'language' => 'th',
                            'options' => ['placeholder' => 'เลือกประเภทรถ'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                //'multiple' => true,
                            ],                    
                    ])    
                    ->label('เลือกประเภทรถ', ['class' => '', 'style' => 'color:red'])         
                ?>
            </div>
                        
                        
            <!--<div class="col-xs-12 col-sm-6 col-md-4">
                    <?=''
                        /*$form
                            ->field($model, 'type_wheel')
                            ->label('ชนิดรถ')
                            ->dropdownList($typewheel, [
                                'id' => 'ddl-type_wheel',
                                'prompt' => 'เลือกชนิดรถ'
                            ])
                    */?>
                </div>
                            
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <?=''
                        /*$form
                            ->field($model, 'car_des')
                            ->widget(DepDrop::classname(), [
                                'options' => ['id' => 'ddl-cardes'],
                                'data' => $cardes,
                                'pluginOptions' => [
                                    'depends' => ['ddl-type_wheel'],
                                    'placeholder' => 'เลือกรายละเอียด...',
                                    'url' => Url::to(['/car/get-cardes'])
                                ]
                            ]);
                    */?>
            </div>-->
                                
        </div>



        <?= $form->field($model, 'pic_car[]')
            ->fileInput(['multiple' => true])
            ->label('รูปภาพรถหน้าเฉียง หลังเฉียงของฉัน  (เลือกได้ 2 ภาพเท่านั้น)', ['class' => '', 'style' => 'color:red'])
             ?>
        <div class="well">
            <?= $model->getPhotosViewerUpdate(); ?>
        </div>

        <br>
                
        <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-12">
        <?= $form->field($model, 'car_explanation')->textarea(['rows' => 7]) ?>
        </div>
        </div>          

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?= $form->field($model, 'plate_number')->textInput(['maxlength' => true])
                   ->textInput()->input('', ['placeholder' => "ตัวอย่าง กข-1234, 80-1234"]);  ?>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">
                <?= $form->field($model, 'plate_province')->textInput(['maxlength' => true]) 
                ->dropdownList($provinces, [                
                    'prompt' => 'เลือกจังหวัด'
                ])
                ?>
            </div>       
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'manufacturer_txt')
                        ->textInput(['maxlength' => true])
                ?>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'color')
                        ->widget(ColorInput::classname(), [
                            'options' =>
                            ['placeholder' => 'Select color ...'],
                        ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?= $form->field($model, 'carry_weight')->textInput(['maxlength' => true])
                    ->hint('หน่วยเป็นตัน (1 ตัน = 1000 กิโลกรัม)')
                    ->label('น้ำหนักบรรทุก (ตัน)', ['class' => '', 'style' => 'color:red'])  ?>
            </div>
        </div>

        <hr>

        <h3>ประกันภัยสินค้า</h3>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

                <?php
                if (!$model->is_insured) $model->is_insured = 0;
                ?>
                <?=
                    $form
                        ->field($model, 'is_insured')
                        ->radioList([0 => 'ไม่มีประกันภัยสินค้า', 1 => 'มีประกันภัยสินค้า'])
                        ->label(false)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'insurance_company')
                        ->textInput(['maxlength' => true])
                ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'insured_amount')
                        ->textInput(['maxlength' => true])
                ?>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'car_services')
                        ->textarea(['maxlength' => true, 'rows' => 6])
                        ->hint('เช่น ลำเลียงสินค้าลง')
                ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?=
                    $form
                        ->field($model, 'car_service_tos')
                        ->textarea(['maxlength' => true, 'rows' => 6])
                //->hint('') 
                ?>
            </div>
        </div>

        <?=
            $form
                ->field($model, 'created_date')
                ->hiddenInput([
                    'readonly' => true,
                    'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
                ])
                ->label(false)
        ?>

        <?=
            $form
                ->field($model, 'updated_date')
                ->hiddenInput([
                    'readonly' => true,
                    'value' => date('Y-m-d H:i:s')
                ])
                ->label(false)
        ?>

        <div class="form-group">
            <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
        </div>
        
    </div>
</div>
        

    <?php ActiveForm::end(); ?>

</div>