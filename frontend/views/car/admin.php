<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin Cars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'car_id',
            'user_id',
            [
                'attribute'=>'car_type',
                'value'=> function($model) use ($cartypes)
                {
                    return (array_key_exists($model->car_type, $cartypes)) ? $cartypes[$model->car_type] : 'ประเภทรถไม่ถูกต้อง';
                },
            ],
            'plate_number',
            'plate_province',
            'carry_weight',
            'manufacturer_txt',
            'is_insured',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
            ],
        ],
    ]); ?>
</div>
