<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogUserCar */

$this->title = 'แก้ไขข้อมูลรถขนส่ง: ' . $model->cardes->typewheel->type_wheel_name . ' - ' . $model->cardes->car_des_name;
$this->params['breadcrumbs'][] = ['label' => 'รถขนส่งให้บริการของฉัน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cardes->typewheel->type_wheel_name . ' - ' . $model->cardes->car_des_name, 'url' => ['view', 'id' => $model->car_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-car-update">

    <div class="card2">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'cartypes' => $cartypes,
       
        'cardes' => $cardes,
    ]) ?>

</div>
