<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$id= yii::$app->user->identity->id;
?>

<div class="rog-car-post-form">

<div class="card2">
    <div class="card-body">
        <h1>เลือกพื้นที่ให้บริการ</h1>
    <?= Html::beginForm(['/car/add-route' ,'id' => $id], 'POST'); ?>
    <?= Html::hiddenInput('step', 'provinceselector'); ?>
    <?= Html::hiddenInput('user_id', $id);  ?>
    <?= Html::checkboxList('selectedprovinces', $selectedprovinces, $provinces, ['class' => 'test']) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']); ?>
    </div>
    <?= Html::endForm(); ?>

    </div>
</div>

</div>