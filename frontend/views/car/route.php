<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\RogUserCar */

$this->title = 'เส้นทางที่ให้บริการ';
$this->params['breadcrumbs'][] = ['label' => 'รถขนส่งที่ให้บริการ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-user-car-index">

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a('เพิ่มข้อมูล', ['car/add-route'], ['class' => 'btn btn-success']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute'=>'province',
            'value'=> function($model) use ($provinces)
            {
                return (array_key_exists($model->province, $provinces)) ? $provinces[$model->province] : 'จังหวัดไม่ถูกต้อง';
            },
        ],
        [
            'attribute'=>'amphur',
            'value'=> function($model) use ($amphures)
            {
                return (array_key_exists($model->amphur, $amphures)) ? $amphures[$model->amphur] : 'อำเภอไม่ถูกต้อง';
            },
        ],
    ],
]); ?>

</div>
