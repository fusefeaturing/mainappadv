<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserCar */

$this->title = 'เพิ่มข้อมูล';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการรถของผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-car-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_search', [
        'model' => $model,
        'cartypes' => $cartypes,
        'amphur' => [],
        'cardes' => [],
    ]) ?>

</div>
