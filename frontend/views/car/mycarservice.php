<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รถขนส่ง และ เส้นทางที่ให้บริการ';
$userid = yii::$app->user->identity->id;
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rog-user-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>1.) เพิ่มรถขนส่งของฉัน</p>
    <p>2.) ระบุเส้นทางที่ฉันให้บริการ</p>
    <p>3.) <?= Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
    <p>**ระบุข้อมูลครบถ้วนทั้ง 3 รายการ เพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง**</p>

    <hr>
    
    <h2>1.) รถขนส่งที่ให้บริการ</h2>

    <p>
        <?= Html::a('+ เพิ่มรถขนส่ง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($modelsLimits as $model) { ?>
        
        <?= Html::a('<div class="card-group">
           
                          <div class="card card-mobile" >
                            <div class="cardd"> 
                            ' . Html::img($model->getPhotoViewerSide(), ['class' => 'img-responsive rounded mx-auto d-block car-display-mobile']) . '                      
                            </div>

                            <div class="card-body">
                                  <h6 class="card-title">' . $model->cardes->typewheel->type_wheel_name . ' ' . $model->cardes() . ' </h6>
                                  <p class="card-text" style="font-size:12px;"><i class="fas fa-map-marker-alt"></i> ' . $model->pickup() . ' ' . $model->deliver() . ' </p>
                                </div>  

                          </div>
                        </div>', ['car/view', 'id' => $model->car_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>

    </div>
    
  </div>

  <?php if (!empty($modelsLimits)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>

    <?='' /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) use ($cartypes)
                {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<h4><strong>' .$model->typewheel() .' - ' . $model->cardes() . '</strong></h4>';
                        //$htmlcard .= '<h4>' . ((array_key_exists($model->car_type, $cartypes)) ? $cartypes[$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . ' </h4>';
                        $htmlcard .= '<p>ทะเบียนรถ : ' . $model->plate_number . '</p>';
                        //$htmlcard .= '<p>ประเภทรถ : ' . ((array_key_exists($model->car_type, $cartypes)) ? $cartypes[$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . '</p>';
                        
                        $htmlcard .= '<p>ประเภทรถ : ' .$model->typewheel() . '</p>';
                        $htmlcard .= '<p>ลักษณะบรรทุก : ' . $model->cardes() . '</p>';
                        $htmlcard .= '<p>น้ำหนักบรรทุก : ' . $model->carry_weight . ' ตัน</p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        $htmlcard .= Html::img($model->getPhotoViewerSide(),['class'=>'img-responsive','style'=>'max-width:200px;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= Html::a('ดูข้อมูล', 
                        ['car/view', 'id' => $model->car_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]);*/
    ?>

    <hr>

    <h2>2.) พื้นที่ให้บริการ</h2>
    <p>
        <?= Html::a('+ ระบุเส้นทางที่ฉันให้บริการ', ['car/add-route'], ['class' => 'btn btn-success']) ?>
    </p>

<?php

$provtran = [];
foreach($serviceroutes as $key => $servicearea)
{
    $provtran[$provinces[$servicearea]][$key] = $amphures[$key];
}

foreach($provtran as $key => $protran)
{
    echo '<h4>' . $key . '</h4>';
    echo '<p>';
    foreach($provtran[$key] as $distran)
    {
        echo $distran . ' ';
    }
    echo '</p>';
}
?>

</div>
