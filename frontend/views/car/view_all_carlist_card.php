<?php

use Symfony\Component\Console\Helper\HelperSet;
use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รถร่วมขนส่ง';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

?>

<div class="rog-user-car-index">


    <?php echo $this->render('_search', ['model' => $searchModel, 'cardes' => $cardes]); ?>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p> </p>
    </div>

    <div class="container">
        <div class=" row row-cols-1 row-cols-md-3 ">
            <?php foreach ($dataProvider->models as $model) { ?>
                <?= Html::a('<div class="card-group h-100">
                          <div class="card card-loop" >
                            <div class="cardd" > 
                            ' . Html::img($model->getFirstPhotoURLP(), ['class' => 'img-responsive rounded mx-auto d-block car-display-loop']) . '                      
                            </div>
                            <div class="card-body card-body-list-car">
                                <h6 class="card-title" >' . $model->cardes->typewheel->type_wheel_name . ' ' . $model->cardes() . ' </h6>
                                <p class="card-text"> ' . $model->user->company_name . '' . ' </p>
                                <p class="card-text"><i class="fas fa-map-marker-alt"></i> ' . $model->user->pickup() . ' ' . $model->user->deliver() . ' </p>
                            </div>  

                          </div>
                        </div>', ['car/view', 'id' => $model->car_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                ?>
            <?php } ?>
        </div>
        <br>
    </div>

    <?php if (!empty($dataProvider->models)) : ?>
        <?php
        echo \yii\bootstrap4\LinkPager::widget([
            'pagination' => $pagination,
        ]);
        ?>
    <?php else : ?>
        <div class="alert alert-danger" role="alert">
            <?php echo "ไม่พบข้อมูล !!"; ?>
        </div>
    <?php endif; ?>





    <div class="row">

        <!--<div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <p>เพื่อให้รถขนส่งติดต่อมาหาคุณ</p>
        </div>-->

        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <?= '' /*Html::a(
                'ลงประกาศ "หารถขนส่ง"',
                ['job/create'],
                ['class' => 'btn btn-info btn-block btn-lg', 'style' => '']
            )*/ ?>
        </div>-->

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

        <!--<div class="col-xs-12 col-sm-12 col-md-12" > 
               
               <?= '' /*Html::a(
                   '<i class="fab fa-facebook-square"></i> แฟนเพจ Rogistic.com',
                   'https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC',
                   ['class' => 'btn btn-primary  btn-block btn-lg']
   
                   )*/
                ?>
   
       </div>

       <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

       <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom: 100px;"> 
               
               <?= '' /*Html::a(
                   '<i class="fab fa-facebook-square"></i> เข้าร่วมกลุ่ม',
                   'https://www.facebook.com/groups/376534166106364/',
                   ['class' => 'btn btn-primary  btn-block btn-lg']
   
                   )*/
                ?>
   
       </div>-->



    </div>
</div>