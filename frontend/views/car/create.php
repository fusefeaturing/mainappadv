<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogUserCar */

$this->title = 'เพิ่มรถขนส่งให้บริการของฉัน';
$this->params['breadcrumbs'][] = ['label' => 'รถขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-car-create">
    <div class="card2">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>

    
    <?= $this->render('_form', [
        'model' => $model,
        'cartypes' => $cartypes,
        'amphur' => [],
        'cardes' => [],
    ]) ?>

</div>
