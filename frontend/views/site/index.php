<?php

/* @var $this yii\web\View */
use yii\helpers\Html;


$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css");

$this->title = 'Rogistic.com เว็บศูนย์รวมงานขนส่ง';
?>


<?php 

if (Yii::$app->user->isGuest) {
        
    
} else {
    $userid = yii::$app->user->identity->id;
    echo '<h2>ยินดีต้อนรับคุณ ' .Yii::$app->user->identity->name . '</h2>';
    
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'ออกจากระบบ (' . Yii::$app->user->identity->name . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}

?>


<div class="container">
    <div class="row"> <!--<i class="fas fa-shipping-fast fa-flip-horizontal"></i> -->
        <div class="col-md-4">
                            <?= Html::a(
                                '<div class="card-counterv4 success">
                                <i class="fas fa-search"></i>

                                <span class="count-numbers">รถร่วมขนส่ง</span>
                                <span class="count-name">ทุกประเภท ทั่วไทย</span>
                                </div>',
                                ['car/index'],
                                [/*'style' => 'background-color:#cb06ff'*/]
                            )?>


        </div>

                                
        <div class="col-md-4">
            <?= Html::a(
                '<div class="card-counterv4 warning">
                <i class="fas fa-car-side"></i>
                <span class="count-numbers">งานขนส่ง</span>
                <span class="count-name">ที่มีลูกค้าประกาศหารถ</span>
                </div>',
                ['job/index'],
                [/*'style' => 'background-color:#cb06ff'*/]
            )?>
        </div>
                                

                                
        <?php

    if (Yii::$app->user->isGuest) {


    
        echo '<div class="col-md-4" style="justify-content: center; align-items: center; ">';
        echo Html::a(
            '<div class="card-counterv4 primary">
            <i class="fab fa-facebook-square"></i>
            <span class="count-numbers">Rogistic.com</span>
            <span class="count-name">สมัครสมาชิก/เข้าสู่ระบบ</span>
            </div>',
            ['site/auth', 'authclient' => 'facebook'],
            ['class' => 'primary', 'style' => '']
        );
        echo '</div>';

    

        echo '<br>';

    
    } else {

    
        echo '<div class=" col-md-4" style="justify-content: center; align-items: center; ">';
        echo Html::a(
            '<div class="card-counterv4 danger">
            <i class="fas fa-user-edit"></i>
            <span class="count-numbers">แก้ไขข้อมูลของฉัน</span>
            <span class="count-name">รถขนส่ง พื้นที่ให้บริการ</span>
            </div>',
            ['/user/profile' , 'id' => $userid],
            ['class' => '']
        );
        echo '</div>';
    }

    ?>     

            <div class="col-md-4" style="justify-content: center; align-items: center; padding-top:10px;">
                <div class="fb-page"  
                    data-href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                    data-tabs="" 
                    data-width="280"         
                    data-small-header="false" 
                    data-adapt-container-width="true" 
                    data-hide-cover="false" 
                    data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                    class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC">
                            Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                        </a>
                    </blockquote>
                </div>
            </div>
    </div>

</div>


    
    
     

   <!--<div class="fb-group col-md-4" style="margin-bottom:30px;" 
        data-href="https://www.facebook.com/groups/376534166106364/" 
        data-width="320" 
        data-show-social-context="true" 
        data-show-metadata="true">
    </div>-->

    
   


   


