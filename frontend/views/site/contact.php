<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'เกี่ยวกับเรา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="card2">
        <div class="card-body">


    <div class="container">
        <h1><?= Html::encode($this->title) ?></h1>
        <hr>
            <div class="row">
                     <div class="col-xs-12 col-sm-12 col-md-6 text-center" style=" margin-top:10px;">
                         <div class="fb-page"  
                             data-href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                             data-tabs="timeline,events,messages" 
                             data-width="315"
                             data-height="400"          
                             data-small-header="true" 
                             data-adapt-container-width="false" 
                             data-hide-cover="false" 
                             data-show-facepile="true">
                             <blockquote cite="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                             class="fb-xfbml-parse-ignore">
                                 <a href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC">
                                     Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                 </a>
                             </blockquote>
                         </div>
                     </div>

                     <div class="fb-group col-xs-12 col-sm-12 col-md-6 text-center" style="margin-top:10px; margin-bottom:20px;" 
                         data-href="https://www.facebook.com/groups/376534166106364/" 
                         data-width="320" 

                         data-show-social-context="true" 
                         data-show-metadata="true">
                     </div>
            </div>
    </div>

    </div>
</div>

</div>
