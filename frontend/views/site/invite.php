<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Rogistic.com เพิ่มลูกค้าให้ธุรกิจขนส่งคุณได้อย่างไร';
Yii::$app->params['og_title']['content'] = $this->title;
$this->params['breadcrumbs'][] = 'เชิญชวน - วิธีใช้';

Yii::$app->params['og_image']['content'] = Yii::getAlias('../common/web/img/new-rog.jpg');
Yii::$app->params['og_url']['content'] = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<style>
    @media only screen and (max-width: 576px){
        ul > h1 {
        font-size: 1.3rem;
    }

    .path {
        font-size: 1.3rem;
    }

    ul > h2 {
        font-size: 1.2rem;
    }

   
}

@media only screen and (min-width: 768px){


    .indentli {
        text-indent: 2.5em;
    }
}



</style>

<div class="site-about">

    <div class="card2">
        <div class="card-body">
            <ul><h1><strong>Rogistic.com เพิ่มลูกค้าให้ธุรกิจขนส่งคุณได้อย่างไร</strong></h1>
           
                <div class="row" style="margin-left:10px;">   
                    <!-- Your share button code -->
                    <div class="fb-share-button" style="margin-right:10px;"
                        data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                        data-layout="button_count" 
                        data-size="large">
                            <a target="_blank" href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                                class="fb-xfbml-parse-ignore">แชร์
                            </a>
                    </div>

                    <div class="line-it-button" style="margin-right:10px;"
                        data-lang="th" 
                        data-type="share-a" 
                        data-ver="3" 
                        data-url=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                        data-color="default" 
                        data-size="large" 
                        style="display: none; ">
                    </div>

                    <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                    คัดลอก URL <i class="fas fa-copy"></i>
                    </button> 

                </div>
            
        <hr>

                <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/new-rog.jpg' ?>" class=" img-fluid rounded mx-auto d-block car-display-mobile" alt="Rogistic.com เพิ่มลูกค้าให้ธุรกิจขนส่งคุณได้อย่างไร ???">
                
                    <br>
             
                    <hr>
                <h4>
                    <li class="indentli" style="">1. ธุรกิจขนส่งมีตัวตน สร้างความน่าเชื่อถือในโลกออนไลน์</li>
                    <br>
                    <li class="indentli" style="">2. เพิ่มรถขนส่งและพื้นที่ให้บริการได้ง่าย</li>
                    <br>
                    <li class="indentli" style="">3. เข้าถึงกลุ่มลูกค้าที่ต้องการใช้บริการรถขนส่งจำนวนมาก</li>
                    <br>
                    <li class="indentli" style="">4. ช่วยทำการตลาดออนไลน์ ทั้ง google facebook line</li>
                    <br>
                    <li class="indentli" style="">5. ลูกค้าติดต่อหาคุณได้โดยตรง</li>
                    <br>
                    <li class="indentli" style="">6. สมัครง่าย 30 วินาที</li>
                </h4>
<hr>

                <h5><strong>สิ่งที่ต้องเตรียมก่อนสมัคร Rogistic.com</strong></h5>
                <li class="indentli" style="">• สมัครด้วย account facebook เท่านั้น</li>
                <li class="indentli" style="">• รายละเอียดกิจการ ชื่อกิจการ , ที่อยู่ , เบอร์โทร , line id , facebook fanpage</li>
                <li class="indentli" style="">• รูปภาพกิจการขนส่งสวยๆ หรือ นามบัตร ไม่เกิน 5 รูป (แนวนอน)</li>
                <li class="indentli" style="">• เพิ่มได้หลายคัน หลายประเภท รูปรถขนส่ง คันละ 2 รูป (เฉียงหน้า เฉียงหลัง) </li>
                <li class="indentli" style="">• พื้นที่ที่ให้บริการ</li>

            </ul>
        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>


        <div class="card2">
            <div class="card-body">

            <h2 class="path"><strong>ขั้นตอนการสมัครสมาชิก</strong></h2>
            <hr>

            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/1.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/2.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/3.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/4.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/5.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            <img src="<?= Yii::$app->request->baseUrl . '/common/web/img/6.jpg' ?>" class=" img-fluid rounded mx-auto d-block commany-display-mobile" alt="">
            <br>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>

 
    <div class="card2">
        <div class="card-body">
            <h4>EP.1 Rogistic คืออะไร มีประโยชน์อะไร?</h4>
                <hr>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vOFjDwfaBMQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
                    <br>
            <h4>EP.2 วิธีสมัครสมาชิก Rogistic และลงข้อมูล เพื่อหาลูกค้า</h4>
                <hr>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-GoFbFgjaYM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
                    <br>
            <h4>EP.3 วิธีใช้ Rogistic เพื่อเข้าถึงลูกค้าจำนวนมาก</h4>
                <hr>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uxejGD3nFeg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>





    <div class="card2">
        <div class="card-body">
                <div class="row" style="margin-left:10px;">   
                    <!-- Your share button code -->
                    <div class="fb-share-button" style="margin-right:10px;"
                        data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                        data-layout="button_count" 
                        data-size="large">
                            <a target="_blank" href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                                class="fb-xfbml-parse-ignore">แชร์
                            </a>
                    </div>
                    <div class="line-it-button" style="margin-right:10px;"
                        data-lang="th" 
                        data-type="share-a"   
                        data-ver="3" 
                        data-url=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                        data-color="default" 
                        data-size="large" 
                        style="display: none; ">
                    </div>
                    <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                    คัดลอก URL <i class="fas fa-copy"></i>
                    </button> 
                </div>
           

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 text-center" style=" margin-top:10px;">
                    <div class="fb-page"  
                        data-href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                        data-tabs="timeline,events,messages" 
                        data-width="280"
                        data-small-header="true" 
                        data-adapt-container-width="true" 
                        data-hide-cover="false" 
                        data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC" 
                        class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/rogisticcom/?ref=search&__tn__=%2Cd%2CP-R&eid=ARBPDkBKtUD3z48k6jPsQjJqlAtrF-48Uh9eAbv40d7D-6V6Frz6FvqcYydiHgYHCKq7sdZ3Bogst2YC">
                                Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                            </a>
                        </blockquote>
                    </div>
                </div>

                <div class="fb-group col-xs-12 col-sm-12 col-md-6 text-center" style="margin-top:10px; margin-bottom:20px;" 
                    data-href="https://www.facebook.com/groups/376534166106364/" 
                    data-width="280" 
                    data-show-social-context="true" 
                    data-show-metadata="true">
                </div>
            </div>
        </div>


    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>


    
</div>

<script>

    // COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
  var copyTest = document.queryCommandSupported('copy');
  var elOriginalText = el.attr('data-original-title');

  if (copyTest === true) {
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = text;
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'Copied!' : 'Whoops, not copied!';
      el.attr('data-original-title', msg).tooltip('show');
    } catch (err) {
      console.log('Oops, unable to copy');
    }
    document.body.removeChild(copyTextArea);
    el.attr('data-original-title', elOriginalText);
  } else {
    // Fallback if browser doesn't support .execCommand('copy')
    window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
  }
}

$(document).ready(function() {
  // Initialize
  // ---------------------------------------------------------------------

  // Tooltips
  // Requires Bootstrap 3 for functionality
  $('.js-tooltip').tooltip();

  // Copy to clipboard
  // Grab any text in the attribute 'data-copy' and pass it to the 
  // copy function
  $('.js-copy').click(function() {
    var text = $(this).attr('data-copy');
    var el = $(this);
    copyToClipboard(text, el);
  });
});
</script>