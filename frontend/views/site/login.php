<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->registerCssFile("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css");




use yii\helpers\Html;

use kartik\form\ActiveForm;

$this->title = 'เข้าสู่ระบบ Rogistic.com';
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
?>

<style>
    /*.separator {
    display: flex;
    align-items: center;
    text-align: center;
}
.separator::before, .separator::after {
    content: '';
    flex: 1;
    border-bottom: 1px solid #000;
}
.separator::before {
    margin-right: .25em;
}
.separator::after {
    margin-left: .25em;
}*/

    .hr-text {
        line-height: 1em;
        position: relative;
        outline: 0;
        border: 0;
        color: black;
        text-align: center;
        height: 1.5em;
        opacity: .5;
    }

    .hr-text:before {
        content: '';

        background: linear-gradient(to right, transparent, #818078, transparent);
        position: absolute;
        left: 0;
        top: 50%;
        width: 100%;
        height: 1px;
    }


    .hr-text:after {
        content: attr(data-content);
        position: relative;
        display: inline-block;
        color: black;

        padding: 0 .5em;
        line-height: 1.5em;

        color: #818078;
        background-color: #fcfcfa;
    }


    .indentli {
        text-indent: 2.5em;
    }
</style>

<?php
$form = ActiveForm::begin([
    'id' => 'login-form-vertical',
    'type' => ActiveForm::TYPE_VERTICAL
]);
?>
<div class="card2">
    <div class="card-body">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <h3><strong> ร่วมเป็น Partner กับ Rogistic.com</strong></h3>
            <p>
                สมัครสมาชิก / เข้าสู่ระบบ ง่ายๆ<br>ด้วยระบบ facebook <br> สะดวก ปลอดภัย
            </p>
        </div>

        <div class="row">

            <div class="col-xs-6 col-sm-6 col-md-6">

                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= '' //$form->field($model, 'rememberMe')->checkbox(['id' => 'remember-me-ver', 'custom' => true]) 
                ?>

                <div class="form-group ">
                    <?= Html::submitButton('เข้าสู่ระบบ', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('สมัครสมาชิก', ['user/register'], ['class' => 'btn btn-secondary']) ?>
                </div>

                <hr class="hr-text" data-content="OR">
                <!--<div class="separator">or login social</div>-->

                <div class="text-center" style="padding-top: 20px;">
                    <?= Html::a(
                        '  สมัครสมาชิก / เข้าสู่ระบบ',
                        ['site/auth', 'authclient' => 'facebook'],
                        ['class' => 'btn btn-primary  btn-lg fab fa-facebook-square ', 'style' => 'background-color: #3b5998; ']
                    )
                    ?>
                </div>

            </div>


            <?php ActiveForm::end(); ?>



            <div class="col-xs-6 col-sm-6 col-md-6">


                <div class="col-xs-12 col-sm-12 col-md-12">

                    <h6 class="text-center" style="color : red; padding-top:30px;"><strong><u>***สิทธิพิเศษสำหรับสมาชิก เท่านั้น***</u></strong><br></h6><br>
                    <div class="">
                        <h6 class="text-center"><u>สำหรับผู้ประกอบการรถขนส่ง</u><br><br></h6>
                        <h6 class="indentli">1. ลงประกาศกิจการของท่าน<br></h6>
                        <h6 class="indentli">2. ลูกค้าสามารถติดต่อท่านได้โดยตรง<br></h6>
                        <h6 class="indentli">3. ฟรี!</h6>
                    </div>
                    <hr>
                    <div class="">
                        <h6 class="text-center"><u>สำหรับลูกค้า</u><br><br></h6>
                        <h6 class="indentli">1. ติดต่อผู้ประกอบการรถขนส่งได้โดยตรง<br></h6>
                        <h6 class="indentli">2. ลงประกาศหารถขนส่งของท่าน<br></h6>
                        <h6 class="indentli">3. ฟรี!</h6>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>