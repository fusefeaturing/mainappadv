﻿<?php

/* @var $this \yii\web\View */
/* @var $content string 
        <p class="pull-left">&copy; Rogistic <?= date('Y') ?></p>
<p class="pull-right"><?= Yii::powered() ?></p>*/

use common\widgets\Alert;

use yii\helpers\Url;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use frontend\assets\AppAsset;

$currentUrl = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; //Url::canonical();
$this->registerMetaTag(Yii::$app->params['og_title'], 'og_title');
$this->registerMetaTag(Yii::$app->params['og_description'], 'og_description');
$this->registerMetaTag(Yii::$app->params['og_url'], 'og_url');
$this->registerMetaTag(Yii::$app->params['og_image'], 'og_image');
$this->registerMetaTag(Yii::$app->params['og_robots'], 'og_robots');
$this->registerMetaTag(Yii::$app->params['og_revisit-after'], 'og_revisit-after');

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- End Load Facebook SDK for JavaScript -->
    
    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v6.0&appId=407866350019570&autoLogAppEvents=1"></script>

<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>


    
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>

    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            //'brandImage' => Html::img(Yii::getAlias('@web').'/img/image.jpg', ['class' => 'img-responsive', 'width' => 800, 'height' => 200]),
            'brandUrl' => Yii::$app->homeUrl,
            //'headerContent' => Html::a('061-872-8888','tel:+66618728888',['class' => 'navbar-brand navbar-link']),
           
            'options' => [
                'class' => 'fixed-top navbar-expand-md navbar-dark bg-dark',
                
            ],
          

        ]);
        ?>

        
        <?php
        //$navbar_items[] = ['label' => 'หน้าแรก', 'url' => ['/site/index']];        
        $navbar_items[] = ['label' => 'รถร่วมขนส่ง', 'url' => ['/car/index']];
        //$navbar_items[] = ['label' => 'ลงประกาศ "หารถขนส่ง"', 'url' => ['/job/my-job']];
        $navbar_items[] = ['label' => 'งานขนส่ง', 'url' => ['/job/index']];
        $navbar_items[] = ['label' => 'ดูประเภทรถทั้งหมด', 'url' => ['/car-type/index']];
        $navbar_items[] = ['label' => 'Blog', 'url' => ['blog/']];        
        $navbar_items[] = ['label' => 'เกี่ยวกับเรา', 'url' => ['/site/contact']];
        $navbar_items[] = ['label' => 'เชิญชวน - วิธีใช้', 'url' => ['/site/invite']];


        if (Yii::$app->user->isGuest) {
            $navbar_items[] = ['label' => 'สมัครสมาชิก', 'url' => ['/site/login']];
            $navbar_items[] = ['label' => 'เข้าสู่ระบบ', 'url' => ['site/auth', 'authclient' => 'facebook']];
        } else {
            $userid = yii::$app->user->identity->id;
            //$navbar_items[] = ['label' => 'ดูและแก้ไขข้อมูลส่วนตัว', 'url' => ['/user/profile', 'id' => $userid] ];
            $navbar_items[] = ['label' => 'ดูและแก้ไขข้อมูลส่วนตัว (' . Yii::$app->user->identity->name . ')', ['class' => 'text-danger'], 'url' => ['/user/profile', 'id' => $userid] ] ;
            //$navbar_items[] = ['label' => Html::tag('span', 'ดูและแก้ไขข้อมูลส่วนตัว<br>' . Yii::$app->user->identity->username . ')', ['style'=>'text-align:center']), 'url' => ['/user/profile']];
            $navbar_items[] = (''
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ออกจากระบบ',
                    ['class' => 'btn btn-outline-danger']
                )
                . Html::endForm()
                . '');
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav ml-auto p-2 bd-highlight '],
            'items' => $navbar_items,
        ]);
        NavBar::end();
        ?>

        <div class="container" style="padding-top: 100px;">            
            <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-chevron-up"></i></button>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
        </div>
       

        

 

    
    <!--<footer class="page-footer font-small indigo fixed-bottom" style="background-color: #FFFFFF;  margin-top: 100px;">       
        <div class="container text-center text-md-left">
          <div class="row2">
            <div class="col-md-3 ">
                <div class="font-weight-bold text-uppercase mt-3 mb-4 ">
                    <p class="d-none d-sm-block ">                            
                        ให้ rogistic.com หารถให้คุณ 
                    </p>
                    <span class="d-lg-none d-md-none d-sm-none"> <?='' //Html::a('<i class="fab fa-facebook  fa-1x f-icon-color-facebook"></i><h5><small>facebook</small></h5>', 'https://www.facebook.com/Rogisticcom-359767341103161') ?> </span>
                </div>
            </div>  
            
                <div class="col-md-3 ">
                    <div class="font-weight-bold text-uppercase mt-3 mb-4 ">                         
                        <p class="d-none d-sm-block ">                           
                            <i class="fas fa-phone-alt fa-1x f-icon-color-phone"></i>                           
                            <?= Html::a(
                                '061-872-8888',
                                'tel:+66618728888',
                                ['class' => 'btn btn-link' , 'style' => 'text-decoration: none;']
                            )
                            ?>
                        </p>
                        <span class="d-lg-none d-md-none d-sm-none " style="text-decoration: none;"> <?= Html::a('<i class="fas fa-phone-alt fa-1x f-icon-color-phone"; margin-top: 10px;"></i> <h5><small>โทร</small></h5>', 'tel:+66618728888') ?> </span>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="font-weight-bold text-uppercase mt-3 mb-4">
                        <p class="d-none d-sm-block ">
                            <i class="fab fa-line fa-1x f-icon-color-line"></i>
                                <?= Html::a(
                                        '@Line',
                                        'https://line.me/R/ti/p/%40rogistic',
                                        ['class' => 'btn btn-link' , 'style' => 'text-decoration: none;']
                                    )
                                ?>
                        </p>
                        <span class="d-lg-none d-md-none d-sm-none"> <?= Html::a('<i class="fab fa-line fa-1x f-icon-color-line"></i><h5><small>line</small></h5>', 'https://line.me/R/ti/p/%40rogistic') ?> </span>         
                    </div>
                </div>
                  

            <div class="col-md-3 ">
              <div class="font-weight-bold text-uppercase mt-3 mb-4">
                <p class="d-none d-sm-block">
                    <i class="fab fa-facebook-messenger fa-1x f-icon-color-messenger"></i>
                    <?= Html::a(
                        'Massenger',
                        'https://m.me/359767341103161',
                        ['class' => 'btn btn-link' , 'style' => 'text-decoration: none;']

                    );
                    ?>
                </p>
                <span class="d-lg-none d-md-none d-sm-none"> <?='' //Html::a('<i class="fab fa-facebook-messenger fa-1x f-icon-color-messenger"></i><h5><small>messenger</small></h5>', 'https://m.me/359767341103161') ?> </span>
            </div> 
            </div>
        </div>
    </footer>-->

    <footer class="navbar fixed-bottom bg-red justify-content-center" >                          
        <div class="wpfront-message wpfront-message576">ติดต่อ Rogistic.com &nbsp;</div>
            <div> 
                <a class="wpfront-button btn-sm" href="https://line.me/R/ti/p/%40rogistic" style="text-decoration: none;" target="_blank" ><i class="fab fa-line"></i> LINE ID : @Rogistic</a>
                <a class="wpfront-button-blue btn-sm" href="tel:+66618728888" style="text-decoration: none;" target="_blank" rel="noopener noreferrer"><i class="fas fa-phone-alt"></i> 061-872-8888</a>                
            </div>               
    </footer>
         
    </div>
    
    <?php $this->endBody() ?>


    <script>
//Get the button:
mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
</script>





</body>

</html>
<?php $this->endPage() ?>