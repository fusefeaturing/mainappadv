<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogUser */

$this->title = 'Create Rog User';
//$this->params['breadcrumbs'][] = ['label' => 'Rog Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
