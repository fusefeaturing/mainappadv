<?php

use common\models\Provinces;
use common\models\RogUser;

use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RogUser */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
?>

    
<?php
    /*
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
    </div>
    </div>
    */
?>

<div class="rog-user-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>
    
    <div class="card2">
        <div class="card-body">

            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true])
            ->label('ชื่อกิจการ',['class'=>'','style'=>'']) ?>

            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <?= $form->field($model, 'company_explanation')->textarea(['rows' => 6]) ?>
                    </div>
                </div>

                <div class="row">
                           <div class="col-xs-12 col-sm-6 col-md-3">
                               <?=
                                   $form
                                       ->field($model, 'province_id')

                                       ->dropdownList($province, [
                                           'id' => 'ddl-province',
                                           'prompt' => 'เลือกจังหวัด'
                                       ])
                                       ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
                               ?>
                           </div>
                                    
                           <div class="col-xs-12 col-sm-6 col-md-3">
                               <?=
                                   $form
                                       ->field($model, 'amphure_id')
                                       ->widget(DepDrop::classname(), [
                                           'options' => ['id' => 'ddl-amphure'],
                                           'data' => $amphure,
                                           'pluginOptions' => [
                                               'depends' => ['ddl-province'],
                                               'placeholder' => 'เลือกอำเภอ...',
                                               'url' => Url::to(['/user/get-amphure'])
                                           ]
                                       ])
                                       ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
                               ?>
                           </div>   
                                        
                           <div class="col-xs-12 col-sm-6 col-md-3">
                               <?=
                                   $form
                                       ->field($model, 'district_id')
                                       ->widget(DepDrop::classname(), [
                                            'options' => ['id' => 'ddl-district'],
                                            'data' => $district,
                                            'pluginOptions' => [
                                                'depends' => ['ddl-province', 'ddl-amphure'],
                                                'placeholder' => 'เลือกตำบลที่ตั้งกิจการ...',
                                                'url' => Url::to(['user/get-district'])
                                            ]
                                       ])
                                       ->label('ตำบลที่ตั้งกิจการ');
                               ?>
                           </div>
                                            
                            <div class="col-xs-12 col-sm-6 col-md-3">
                               <?=
                                   $form
                                       ->field($model, 'zipcode_id')
                                       ->widget(DepDrop::classname(), [
                                            'options' => ['id' => 'ddl-zipcode'],
                                            'data' => $zipcode,
                                            'pluginOptions' => [
                                                'depends' => ['ddl-province', 'ddl-amphure', 'ddl-district'],
                                                'placeholder' => 'เลือกรหัสไปรษณีย์...',
                                                'url' => Url::to(['user/get-zipcode'])
                                            ]
                                       ])
                                       ->label('รหัสไปรษณีย์');
                               ?>
                           </div>  
                       </div>
                                            
                                            
            <?= 
            $form
            ->field($model, 'tel_1')
                                            
            ->label('เบอร์ติดต่อ 1',['class'=>'','style' => 'color:red'])
            ?>

            <?= $form->field($model, 'line_id')->textInput(['maxlength' => true])
                ->label('Line ID',['class'=>'', 'style' => 'color:red']) ?>

                                            
            <?= $form->field($model, 'email_contact')->textInput(['maxlength' => true]) ?>
                                            
            <?= $form->field($model, 'facebook_token')->textInput(['maxlength' => true]) ?>
                                            
            <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
                                            
                                            
            <div class="">
                                            
            <?= $form->field($model, 'company_address')->textarea(['rows' => 6]) ?>
                                            
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <?= $form->field($model, 'company_pic[]')->fileInput(['multiple' => true])
                            ->label('ภาพกิจการขนส่งใส่ได้ 1 - 5 เท่านั้น', ['class' => '', 'style' => 'color:red'])
                             ?>

                                            
                        <div class="well">
                            <?= $model->getPhotosViewerFront() ?>
                        </div>       
                    </div>
                </div>
            </div>
                                            
                                            
            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top : 10px;">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            </div>
                                            
            <?php ActiveForm::end(); ?>

        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

</div>
