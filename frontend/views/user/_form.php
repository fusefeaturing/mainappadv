<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\jui\DatePicker;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use common\models\Provinces;
use common\models\Amphures;
use common\models\Districts;

$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');

$id = yii::$app->user->identity->id;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-job-post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);
    ?>

    <?php
    /*
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
    </div>
    </div>
    */
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'title')
                    ->label('* หัวข้อ', ['class' => '', 'style' => 'color:red'])
                    ->textInput(['maxlength' => true])
            //->hint('text')
            ?>
        </div>

        <div class="col-xs-12 col-sm-12/ col-md-12">
            <?=
                $form
                    ->field($model, 'full_description')
                    ->textarea(['maxlength' => true, 'rows' => 6])
            ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'contact_person')
                    ->textInput(['maxlength' => true,])
                    ->hint('ชื่อ / เบอร์ติดต่อ ตัวอย่าง : คุณเก่ง 08x-xxx-xxxx')
            ?>
        </div>
    </div>

    <hr>

    <h3 style="color:red;">* เลือกประเภทรถขนส่งที่ต้องการ</h3>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'cartype_valid')
                    ->checkBoxList(
                        $this->context->getCarTypes(),
                        ['itemOptions' => $model->cartypeToArray(), 'separator' => '<br>']
                    )
                    ->hint(Html::a('คลิกดูประเภทรถ', ['car-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'short_description')
                    ->textarea(['maxlength' => true, 'rows' => 6])
            ?>
        </div>
    </div>

    <?= $form->field($model, 'photos[]')->fileInput(['multiple' => true]) ?>
    <div class="well">
        <?= $model->getPhotosViewer(); ?>
    </div>

    <hr>

    <h3>ต้นทาง</h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'pickup_location')
                    ->textarea(['maxlength' => true])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_province')
                    ->label('* จังหวัดต้นทาง', ['class' => '', 'style' => 'color:red'])
                    ->dropdownList($provinces, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_district')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-amphur'],
                        'data' => $amphur,
                        'pluginOptions' => [
                            'depends' => ['ddl-province'],
                            'placeholder' => 'เลือกอำเภอ...',
                            'url' => Url::to(['/job/get-amphur'])
                        ]
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_subdistrict')
                    ->widget(DepDrop::classname(), [
                        'data' => $district,
                        'pluginOptions' => [
                            'depends' => ['ddl-province', 'ddl-amphur'],
                            'placeholder' => 'เลือกตำบล...',
                            'url' => Url::to(['/job/get-district'])
                        ]
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'contact_person_pickup')
                    ->textInput(['maxlength' => true])
                    ->hint('ชื่อ / เบอร์ติดต่อ ตัวอย่าง : คุณเก่ง 08x-xxx-xxxx')
            ?>
        </div>
    </div>

    <hr>

    <h3>ปลายทาง</h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'deliver_location')
                    ->textarea(['maxlength' => true])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_province')
                    ->label('* จังหวัดปลายทาง', ['class' => '', 'style' => 'color:red'])
                    ->dropdownList($provinces, [
                        'id' => 'ddl-dprovince',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_district')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-damphur'],
                        'data' => $damphur,
                        'pluginOptions' => [
                            'depends' => ['ddl-dprovince'],
                            'placeholder' => 'เลือกอำเภอ...',
                            'url' => Url::to(['/job/get-amphur'])
                        ]
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_subdistrict')
                    ->widget(DepDrop::classname(), [
                        'data' => $ddistrict,
                        'pluginOptions' => [
                            'depends' => ['ddl-dprovince', 'ddl-damphur'],
                            'placeholder' => 'เลือกตำบล...',
                            'url' => Url::to(['/job/get-district'])
                        ]
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'contact_person_delivery')
                    ->textInput(['maxlength' => true])
                    ->hint('ชื่อ / เบอร์ติดต่อ ตัวอย่าง : คุณเก่ง 08x-xxx-xxxx')
            ?>
        </div>
    </div>

    <hr>

    <h3>ความถี่ของงาน</h3>

    <div class="row">

        <div class="col-xs-12 col-sm-4 col-md-4">

            <?php
            if (!$model->is_period_job) $model->is_period_job = 0;
            ?>

            <?=
                $form
                    ->field($model, 'is_period_job')
                    ->radioList([0 => 'งานครั้งเดียว', 1 => 'งานประจำ'])
                    ->label(false)
            ?>
        </div>

        <div class="col-xs-12 col-sm-8 col-md-8">
            <?= $form
                ->field($model, 'deliver_date')
                ->widget(DatePicker::classname(), [
                    'language' => 'th',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                    ],
                    'options' => ['class' => 'form-control']
                ])
            ?>
        </div>
    </div>

    <hr>

    <h3>สถานะงาน</h3>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?php
            if (!$model->is_job_taken) $model->is_job_taken = 0;
            ?>

            <?=
                $form
                    ->field($model, 'is_job_taken')
                    ->radioList([0 => 'ยังไม่ถูกรับ', 1 => 'ถูกรับแล้ว'])
            //->label(false)
            ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?php
            if (!$model->is_job_close) $model->is_job_close = 0;
            ?>

            <?=
                $form
                    ->field($model, 'is_job_close')
                    ->radioList([0 => 'งานยังไม่จบ', 1 => 'จบงานแล้ว'])
            //->label(false)
            ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <?php
            if (!$model->is_job_appear) $model->is_job_appear = 0;
            ?>

            <?=
                $form
                    ->field($model, 'is_job_appear')
                    ->radioList([0 => 'แสดงงานในหน้าค้นหา', 1 => 'ซ่อนงานจากหน้าค้นหา'])
                    //->label(false)
                    ->hint('ยังสามารถเข้าได้จาก url ที่แชร์ไว้')
            ?>
        </div>

    </div>



    <?=
        $form
            ->field($model, 'created_date')
            ->hiddenInput([
                'readonly' => true,
                'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
            ])
            ->label(false)
    ?>

    <?=
        $form
            ->field($model, 'updated_date')
            ->hiddenInput([
                'readonly' => true,
                'value' => date('Y-m-d H:i:s')
            ])
            ->label(false)
    ?>

    <?=
        $form
            ->field($model, 'user_id')
            ->hiddenInput(['readonly' => true, 'value' => $id])
            ->label(false)
    ?>

    <div class="form-group" >
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>