<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogUser */

$this->title = 'แก้ไขข้อมูล : ' . $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'Rog Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['profile', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>