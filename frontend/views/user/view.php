<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\RogUser 

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Delete', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
</p>*/

$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="rog-user-view">
    <div class="container">
        
    <h1><?= Html::encode($this->title) ?></h1>

    <?php

    if ($owner) {
        echo '<p>';
        echo Html::a('แก้ไขรายละเอืยด', ['user/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo '</p>';
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prefix',
            'name',
            'surname',
            'middlename',
            'tel_1',
            'tel_2',
            'line_id',
            'email_contact:email',
            'facebook_token',
            'website',
            
            //'username',
            'created_date',
            'updated_date',
        ],
    ]) ?>

    <hr>

        <div class="service_detail" style="margin-bottom :10px">

            <h2>ข้อมูลกิจการ</h2>

            <?php
            if ($owner) {
                echo '<p>';
                echo Html::a('แก้ไขรายละเอืยด', ['user/update-service', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo '</p>';
            }
            ?>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'company_name',
                    'company_address:ntext',
                    'company_pic',
                    'company_explanation'
                ],
            ]) ?>
            </div>

            <hr>
        
        <div class="">
        
            <h2>พื้นที่ให้บริการ</h2>
        
        
                <?php

            $provtran = [];
            foreach($serviceroutes as $key => $servicearea)
            {
                $provtran[$provinces[$servicearea]][$key] = $amphures[$key];
            }
        
            foreach($provtran as $key => $protran)
            {
                echo '<h4>' . $key . '</h4>';
                echo '<p>';
                foreach($provtran[$key] as $distran)
                {
                    echo $distran . ' ';
                }
                echo '</p>';
                echo '<hr>';
            }
            ?>

        </div>
    </div>

</div>



    