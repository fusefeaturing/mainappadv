<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
$userid = yii::$app->user->identity->id;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลผู้ใช้', 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>



<!--
<div class="rog-user-car-index">
    <?= Html::a('ดูเส้นทางที่ให้บริการ', ['car/route']) ?>
    <br>
</div>

<div class="rog-user-car-index">
    <?= Html::a('เพิ่มเส้นทางที่ให้บริการ', ['car/add-route']) ?>
    <br>
</div>

<div class="rog-user-car-index">
    <?= Html::a('รถของฉัน', ['car/my-car']) ?>
    <br>
</div>-->

<!--<div class="rog-user-car-index">
    <?= Html::a('ประกาศหารถของฉัน', ['job/my-job'],['class' => 'btn btn-warning']) ?>

    <hr>
</div>-->

<div class="rog-user-view">
    
    <div class="card2">
        <div class="card-body">

        <h1>ข้อมูลส่วนตัว <?= Html::encode($this->title) ?></h1>

        <?php
        if (yii::$app->user->identity->id == $model->id) {
            echo '<p>';
            echo Html::a('แก้ไขข้อมูลติดต่อของฉัน', ['user/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo '</p>';
        }
        ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'surname',
                //'company_name',
                //'tel_1',
                //'tel_2',
                //'line_id',
                'email_contact:email',
                //'facebook_token',
                //'website',
                //'prefix',

                //'middlename',
                //'username',
                'created_date',
                'updated_date',
            ],
        ]) ?>

        </div>
    </div>

    </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>

            <div class="card2">
                <div class="card-body">



        <div class="service-detail">

            <h2>ข้อมูลกิจการ</h2>
                <h6 style="color:red;">กรอกข้อมูลให้ครบ ข้อมูลส่วนนี้จะนำไปแสดงเพื่อให้ลูกค้าติดต่อท่านได้โดยตรง</h6>
            <?php
                if (yii::$app->user->identity->id == $model->id) {
                    echo '<p>';
                    echo Html::a('แก้ไขรายละเอียด', ['user/update-service', 'id' => $model->id], ['class' => 'btn btn-primary']);
                    echo '</p>';
                }
            ?>

                <div class="">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
            
                                <?php
                                    $imagesexplode = explode(",", $model->company_pic);
                                    $photosImage = $model->company_pic;
            
                                foreach ($imagesexplode as $key => $loopImage) { ?>

                                        <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                                
                                <?php } ?>
                                
                        </ol>
                                
                        <div class="carousel-inner">
                                
                            <?php $count=0;

                            foreach ($imagesexplode as $key => $loopImage) { $count++ ?>
                                <div class="carousel-item <?= ($count==1)?'active':'' ?>">
                                    <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/'. $loopImage ?> " 
                                    class=" img-thumbnail rounded mx-auto d-block gImg commany-display-mobile" >
                                </div>
                            
                                <div id="myModal" class="modal">
                                    <span class="close">×</span>
                                        <img class="modal-content" id="img01">
                                    <div id="caption"></div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                    </div>
                            
                    <hr>
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><h4><strong><?=$model->user->company_name ?></strong> </h4></p>
                    </div>
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>คำอธิบายกิจการ</strong> : <?= nl2br($model->user->company_explanation) ?></p>
                    </div>
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>ที่อยู่กิจการ</strong> : ต. <?= $model->district() ?> อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
                            
                    </div>
                            
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>Facebook Fanpage</strong> : 
                                <br>
                            <div class="fb-page col-md-4 text-center "  
                                    data-href="<?= $model->user->facebook_token ?>" 
                                    data-tabs="" 
                                    data-width="280"         
                                    data-small-header="false" 
                                    data-adapt-container-width="false" 
                                    data-hide-cover="false" 
                                    data-show-facepile="true">
                                    <blockquote cite="<?= $model->user->facebook_token ?>" 
                                    class="fb-xfbml-parse-ignore">
                                        <a href="<?= $model->user->facebook_token ?>">
                                            Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                        </a>
                                    </blockquote>
                            </div>
                        </p>
                    </div>
                            
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>Email</strong> : <?=$model->user->email_contact ?></p>
                    </div>
                            
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>Website</strong> : <?=$model->user->website ?></p>
                    </div>
                </div>
                            
        
        </div>
    </div>
</div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>

        <div class="card2">
            <div class="card-body">
        <h2>รถขนส่งให้บริการของฉัน</h2>

            <p>
                <?= Html::a('+ เพิ่มรถขนส่ง', ['car/create'], ['class' => 'btn btn-success']) ?>
            </p>

            </div>
        </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>

            <div class="container">
                <div class=" row row-cols-1 row-cols-md-3">

                  <?php foreach ($modelsLimits as $model) { ?>
                
                    <?= Html::a('<div class="card-group h-100"">
                                      <div class="card card-loop" >
                                        <div class="cardd"> 
                                        ' . Html::img('../'.$model->getFirstPhotoURLP(), ['class' => 'img-responsive rounded mx-auto d-block car-display-loop' ]) . '                      
                                        </div>        
                                        <div class="card-body card-body-list-car">
                                          <h6 class="card-title">' . $model->cardes->typewheel->type_wheel_name . ' ' . $model->cardes() . ' </h6>
                                          <p class="card-text" style=""> ' . $model->user->company_name . ' </p>
                                          <p class="card-text"><i class="fas fa-map-marker-alt"></i> ' . $model->user->pickup() . ' ' . $model->user->deliver() . ' </p>
                                        </div>       
                                      </div>
                                    </div>', ['car/view', 'id' => $model->car_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                    ?>

                  <?php } ?>
                
                </div>
                <br>
            </div>
                
            <?php if (!empty($modelsLimits)) : ?>
                        <?php
                                echo \yii\bootstrap4\LinkPager::widget([
                                'pagination' => $pagination,
                            ]);
                        ?>
            <?php else : ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo "ไม่พบข้อมูล !!"; ?>
                        </div>
            <?php endif; ?>


        <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
        </div>

<div class="card2">
    <div class="card-body"> 
        <div class="">

            <h2>พื้นที่ให้บริการ</h2>
                <p>
                    <?= Html::a('+ ระบุเส้นทางที่ฉันให้บริการ', ['car/add-route', 'id' => $userid], ['class' => 'btn btn-success']) ?>
                </p>

                <hr>

            <?php           

                    $provtran = [];
                    foreach($serviceroutes as $key => $servicearea)
                        {
                            $provtran[$provinces[$servicearea]][$key] = $amphures[$key];
                        } 
                    if (!empty($serviceroutes)) {          
                    
                    } else {
                                   echo ' <div class="alert alert-danger" role="alert">';
                                         echo "ไม่มีข้อมูลพื้นที่ให้บริการ !!"; 
                                   echo '</div>';
                        }     
                    foreach($provtran as $key => $protran)
                        { 
                            echo '<h4>' . $key . '</h4>';
                            echo '<p>';
                            foreach($provtran[$key] as $distran)                                    
                            {     
                                echo $distran . ' ';
                            }
                            echo '</p>';
                            echo '<hr>';
                        }
            ?>

        </div>
    </div>
</div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>

        <div class="card2">
            <div class="card-body">



            <div>
                <h2>ลงประกาศ หารถขนส่ง</h2>
                <p>
                    <?= Html::a('+ ลงประกาศ หารถขนส่ง', ['job/my-job'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>

            </div>
        </div>


    <div class="shortcut_relink" style="margin-bottom :10px">
        <div class="row">

            <!--<div class="col-xs-12 col-sm-12 col-md-12">
                <p>เมื่อท่านสมาชิกกรอกข้อมูลติดต่อเรียบร้อย ท่านสามารถใช้บริการลงประกาศใน Rogistic.com ได้เลยค่ะ</p>
            </div>-->

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p> </p>
            </div>

            <!--<div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a(
                    'ลงประกาศ "หารถขนส่ง"',
                    ['job/my-job'],
                    ['class' => 'btn btn-info btn-block btn-lg', 'style' => '']
                ) ?>
            </div>-->

 
            


            <!--<div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a(
                    'ลงประกาศ "ฉันมีรถขนส่ง" ให้บริการ',
                    ['car/my-car'],
                    ['class' => 'btn btn-warning btn-block btn-lg', 'style' => '']
                ) ?>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <hr>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                
                
                        <?= Html::a(
                            '<i class="fab fa-facebook-square"></i> แฟนเพจ Rogistic.com',
                            'https://www.facebook.com/Rogisticcom-359767341103161',
                            ['class' => 'btn btn-primary  btn-block btn-lg']

                            )
                        ?>    

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p> </p>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a(
                    'กลับสู้หน้าหลัก Rogistic.com',
                    ['site/index'],
                    ['class' => 'btn btn-danger btn-block btn-lg', 'style' => '']
                ) ?>
            </div>-->

        </div>
    </div>