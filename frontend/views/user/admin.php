<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--?= Html::a('Create Rog User', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'username',
            //'password',
            //'company_name',
            //'company_address:ntext',
            //'company_pic',
            //'pic_idcard',
            //'pic_carlic',
            //'pic_self',
            //'facebook_id',
            //'facebook_token',
            //'citizen_id',
            //'carlic_id',
            //'prefix',
            'name',
            'surname',
            //'middlename',
            'dob',
            //'email:email',
            //'website',
            //'line_id',
            //'tel_1',
            //'tel_2',
            'is_user_findcar',
            'is_user_car_owner',
            'is_user_verified',
            //'created_date',
            //'updated_date',

            //['class' => 'yii\grid\ActionColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
            ],
        ],
    ]); ?>
</div>
