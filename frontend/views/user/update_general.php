<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogUser */

$this->title = 'อัพเดทข้อมูลผู้ใช้ - ข้อมูลทั่วไป : ' . $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'ผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['profile', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-update">

    <div class="card2">
        <div class="card-body">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <?= $this->render('_form_general', [
        'model' => $model,
    ]) ?>

</div>
