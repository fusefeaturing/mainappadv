<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogJobPost */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin Job';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'job_id',
            //'user_id' => 'User ID',
            'title',
            'contact_person',
            //'contact_person_pickup' => 'ผู้ติดต่อ จุดรับของ',
            //'contact_person_delivery' => 'ผู้ติดต่อ จุดส่งของ',
            //'short_description' => 'ประเภทสินค้า / รายละเอียด (ถ้ามี)',
            //'full_description' => 'รายละเอียดงานขนส่ง / การขึ้น ลงสินค้า',
            //'photos' => 'รูปสินค้า / งานที่ต้องการขนส่ง (เลือกได้ 4 ภาพ)',
            //'cartype_valid' => 'ประเภทรถขนส่งที่ต้องการ',
            //'deliver_date' => 'วันที่ใช้รถขนส่ง',
            
            'pickup_location',
            //'pickup_province',
            [
                'attribute'=>'pickup_province',
                'value'=> function($model)
                {
                    return $model->pickup();
                },
            ],
            //'pickup_district' => 'อำเภอ/เขตต้นทาง',
            //'pickup_subdistrict' => 'ตำบล/แขวงต้นทาง',

            'deliver_location',
            //'deliver_province',
            [
                'attribute'=>'deliver_province',
                'value'=> function($model)
                {
                    return $model->deliver();
                },
            ],

            //'deliver_district' => 'อำเภอ/เขตปลายทาง',
            //'deliver_subdistrict' => 'ตำบล/แขวงปลายทาง',

            //'created_date' => 'Created Date',
            //'updated_date' => 'Updated Date',

            'is_job_taken',
            //'is_period_job' => 'งานประจำ',
            'is_job_close',
            'is_job_appear',

            //['class' => 'yii\grid\ActionColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
            ],
        ],
    ]); ?>
</div>
