<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogJobPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประกาศหารถขนส่งของฉัน';
$userid = yii::$app->user->identity->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-job-post-index">

<div class="card2">
    <div class="card-body">



    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>1.) ลงรายละเอียดงานขนส่งตามแบบฟอร์ม</p>
    <p>2.) <?= Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
    <p>ระบุข้อมูลครบถ้วน 2 รายการ เพิ้อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>

    </div>
</div>
    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <p> </p>
    </div>
   
    <div class="card2">
        <div class="card-body">
    <p>
        <?= Html::a('+ เพิ่มประกาศหารถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p> </p>
    </div>

    <div class="container">
        <div class=" row row-cols-1 row-cols-md-3">
            <?php foreach ($dataProvider->models as $model) { ?>
              <?= Html::a('<div class="card-group h-100">
                                <div class="card card-loop">
                                  <div class="cardd-job"> 
                                  ' . Html::img($model->getFirstPhotoURLP(), ['class' => 'img-responsive rounded mx-auto d-block job-display-loop']) . '                      
                                  </div>        
                                  <div class="card-body card-body-list">
                                    <h6 class="card-title">' . $model->title . '</h6>
                                    <p class="card-text"> ' . $model->user->company_name . ' </p>
                                    <p class="card-text"><i class="fas fa-map-marker-alt"></i> ' . $model->pickup() . ' ' . $model->deliver() . ' </p>
                                  </div>       
                                </div>
                              </div>', ['job/view', 'id' => $model->job_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
              ?>
            <?php } ?>
        </div>
        <br>
    </div>

    <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่พบข้อมูล !!"; ?>
                </div>
    <?php endif; ?>

  



        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>

        
</div>
