<?php

use common\models\Amphures;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Provinces;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPostSearch */
/* @var $form yii\widgets\ActiveForm */
/* 
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
    </div>
    </div>

    Collapse::widget([
    'items' => [
        // equivalent to the above
        [
            'label' => 'Collapsible Group Item #1',
            'content' => 'Anim pariatur cliche...',
            // open its content by default
            'contentOptions' => ['class' => 'in']
        ],
        // another group item
        [
            'label' => 'Collapsible Group Item #1',
            'content' => 'Anim pariatur cliche...',
            'contentOptions' => [...],
            'options' => [...],
        ],
        // if you want to swap out .panel-body with .list-group, you may use the following
        [
            'label' => 'Collapsible Group Item #1',
            'content' => [
                'Anim pariatur cliche...',
                'Anim pariatur cliche...'
            ],
            'contentOptions' => [...],
            'options' => [...],
            'footer' => 'Footer' // the footer label in list-group
        ],
    ]
]);
*/

$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
$amphures = ArrayHelper::map(Amphures::find()->asArray()->all(), 'id', 'name_th');
?>

<div class="rog-job-post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    
    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;">งานขนส่ง</h1>    
        <div class="card-body">



    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <?=

                $form
                    ->field($model, 'cartype_valid')
                    ->dropdownList($this->context->getCarTypes(), ['prompt' => 'เลือกประเภทรถขนส่ง'])
                    //->checkBoxList($this->context->getCarTypes(), ['itemOptions' => $model->cartypeToArray(), 'separator'=>'<br>'])
                    //->hint(Html::a('คลิกดูประเภทรถ', ['car-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
            ->label(false)
            //->hint('เลือกได้หลายรายการ')

            ?>
        </div>
    </div>

<div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'pickup_province')
                ->dropdownList($provinces, ['prompt' => 'เลือกจังหวัดต้นทาง'])
                ->label(false)
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'deliver_province')
                ->dropdownList($amphures, ['prompt' => 'เลือกจังหวัดปลายทาง']) 
                ->label(false)
            ?>
        </div>



<div class="col-sm-6">
    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block']) ?>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">       
        <?= Html::a(
            'รีเซ็ตค่าค้นหา',
            ['job/index'],
            ['class' => 'btn btn-danger btn-block', 'style' => '']
        ) ?>
    </div>
</div>
    
    </div>
</div>

    <?php ActiveForm::end(); ?>

<!--<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
            <?= Html::a('ลงประกาศ "ฉันมีรถขนส่งให้บริการ"', 
            ['car/my-car'], 
            ['class' => 'btn btn-warning btn-block btn-lg', 'style' => '']) ?>
    </div>
</div>-->


</div>

</div>