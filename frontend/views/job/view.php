<?php

use yii\bootstrap4\Carousel;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPost */


Yii::$app->params['og_description']['content'] = $model->getOGDesc();
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$this->title = $model->title;
Yii::$app->params['og_title']['content'] = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ประกาศหารถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="rog-job-post-view">
    <div class="row">
        <div class="col-lg-9">
            <div class="card2">
                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <h2><strong><?= Html::encode($this->title) ?></strong></h2>
                    </div>
                    <div class="mt-4">

                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">

                                <?php
                                $imagesexplode = explode(",", $model->photos);
                                $photosImage = $model->photos;

                                foreach ($imagesexplode as $key => $loopImage) { ?>

                                <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>

                                <?php } ?>

                            </ol>

                            <div class="carousel-inner">

                                <?php $count = 0;

                                foreach ($imagesexplode as $key => $loopImage) {
                                    $count++ ?>
                                <div class="carousel-item <?= ($count == 1) ? 'active' : '' ?>">
                                    <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/' . $loopImage ?> "
                                        class=" img-fluid rounded mx-auto d-block gImg job-display-mobile">
                                </div>

                                <div id="myModal" class="modal">
                                    <span class="close">×</span>
                                    <img class="modal-content" id="img01">
                                    <div id="caption"></div>
                                </div>
                                <?php } ?>

                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p></p>
                    </div>


                    <div class="row" style="margin-left:10px;">
                        <!-- Your share button code -->
                        <div class="fb-share-button" style="margin-right:10px;"
                            data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                            data-layout="button_count" data-size="large">
                            <a target="_blank"
                                href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                                class="fb-xfbml-parse-ignore">แชร์
                            </a>
                        </div>

                        <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a"
                            data-ver="3" data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>
                            data-color="default" data-size="large" style="display: none;">
                        </div>

                        <button type="button" style="margin-left:10px;"
                            class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip"
                            data-placement="bottom"
                            data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                            title="Copy to clipboard">
                            คัดลอก URL <i class="fas fa-copy"></i>
                        </button>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p></p>
                    </div>





                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p></p>
                    </div>

                    <!--<div class="col-sm-2">
                <?php
                if (!$owner) {
                    echo '<p>';
                    echo Html::a('<i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                    echo '</p>';
                }
                ?>
            </div>-->


                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <?php

                            echo '<p>';
                            echo Html::a(
                                '<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(),
                                ($model->user->tel_1() != '')
                                    ? 'tel:' . $model->user->tel_1()
                                    : ['user/view', 'id' => $model->user_id],
                                ['class' => 'btn btn-primary btn-block btn-sm rounded-pill']
                            );
                            //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                            echo '</p>';

                            ?>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->user->line_id ?>"
                                    class="wpfront-button text-center btn-block btn-sm rounded-pill btn-sm"
                                    style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID :
                                    <?= $model->user->line_id() ?></a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-4">
                <div class="card2">

                    <div class="card-body">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p>
                                <h3><strong>ติดต่อคุณ</strong> : <?= $model->user->name ?></h3>
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p>
                                <h6><strong>รายละเอียดงานขนส่ง / การขึ้น ลงสินค้า</strong></h6> :
                                <?= nl2br($model->full_description) ?>
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p><strong>ประเภทสินค้า / รายละเอียด</strong> : <?= $model->short_description ?></p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p><strong>ประเภทรถขนส่งที่ต้องการ</strong><br><?= $model->cartype_text() ?></p>
                        </div>





                        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <?php
            if (!$owner) {
                echo '<p>';
                echo Html::a('ติดต่อ', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                echo '</p>';
            }
            ?>
        </div>-->



                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p>
                                <h5><strong>ต้นทาง</strong></h5>
                            </p>

                            <p><strong>สถานที่ : </strong><?= $model->pickup_location ?></p>
                            <p><?= $model->location_text($model->pickup_province, $model->pickup_district, $model->pickup_subdistrict) ?>
                            </p>
                            <p><strong>ผู้ติดต่อ : </strong><?= $model->contact_person_pickup ?></p>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p> </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p>
                                <h5><strong>ปลายทาง</strong></h5>
                            </p>

                            <p><strong>สถานที่</strong> : <?= $model->deliver_location ?></p>
                            <p><?= $model->location_text($model->deliver_province, $model->deliver_district, $model->deliver_subdistrict) ?>
                            </p>
                            <p><strong>ผู้ติดต่อ : </strong><?= $model->contact_person_delivery ?></p>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <p> </p>
                        </div>


                        <div class="row" style="margin-left:10px;">
                            <!-- Your share button code -->
                            <div class="fb-share-button" style="margin-right:10px;"
                                data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                                data-layout="button_count" data-size="large">
                                <a target="_blank"
                                    href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                                    class="fb-xfbml-parse-ignore">แชร์
                                </a>
                            </div>

                            <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a"
                                data-ver="3"
                                data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>
                                data-color="default" data-size="large" style="display: none; ">
                            </div>

                            <button type="button" style="margin-left:10px;"
                                class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip"
                                data-placement="bottom"
                                data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
                                title="Copy to clipboard">
                                คัดลอก URL <i class="fas fa-copy"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>

            <div class="card2 d-none d-sm-block d-md-none d-lg-block">
                <div class="card-body">
                    <div align="center">
                        <div class="fb-group" style="margin-bottom:10px;"
                            data-href="https://www.facebook.com/groups/376534166106364/" data-width="280"
                            data-show-social-context="true" data-show-metadata="true">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.col-lg-9 -->

        <div class="col-lg-3">

            <div class="card2">
                <div class="card-body">
                    <div class="list-group">
                        <div class="d-lg-none d-md-none d-sm-none">
                            <?php
                            if ($owner) {
                                echo '<div class="">';

                                echo '<div class="">';
                                echo '<div class="form-group">';
                                echo Html::a('<i class="fas fa-edit"></i> แก้ไข', ['update', 'id' => $model->job_id], ['class' => 'btn btn-primary btn-block btn-sm']);
                                echo '</div>';
                                echo '</div>';

                                echo '<div class="">';
                                echo '<div class="form-group">';
                                echo Html::a('<i class="fas fa-trash-alt"></i> ลบ', ['delete', 'id' => $model->job_id], [
                                    'class' => 'btn btn-danger btn-block btn-sm',
                                    'data' => [
                                        'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                        'method' => 'post',
                                    ],
                                ]);

                                echo '</div>';
                                echo '</div>';

                                echo '</div>';
                            }
                            ?>



                        </div>

                        <div class="d-none d-sm-block">

                            <?php
                            if ($owner) {
                                echo '<div class="">';

                                echo '<div class="">';
                                echo '<div class="list-group-item">';
                                echo Html::a('<i class="fas fa-edit"></i> แก้ไข', ['update', 'id' => $model->job_id], ['class' => '', 'style' => 'text-decoration: none;']);
                                echo '</div>';
                                echo '</div>';

                                echo '<div class="">';
                                echo '<div class="list-group-item">';
                                echo Html::a('<i class="fas fa-trash-alt"></i> ลบ', ['delete', 'id' => $model->job_id], [
                                    'class' => '',
                                    'style' => 'text-decoration: none;',
                                    'data' => [
                                        'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                        'method' => 'post',
                                    ],
                                ]);

                                echo '</div>';
                                echo '</div>';

                                echo '</div>';
                            }
                            ?>


                        </div>
                    </div>

                    <div class="" style="padding-top: 10px;">
                        <?php

                        echo '<p>';
                        echo Html::a(
                            '<i class="fas fa-phone-alt"></i> ' . $model->user->tel_1(),
                            ($model->user->tel_1() != '')
                                ? 'tel:' . $model->user->tel_1()
                                : ['user/view', 'id' => $model->user_id],
                            ['class' => 'btn btn-primary btn-block rounded-pill btn-sm']
                        );
                        //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                        echo '</p>';

                        ?>
                    </div>


                    <div class="">
                        <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->user->line_id ?>"
                                class="wpfront-button text-center btn-sm rounded-pill btn-sm btn-block"
                                style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID :
                                <?= $model->user->line_id() ?></a></p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p></p>
                    </div>

                    <div class=" d-lg-none d-xl-block " align="center">
                        <div class="fb-group d-xl-none" style="margin-top:10px;"
                            data-href="https://www.facebook.com/groups/376534166106364/" data-width="250"
                            data-show-social-context="true" data-show-metadata="true">
                        </div>
                    </div>

                </div>
                <!-- /.col-lg-9 -->

            </div>


        </div>





        <script>
            // COPY TO CLIPBOARD
            // Attempts to use .execCommand('copy') on a created text field
            // Falls back to a selectable alert if not supported
            // Attempts to display status in Bootstrap tooltip
            // ------------------------------------------------------------------------------

            function copyToClipboard(text, el) {
                var copyTest = document.queryCommandSupported('copy');
                var elOriginalText = el.attr('data-original-title');

                if (copyTest === true) {
                    var copyTextArea = document.createElement("textarea");
                    copyTextArea.value = text;
                    document.body.appendChild(copyTextArea);
                    copyTextArea.select();
                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'Copied!' : 'Whoops, not copied!';
                        el.attr('data-original-title', msg).tooltip('show');
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                    document.body.removeChild(copyTextArea);
                    el.attr('data-original-title', elOriginalText);
                } else {
                    // Fallback if browser doesn't support .execCommand('copy')
                    window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
                }
            }

            $(document).ready(function () {
                // Initialize
                // ---------------------------------------------------------------------

                // Tooltips
                // Requires Bootstrap 3 for functionality
                $('.js-tooltip').tooltip();

                // Copy to clipboard
                // Grab any text in the attribute 'data-copy' and pass it to the 
                // copy function
                $('.js-copy').click(function () {
                    var text = $(this).attr('data-copy');
                    var el = $(this);
                    copyToClipboard(text, el);
                });
            });
        </script>



        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            modal.addEventListener('click', function () {
                this.style.display = "none";
            })

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // Get all images and insert the clicked image inside the modal
            // Get the content of the image description and insert it inside the modal image caption
            var images = document.getElementsByTagName('img');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            var i;
            for (i = 0; i < images.length; i++) {
                images[i].onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    modalImg.alt = this.alt;
                    captionText.innerHTML = this.nextElementSibling.innerHTML;
                }
            }
        </script>

    </div>
</div>