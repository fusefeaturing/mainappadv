<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogJobPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประกาศหารถขนส่งของฉัน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-job-post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('+ เพิ่มประกาศหารถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'label' => 'รายการประกาศ',
                'format' => 'raw',
                'value' => function ($model)
                {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';
                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<h4>' . $model->title . ' </h4>';
                        $htmlcard .= '<p><strong>ประเภทรถที่ต้องการ</strong><br>' . $model->cartype_text() . '</p>';
                        //$htmlcard .= '<p>' . $model->short_description . '</p>';
                        //$htmlcard .= '<p>รายละเอียด : ' . $model->full_description . '</p>';
                        $htmlcard .= '<p><strong>ต้นทาง</strong> : '. $model->pickup() .'</p>';
                        $htmlcard .= '<p><strong>ปลายทาง</strong> : '. $model->deliver() .'</p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        $htmlcard .= $model->getFirstPhotos();
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= Html::a('ดูข้อมูล', 
                        ['job/view', 'id' => $model->job_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';
                    
                    $htmlcard .= '</div>';
                    
                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]);
    ?>
</div>
