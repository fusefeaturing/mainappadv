<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPost */

$this->title = 'อัพเดทประกาศ : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ประกาศหารถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-job-post-update">
<div class="card2">
    <div class="card-body">
        <h2><?= Html::encode($this->title) ?></h2>
    </div>
</div>
    

    <?= $this->render('_form', [
        'model' => $model,
        'amphur' => $amphur,
        'district' => $district,
        'damphur' => $damphur,
        'ddistrict' => $ddistrict,
    ]) ?>

</div>
