<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPost */

$this->title = 'สร้างประกาศหารถขนส่ง';
$this->params['breadcrumbs'][] = ['label' => 'ประกาศหารถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-job-post-create">
    <div class="card2">
        <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>

    

    <?= $this->render('_form', [
        'model' => $model,
        'amphur' => [],
        'district' => [],
        'damphur' => [],
        'ddistrict' => [],
    ]) ?>

</div>
