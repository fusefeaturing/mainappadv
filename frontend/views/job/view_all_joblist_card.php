<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogJobPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'งานขนส่ง';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_url']['content'] = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

?>
<div class="rog-job-post-index">

    
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p></p>
    </div>

    <div class="container">
        <div class=" row row-cols-1 row-cols-md-3">
            <?php foreach ($dataProvider->models as $model) { ?>
                <?= Html::a('<div class="card-group h-100">
                                <div class="card card-loop">
                                    <div class="cardd-job"> 
                                    ' . Html::img($model->getFirstPhotoURLP(), ['class' => 'img-responsive rounded mx-auto d-block job-display-loop',]) . '                      
                                    </div>        
                                    <div class="card-body card-body-list">
                                        <h6 class="card-title">' . $model->title . '</h6>
                                        <p class="card-text"> ' . $model->user->company_name . ' </p>
                                        <p class="card-text"><i class="fas fa-map-marker-alt"></i> ' . $model->pickup() . ' ' . $model->deliver() . ' </p>
                                    </div>       
                                </div>
                            </div>', ['job/view', 'id' => $model->job_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                ?>
            <?php } ?>
        </div>
        <br>
    </div>

    <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่พบข้อมูล !!"; ?>
                </div>
    <?php endif; ?>
       
    <div class="card2">
        <div class="card-body">   
            <div class="row" > 
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <p>เพื่อให้ผู้มีงานติดต่อมาหาคุณ</p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                            <?= Html::a('ลงประกาศ "ฉันมีรถขนส่งให้บริการ"', 
                            ['car/my-car'], 
                            ['class' => 'btn btn-warning btn-block btn-sm', 'style' => '']) ?>
                </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p> </p>
                    </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12"> 
        
                        <?= Html::a(
                            '<i class="fab fa-facebook-square"></i> Rogistic.com',
                            'https://www.facebook.com/Rogisticcom-359767341103161',
                            ['class' => 'btn btn-primary  btn-block btn-sm']
                     
                            )
                        ?>
    
                </div>
            </div>     
        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>

</div>