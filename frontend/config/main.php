<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'on beforeRequest' => function ($event) {
    if(!Yii::$app->request->isSecureConnection){
        $url = Yii::$app->request->getAbsoluteUrl();
        $url = str_replace('http:', 'https:', $url);
        $url = str_replace('www.', '', $url);
        Yii::$app->getResponse()->redirect($url);
        Yii::$app->end();
    }
},
    
    'components' => [

        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,

        ],
         
         //host     
        /*'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '407866350019570',
                    'clientSecret' => '3ebff05317c1b684165c1f9c7f7ffbcf',
                ],
            ],
        ],*/



        //localhost
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '207111170376109',
                    'clientSecret' => '02f21b647a4eb94f3f62c59d9516084f',
                ],
            ],
        ],

        'user' => [
            'identityClass' => 'common\models\RogUser',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
			'baseUrl' => $baseUrl,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [  '<controller:\w+>/<id:\d+>' => '<controller>/view',

            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',  			

                    ],
		]
    
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    
    'params' => $params,
];
