<?php
return [
    'adminEmail' => 'admin@example.com',
    'bsVersion' => '4.x',
    'og_title' => ['property' => 'og:title', 'content' => 'Rogistic.com เว็บศูนย์รวมงานขนส่ง'],
    'og_description' => ['property' => 'og:description', 'content' => '/'],
    'og_url' => ['property' => 'og:url', 'content' => 'https://www.rogistic.com/'],
    'og_image' => ['property' => 'og:image', 'content' => ''],
    'og_robots' => ['property' => 'og:robots', 'content' => 'index, follow, all'],
    'og_revisit-after' => ['property' => 'og:revisit-after', 'content' => '3 Days'],
];
