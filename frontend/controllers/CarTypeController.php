<?php

namespace frontend\controllers;

use Yii;

use common\models\RogCarType;
use common\models\RogCarTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use GuzzleHttp\Psr7\UploadedFile;
use yii\data\Pagination;

/**
 * CarTypeController implements the CRUD actions for RogCarType model.
 */
class CarTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RogCarType models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if (Yii::$app->user->isGuest) { return $this->redirect(['site/login']); }

        $searchModel = new RogCarTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort(['defaultOrder' => ['car_type_level' => SORT_ASC]]);

        $cartypeloops = RogCarType::find();
        $pagination = new Pagination([
            'totalCount' => $cartypeloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $cartypeloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $cartypeloops->orderBy('car_type_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'templatebutton' => '{view}',
        ]);
    }

    public function actionAdmin()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $searchModel = new RogCarTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort(['defaultOrder' => ['index_sort' => SORT_ASC]]);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'templatebutton' => '{view} {update} {delete}',
        ]);
    }

    /**
     * Displays a single RogCarType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RogCarType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RogCarType();
        

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->picture = $model->upload($model, 'picture');
            $model->save();
            return $this->redirect(['view', 'id' => $model->car_type_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RogCarType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->picture = $model->upload($model, 'picture');
            $model->save();
            return $this->redirect(['view', 'id' => $model->car_type_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RogCarType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogCarType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RogCarType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RogCarType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
