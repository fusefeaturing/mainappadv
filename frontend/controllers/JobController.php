<?php

namespace frontend\controllers;

use Yii;
use common\models\RogJobPost;
use common\models\RogJobPostSearch;


use common\models\Amphures;
use common\models\Districts;
use common\models\RogCarDescription;
use common\models\RogCarType;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * JobController implements the CRUD actions for RogJobPost model.
 */
class JobController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all RogJobPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new RogJobPost();
        $searchModel = new RogJobPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dbug = Yii::$app->request->queryParams;

        $jobloops = RogJobPost::find();
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        //echo var_dump($dbug); exit();
        //$dataProvider->query->where(['is_job_appear' => '0']);

        return $this->render('view_all_joblist_card', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }

    public function actionOldIndex()
    {
        $searchModel = new RogJobPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    protected function isAdmin($userid)
    {
        $adminId = [1, 2, 3, 4, 9, 10];
        return in_array($userid, $adminId);
    }

    public function actionAdmin()
    {
        $user_id = Yii::$app->user->id;

        if (!$this->isAdmin($user_id)) {
            return $this->redirect(['site/login']);
        }

        $searchModel = new RogJobPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMyJob()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->id;

        $searchModel = new RogJobPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $jobloops = RogJobPost::find();
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        //echo var_dump($dbug); exit();
        //$dataProvider->query->where(['is_job_appear' => '0']);

        return $this->render('myjob', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Displays a single RogJobPost model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = RogJobPost::findOne($id);
        $owner = false;

        $user_id = Yii::$app->user->id;
        if ($model->user_id == $user_id) {
            $owner = true;
        }

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,
        ]);
    }

    /**
     * Creates a new RogJobPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            echo 'please login';
            return $this->redirect(['site/login']);
        }

        $model = new RogJobPost();

        //if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($model->load(Yii::$app->request->post())) {
            if ($model->cartype_valid) {
                $model->cartype_valid = implode(",", $model->cartype_valid);
            }

            if ($model->validate()) {
                $model->photos = $model->uploadMultiple($model, 'photos');
            }

            $model->save();
            //return $this->redirect(['view', 'id' => $model->job_id]);
            return $this->redirect(['job/my-job']);
        }

        return $this->render('create', [
                'model' => $model,
            ]
        );
    }

    /**
     * Updates an existing RogJobPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = $this->findModel($id);
       

        $user_id = Yii::$app->user->id;
        if ($model->user_id != $user_id) {
            return $this->redirect(['view', 'id' => $model->job_id]);
        }

        $oldimage = $model->photos;
        $imagesexplode = explode(",", $oldimage);

        //var_dump($oldimage);
        //echo "<br>";
        //die();

        $amphur = ArrayHelper::map($this->getAmphur($model->pickup_province), 'id', 'name');
        $district = ArrayHelper::map($this->getDistrict($model->pickup_district), 'id', 'name');

        $damphur = ArrayHelper::map($this->getAmphur($model->deliver_province), 'id', 'name');
        $ddistrict = ArrayHelper::map($this->getDistrict($model->deliver_district), 'id', 'name');
       
        if ($model->load(Yii::$app->request->post())) {
            $model->photos = $model->uploadMultiple($model, 'photos');

            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->cartype_valid) {
                $model->cartype_valid = implode(",", $model->cartype_valid);
            }

            if ($model->validate()) {
                
                $model->photos = $model->uploadMultiple($model, 'photos');
                
                $photo = $model->photos;
               
                //echo $photo;
                //die();

                if ( $photo != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
               
                } else if ($photo == $oldimage) {

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                }
            }

        }

        return $this->render('update', [
            'model' => $model,
            'amphur' => $amphur,
            'district' => $district,
            'damphur' => $damphur,
            'ddistrict' => $ddistrict,
        ]);
    }

    /**
     * Deletes an existing RogJobPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
       
        $user_id = Yii::$app->user->id;
        $model = RogJobPost::findOne($id);
        $images = explode(",", $model->photos);


        if ($model->photos != null) {
            
            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }
            
            $this->findModel($id)->delete();
        } else {
           
            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }
        
        

        return $this->redirect(['job/my-job' , 'id' => $user_id]);
    }

    /**
     * Finds the RogJobPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RogJobPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RogJobPost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetAmphur()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphur($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }  

    public function actionGetDistrict()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphur_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getDistrict($amphur_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphur($id)
    {
        $datas = Amphures::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'id', 'name_th');
    }

    protected function getDistrict($id)
    {
        $datas = Districts::find()->where(['amphure_id' => $id])->all();
        return $this->MapData($datas, 'id', 'name_th');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }


    public function getCarTypes()
    {
        if (($model = RogCarDescription::find()) !== null) {
            return ArrayHelper::map(
                RogCarDescription::find()
                ->orderBy([
                    'car_des_level' => SORT_ASC
                ])
                
                ->all(),
                'car_des_id',
                function ($model) {
                    if ( $model['type_wheel_id'] == $model['car_des_name']) return $model['type_wheel_id'];
                    else return $model->typewheel->type_wheel_name . ' - ' . $model->car_des_name;
                }
            );
        }

        return array(-1, 'No car data');
    }


}
