<?php

namespace frontend\controllers;

use Yii;
use common\models\RogCarType;
use common\models\RogUser;
use common\models\RogUserCar;
use common\models\RogUserCarSearch;
use common\models\RogUserRoute;
use common\models\RogTypeWheel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use common\models\Provinces;
use common\models\Amphures;
use common\models\RogCarDescription;
use yii\data\Pagination;
use yii\helpers\Json;

/**
 * CarController implements the CRUD actions for RogUserCar model.
 */
class CarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all RogUserCar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new RogUserCar();
        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $cardes = ArrayHelper::map($this->getCardes($model->car_des), 'id', 'name');
        $amphur = ArrayHelper::map($this->getAmphur($model->pickup_province), 'id', 'name');
        

        $carloops = RogUserCar::find();
        $pagination = new Pagination([
            'totalCount' => $carloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $carloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $carloops->orderBy('car_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/


        return $this->render('view_all_carlist_card', [
            'model' => $model,
            'amphur' => $amphur,
            'cardes' => $cardes,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    protected function isAdmin($userid)
    {
        $adminId = [1, 2, 3, 4, 9, 10];
        return in_array($userid, $adminId);
    }

    public function actionAdmin()
    {
        $user_id = Yii::$app->user->id;

        if (!$this->isAdmin($user_id)) {
            return $this->redirect(['site/login']);
        }

        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    public function actionMyCar()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->id;

        $userpermit = $this->getUserPermitData($user_id);

        $carloops = RogUserCar::find()
        ->where(['in', 'user_id', $user_id]);
        $pagination = new Pagination([
            'totalCount' => $carloops->count(),
            'defaultPageSize' => 27
        ]);

    /*$countQuery =  clone $carloops;
    $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
    $modelsLimits = $carloops->orderBy('car_search_level')
        ->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();
    /*var_dump($countQuery);
    die();*/

        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            RogUserRoute::find()
                ->where(['in', 'user_id', $user_id])
                ->orderBy(['province' => SORT_ASC])
                ->asArray()
                ->all(),
            'amphur',
            'province'
        );

        $provinces = ArrayHelper::map(
            Provinces::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $this->render('mycarservice', [
            'searchModel' => $searchModel,
            'userpermit' => $userpermit,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
            'provinces' => $provinces,
            'amphures' => $amphures,
            'serviceroutes' => $serviceroutes,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }

    public function actionCarcompany($id)
    {
        $model = RogUser::findOne($id);
        $owner = false;
        $user_id = Yii::$app->user->id;
        $user_id = Yii::$app->user->id;
        if ($model->user_id == $user_id) {
            $owner = true;
        }
        
        /*var_dump($user_id);
        die();*/

        $request = Yii::$app->request;
        $users_id = $request->post('user_id');

        /*var_dump($id);
        die();*/

        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $id]);

            $carloops = RogUserCar::find()
            ->where(['in', 'user_id', $id]);
            
            $pagination = new Pagination([
                'totalCount' => $carloops->count(),
                'defaultPageSize' => 27
            ]);
    
        /*$countQuery =  clone $carloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $carloops->orderBy('car_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/


        return $this->render('car_company', [
            'model' => $model,
            'owner' => $owner,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    /**
     * Displays a single RogUserCar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $owner = false;

        $user_id = Yii::$app->user->id;
        if ($model->user_id == $user_id) {
            $owner = true;
        }

        return $this->render('view', [
            'model' => $model,
            'cartypes' => $this->getCarTypes(),
            'owner' => $owner,
        ]);
    }

    /**
     * Creates a new RogUserCar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new RogUserCar();
        $user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->pic_car = $model->uploadMultiple($model, 'pic_car');
            }

            $model->save();
            
            //return $this->redirect(['view', 'id' => $model->car_id]);
            return $this->redirect(['user/profile' , 'id' => $user_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    /**
     * Updates an existing RogUserCar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = $this->findModel($id);

        

        $user_id = Yii::$app->user->id;

        if ($model->user_id != $user_id) {
            return $this->redirect(['view', 'id' => $model->car_id]);
        }

        
        $cardes = ArrayHelper::map($this->getCardes($model->type_wheel), 'id', 'name');

        
            /*var_dump($model->pic_side);
            die();*/
            $oldimage = $model->pic_car;
            $imagesexplode = explode(",", $oldimage);
           

        if ($model->load(Yii::$app->request->post())) {
            

            if ($model->validate()) {
                

                $model->pic_car = $model->uploadMultiple($model, 'pic_car');
                

                $photo = $model->pic_car;
               
                //echo $photo;
                //die();

                if ( $photo != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->car_id]);
                    }
               
                } else if ($photo == $oldimage) {

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->car_id]);
                    }
                }
            }

        }

            //return $this->redirect(['view', 'id' => $model->car_id]);
            
        return $this->render('update', [
            'model' => $model,
            'cartypes' => $this->getCarTypes(),           
            'cardes' => $cardes,
        ]);
    }

    /**
     * Deletes an existing RogUserCar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)    
    {
        $user_id = Yii::$app->user->id;
        $model = RogUserCar::findOne($id);
        
        $images = explode(",", $model->pic_car);
        
            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->pic_car != null) {
            
                foreach ($images as $image) {
                    unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
                }
                
                $this->findModel($id)->delete();
            } else {
               
                $this->findModel($id)->delete();
                //echo 'ไม่สามารถลบข้อมูลได้';
                //die();
            }


        return $this->redirect(['user/profile', 'id' => $model->user_id]);
    }

    /**
     * Finds the RogUserCar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RogUserCar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RogUserCar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getCarTypes()
    {
        if (($model = RogCarType::find()) !== null) {
            return ArrayHelper::map(
                RogCarType::find()->orderBy(['index_sort' => SORT_ASC])->asArray()->all(),
                'car_type_id',
                function ($model) {
                    if ($model['name'] == $model['description']) return $model['name'];
                    else return $model['name'] . ' - ' . $model['description'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    

    public function getCarTypes2(){
        $cartype = ArrayHelper::map(RogCarType::find()->all(),'car_type_id','name');;
        $cartypeSelected = explode(',', $this->getCarTypes());
        $cartypeSelectedName = [];
        foreach ($cartype as $key => $cartypeName) {
          foreach ($cartypeSelected as $cartypeKey) {
            if($key === $cartypeKey){
              $cartypeSelectedName[] = $cartypeName;
            }
          }
        }
    
        return explode(', ', $cartypeSelectedName);
    }
    protected function findUserRoute($user_id)
    {
        if (($model = RogUserRoute::find()
            ->where(['in', 'user_id', $user_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphuresByProvinceID($provinceID)
    {
        $amphures = ArrayHelper::map(
            Amphures::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $amphures;
    }

    public function getAmphuresByProvinceName($provinceName)
    {
        $province = Provinces::find()
            ->where(['in', 'name_th', $provinceName])
            ->one();

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->where(['in', 'province_id', $province->id])
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $amphures;
    }

    public function actionRoute()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->id;

        $dataProvider = new ActiveDataProvider([
            'query' => RogUserRoute::find()
                ->where(['in', 'user_id', $user_id]),
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);

        $provinces = ArrayHelper::map(
            Provinces::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $this->render('route', [
            //'model' => $this->findUserRoute($user_id),
            'dataProvider' => $dataProvider,
            'provinces' => $provinces,
            'amphures' => $amphures,
        ]);
    }

    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
        $model = RogUserRoute::findOne($id);
        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedprovinces = $request->post('selectedprovinces');
            $selectedamphur = $request->post('selectedamphur');
            $user_id = $request->post('user_id');

            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphur';

                $provinces = ArrayHelper::map(
                    Provinces::find()
                        ->where(['in', 'id', $selectedprovinces])
                        ->asArray()
                        ->all(),
                    'id',
                    'name_th'
                );

                $amphures = ArrayHelper::map(
                    Amphures::find()
                        ->where(['in', 'province_id', $selectedprovinces])
                        ->asArray()
                        ->all(),
                    'id',
                    'name_th'
                );

                $amphIDs = ArrayHelper::map(
                    Amphures::find()
                        ->asArray()
                        ->all(),
                    'id',
                    'id'
                );

                $selectedamphur = $amphIDs;
            }

            if ($request->post('step') == 'amphurselector') {
                //save

                $userid = Yii::$app->user->id;
                $model = RogUserRoute::findOne($id);
                $this->saveUserRoute($user_id, $selectedamphur);
                //redirect
                //return $this->redirect(['car/route']);
                /*print_r($id);
                die();*/
                return $this->redirect(['user/profile' , 'id' => $userid]);
            }

            return $this->render($redirect, [
                'provinces' => $provinces,
                'amphures' => $amphures,
                'selectedprovinces' => $selectedprovinces,
                'selectedamphur' => $selectedamphur,
                
                'model' => $model,
            ]);
        } else {
            $selectedprovinces = [];

            return $this->render('select_province', [
                'provinces' => $provinces,
                'selectedprovinces' => $selectedprovinces,
            ]);
        }
    }

    protected function saveUserRoute($user_id, $amphurids)
    {
        //delete old
        $old_user_route = RogUserRoute::find()
            ->where(['in', 'user_id', $user_id])
            ->all();

        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }

        //find all
        $amphures = Amphures::find()
            ->where(['in', 'id', $amphurids])
            ->all();

        //add new
        foreach ($amphures as $amphur) {
            $new_route = new RogUserRoute();
            $new_route->user_id = $user_id;
            $new_route->province = $amphur->province_id;
            $new_route->amphur = $amphur->id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            $new_route->save();

            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }

    protected function getUserPermitData($id)
    {
        if (($model = RogUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetCardes()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $typewheel = $parents[0];
                $out = $this->getCardes($typewheel);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    } 

    protected function getCardes($id)
    {
        $datas = RogCarDescription::find()->where(['type_wheel_id' => $id])->all();
        return $this->MapData($datas, 'car_des_id', 'car_des_name');
    }

    
    public function actionGetAmphur()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphur($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }



    protected function getAmphur($id)
    {
        $datas = Amphures::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'id', 'name_th');
    }   




    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getwheelTypes()
    {
        if (($model = RogTypeWheel::find()) !== null) {
            return ArrayHelper::map(
                RogTypeWheel::find()
                ->orderBy([
                    'type_wheel_level' => SORT_ASC
                ])
                ->asArray()
                ->all(),
                'type_wheel_id',

                function ($model) {
                    if ($model['type_wheel_name'] ) return $model['type_wheel_name'];
                    else return $model['type_wheel_name'];
                }
            );
        }
        return array(-1, 'No car data');
    }

    public static function getCarSearch() {
        $options = [];

        $parents = RogTypeWheel::find()->orderBy('type_wheel_level')->all();
        //$parents = MhJobSubPt::find()->where('job_pt_id')->orderBy('job_pt_id')->all();
        foreach($parents as $id => $p) {
            $children = RogCarDescription::find()->orderBy('car_des_level')->where('type_wheel_id=:type_wheel_id', [':type_wheel_id'=>$p->type_wheel_id])->all();
            $child_options = [];
            foreach($children as $child) {
                $child_options[$child->type_wheel_id] = $child->car_des_name;
            }
            //$options[$p->jobPt->job_pt_name] = $child_options;
            $options[$p->type_wheel_name] = ArrayHelper::map($children, 'car_des_id', 'car_des_name');
        }
        return $options;
    }
}
