<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\User;
use common\models\RogUser;

use yii\web\Response;;




/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    public function oAuthSuccess($client)
    {
        // get user data from client
        /*$userAttributes = $client->getUserAttributes();
  
        //echo "FACEBOOK ID :".$userAttributes["id"];
        //var_dump($userAttributes);
  
        $user = new User;//User::findByUsername("admin");
  
        $user->username = $userAttributes["email"];
        $user->id = "100";//$userAttributes["id"];
        $user->password = "";//"admin";
        $user->authKey = "";// "test100key";
        $user->accessToken = "";// "100-token";
  
        Yii::$app->user->login($user, 3600*24*30);*/

        // do some thing with user data. for example with $userAttributes['email']

        $unit_debug = false;
        Yii::$app->user->logout();

        // get user data from client
        $userAttributes = $client->getUserAttributes();

        if ($unit_debug) {
            echo '<pre>';
            var_dump($userAttributes);
            echo '</pre>';
        }

        if (empty($userAttributes['email'])) {
            Yii::$app->session->setFlash('error', 'กรุณากด Allow Access ใน Facebook เพื่อใช้งาน Facebook Login');
            return $this->redirect('/site/login');
        }

        //$user = RogUser::findOne(['email' => $userAttributes['email']]);
        $user = RogUser::findOne(['facebook_id' => $userAttributes['id']]);
        if ($user) {
            echo 'Welcome back';
            Yii::$app->getUser()->login($user);

            if ($unit_debug) {
                echo '<pre>';
                var_dump($user);
                echo '</pre>';
                exit();
            }
        } else {
            //echo 'New User';
            //getusername from mail
            $uname = explode("@", $userAttributes['email']);
            $getuser = RogUser::findOne(['username' => $uname[0]]);
            if ($getuser) {
                //echo 'dupe username';
                $rand = rand(10, 99);
                $username = $uname[0] . $rand;
            } else {
                //echo 'usable username';
                $username = $uname[0];
            }

            /*
              'company_name' => 'ชื่อกิจการ', 
              'company_address' => 'ที่อยู่กิจการ',
              'company_pic' => 'ภาพกิจการ (4 ภาพ)',
              'pic_idcard' => 'ภาพบัตรประชาชน', 
              'pic_carlic' => 'ภาพใบขับขี่', 
              'pic_self' => 'ภาพถ่ายหน้าตรงถือใบขับขี่', 
              'facebook_token' => 'Facebook ส่วนตัว (URL)',
              'citizen_id' => 'เลขประจำตัวประชาชน',
              'carlic_id' => 'เลขใบขับขี่', 
              'prefix' => 'คำนำหน้า',
              'middlename' => 'ชื่อกลาง / ชื่อเล่น',
              'dob' => 'วันเกิด',
              'website' => 'Website',
              'line_id' => 'Line ID',
              'tel_2' => 'เบอร์ติดต่อ 2',
              'is_user_findcar' => 'ใช้บริการค้นหารถขนส่ง',
              'is_user_car_owner' => 'ให้บริการรถขนส่งของฉัน',
              'is_user_verified' => 'ยืนยันตัวตน',
  */

            //echo $username;
            $new_user = new RogUser();
            $new_user->username = $username;
            $new_user->password = Yii::$app->security->generatePasswordHash($username);
            $new_user->email = $userAttributes['email'];
            $new_user->email_contact = $userAttributes['email'];
            $new_user->facebook_id = $userAttributes['id'];
            $new_user->is_user_verified = 0;
            $new_user->created_date = date('Y-m-d H:i:s');
            $new_user->updated_date = date('Y-m-d H:i:s');

            $new_user->tel_1 = '';



            if ($new_user->save()) {
                $name = explode(" ", $userAttributes['name']);
                $new_user->name = $name[0];
                $new_user->surname = $name[1];
                $new_user->save();
                Yii::$app->getUser()->login($new_user);
            } else {
                echo 'Register Failed';
            }

            if ($unit_debug) {
                echo '<pre>';
                var_dump($new_user);
                echo '</pre>';
                exit();
            }
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new RogUser();

        return $this->render('index',[
            'model' => $model,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionInvite()
    {
        return $this->render('invite');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
