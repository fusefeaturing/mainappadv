<?php

namespace frontend\controllers;

use Yii;
use common\models\RogUser;
use common\models\RogUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\RogCarType;
use common\models\RogUserCarSearch;
use yii\helpers\ArrayHelper;
use common\models\RogUserRoute;
use common\models\Provinces;
use common\models\Amphures;
use common\models\Districts;
use common\models\RogJobPost;
use common\models\RogJobPostSearch;
use common\models\RogUserCar;
use yii\data\Pagination;
use yii\helpers\Json;

/**
 * UserController implements the CRUD actions for RogUser model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all RogUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user = Yii::$app->user->identity;

        return $this->render('profile', [
            'model' => $this->findModel($user->id),
        ]);
    }

    protected function isAdmin($userid)
    {
        $adminId = [1, 2, 3, 4, 9, 10];
        return in_array($userid, $adminId);
    }

    public function actionAdmin()
    {
        $user_id = Yii::$app->user->id;

        if (!$this->isAdmin($user_id)) {
            return $this->redirect(['site/login']);
        }

        $searchModel = new RogJobPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionProfile($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user = Yii::$app->user->identity;
        $id = Yii::$app->user->id;




        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $id]);

        $serviceroutes = ArrayHelper::map(
            RogUserRoute::find()
                ->where(['in', 'user_id', $id])
                ->orderBy(['province' => SORT_ASC])
                ->asArray()
                ->all(),
            'amphur',
            'province'
        );

        $provinces = ArrayHelper::map(
            Provinces::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $carloops = RogUserCar::find()
            ->where(['in', 'user_id', $id]);

        $pagination = new Pagination([
            'totalCount' => $carloops->count(),
            'defaultPageSize' => 9
        ]);

        /*$countQuery =  clone $carloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $carloops->orderBy('car_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($id);
            die();*/

        return $this->render('profile', [
            'model' => $this->findModel($user->id),

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
            'provinces' => $provinces,
            'amphures' => $amphures,
            'serviceroutes' => $serviceroutes,
            'carloops' => $carloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,

        ]);
    }


    /**
     * Displays a single RogUser model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);

        $user = Yii::$app->user->identity;



        $user_id = Yii::$app->user->id;

        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            RogUserRoute::find()
                ->where(['in', 'user_id', $id])
                ->orderBy(['province' => SORT_ASC])
                ->asArray()
                ->all(),
            'amphur',
            'province'
        );

        $provinces = ArrayHelper::map(
            Provinces::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        if (Yii::$app->user->isGuest) {
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
            'provinces' => $provinces,
            'amphures' => $amphures,
            'serviceroutes' => $serviceroutes,
        ]);
    }

    /**
     * Creates a new RogUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = new RogUser();

        if ($model->load(Yii::$app->request->post())) {


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }

            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionRegister()
    {

        $model = new RogUser();

        if ($model->load(Yii::$app->request->post())) {


            if ($model->save()) {
                return $this->redirect(['site/login']);
            }

            return $this->redirect(['site/login']);
        }

        return $this->render('_form_general', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateOld($id)
    {
        $model = $this->findModel($id);

        $user_id = Yii::$app->user->id;
        if ($model->user_id != $user_id) {
            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->company_pic = $model->uploadMultiple($model, 'company_pic');
            }
            if ($model->save()) {
                return $this->redirect(['profile', 'id' => $model->user_id]);
            }

            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $user_id = Yii::$app->user->id;
        if ($model->user_id != $user_id) {
            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        return $this->render('update_general', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateService($id)
    {
        $model = $this->findModel($id);

        $user_id = Yii::$app->user->id;
        if ($model->user_id != $user_id) {
            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        $amphure = ArrayHelper::map($this->getAmphure($model->province_id), 'id', 'name');
        $district = ArrayHelper::map($this->getDistrict($model->amphure_id), 'id', 'name');
        $zipcode = ArrayHelper::map($this->getZipcode($model->amphure_id), 'id', 'name');

        $oldimage = $model->company_pic;
        $imagesexplode = explode(",", $oldimage);



        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->company_pic = $model->uploadMultiple($model, 'company_pic');

                $company_pic = $model->company_pic;

                if ($company_pic != $oldimage) {

                    if ($oldimage == null) {
                        $model->save();
                        return $this->redirect(['profile', 'id' => $model->user_id]);
                    }

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }

                    if ($model->save()) {
                        return $this->redirect(['profile', 'id' => $model->user_id]);
                    }
                } else if ($company_pic == $oldimage) {

                    if ($model->save()) {
                        return $this->redirect(['profile', 'id' => $model->user_id]);
                    }
                }
            }

            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        return $this->render('update_service', [
            'model' => $model,
            'amphure' => $amphure,
            'district' => $district,
            'zipcode' => $zipcode,
        ]);
    }

    /**
     * Deletes an existing RogUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $user_id = Yii::$app->user->id;
        $model = RogUser::findOne($id);
        $images = explode(",", $model->company_pic);


        if ($model->company_pic != null) {

            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }

            $this->findModel($id)->delete();
        } else {

            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }



        return $this->redirect(['job/my-job', 'id' => $user_id]);
    }

    /**
     * Finds the RogUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RogUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RogUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    /*    
    public function actionChangepassword()
    {
        $user = Yii::$app->user->identity;
        $loadedPost = $user->load(Yii::$app->request->post());

        if($loadedPost && $user->validate())
        {
            $user->password = $user->newPassword;
            $user->save(false);
            
        }
    }
*/

    public function getCarTypes()
    {
        if (($model = RogCarType::find()) !== null) {
            return ArrayHelper::map(
                RogCarType::find()->orderBy(['index_sort' => SORT_ASC])->asArray()->all(),
                'car_type_id',

                function ($model) {
                    if ($model['name'] == $model['description']) return $model['name'];
                    else return $model['name'] . ' - ' . $model['description'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function actionGetAmphure()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphure($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetDistrict()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphure_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getDistrict($amphure_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetZipcode()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphure_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getZipcode($amphure_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphure($id)
    {
        $datas = Amphures::find()->where(['province_id' => $id])->orderBy('name_th')->all();
        return $this->MapData($datas, 'id', 'name_th');
    }

    protected function getDistrict($id)
    {
        $datas = Districts::find()->where(['amphure_id' => $id])->groupBy('name_th')->all();
        return $this->MapData($datas, 'id', 'name_th');
    }

    protected function getZipcode($id)
    {
        $datas = Districts::find()->where(['amphure_id' => $id])->groupBy('zip_code')->all();
        return $this->MapData($datas, 'id', 'zip_code');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }
}
