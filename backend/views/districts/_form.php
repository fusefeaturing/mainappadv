<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Amphures;


/* @var $this yii\web\View */
/* @var $model common\models\Districts */
/* @var $form yii\widgets\ActiveForm */
$amphures = ArrayHelper::map(Amphures::find()->asArray()->all(), 'id', 'name_th');
?>



<div class="districts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip_code')->textInput() ?>

    <?= $form->field($model, 'name_th')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>


    <?=
                $form
                    ->field($model, 'amphure_id')
                    ->label('ชื่ออำเภอ')
                    ->dropdownList($amphures, [
                        'id' => 'ddl-damphures',
                        'prompt' => 'เลือกอำเภอ'
                    ])
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
