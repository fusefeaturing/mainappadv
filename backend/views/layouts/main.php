<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);


        if (Yii::$app->user->isGuest) {

            $menuItems[] = ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']];
        } else {

            $menuItems[] = ['label' => 'การจัดการรถของผู้ใช้', 'url' => ['/car']];
            $menuItems[] = ['label' => 'การจัดการข้อมูลงานขนส่ง', 'url' => ['/job']];

            $menuItems[] = [
                'label' => ('การจัดการข้อมูลพื้นฐาน'),
                'items' => [
                    ['label' => ('การจัดการข้อมูลผู้ใช้งาน'), 'url' => ['/user']],
                    ['label' => ('การจัดการข้อมูลผู้ใช้งานเบื้องหลัง'), 'url' => ['/backend-login']],
                    ['label' => ('การจัดการข้อมูลเส้นทางขนส่ง'), 'url' => ['/route']],
                    ['label' => ('จัดการหน้าประเภทรถ'), 'url' => ['/cartype']],
                    ['label' => ('จัดการประเภทรถ'), 'url' => ['/rog-type-wheel']],
                    ['label' => ('จัดการชนิดรถ'), 'url' => ['/rog-car-description']],
                    ['label' => ('ระดับภูมิภาค'), 'url' => ['/geographies']],
                    ['label' => ('ระดับจังหวัด'), 'url' => ['/provinces']],
                    ['label' => ('ระดับอำเภอ'), 'url' => ['/amphures']],
                    ['label' => ('ระดับตำบล'), 'url' => ['/districts']],
                ],
            ];
            $menuItems[] = '<li>'

                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ออกจากระบบ (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>