<?php

use common\models\Provinces;
use common\models\RogCarType;
use common\models\RogTypeWheel;
use common\models\RogUser;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\RogJobPost */
/* @var $form yii\widgets\ActiveForm */

$id = yii::$app->user->identity->id;
$roguser = ArrayHelper::map(RogUser::find()->asArray()->all(), 'user_id', 'username');
$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
$typewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$rogcartype = ArrayHelper::map(RogCarType::find()->asArray()->all(), 'car_type_id', 'name');

?>

<div class="rog-job-post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= '' // $form->field($model, 'user_id')->textInput()
    ?>



    <?=
        $form
            ->field($model, 'user_id')
            ->label('ชื่อผู้ใช้งาน')
            ->dropdownList($roguser, [
                'id' => 'ddl-roguser',
                'prompt' => 'เลือกผู้ใช้งาน',
            ])
    ?>

    <?=
        $form
            ->field($model, 'job_search_level')
            ->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
            //->input('', ['placeholder' => ""])
            ->label()
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person_pickup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person_delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'photos[]')->fileInput(['multiple' => true]) ?>
    <div class="well">
        <?= $model->getPhotosViewerback(); ?>
    </div>



    <!--<?=
            $form
                ->field($model, 'cartype_valid')
                ->label('ประเภทรถขนส่งที่ต้องการ')
                ->dropdownList($rogcartype, [
                    'id' => 'ddl-rogcartype',
                    'prompt' => 'เลือกประเภทรถ',
                ])
        ?>-->


    <hr>

    <?= $form->field($model, 'cartype_valid')->checkboxList($model->getItemSkill())->label(false) ?>

    <h3 style="color:red;">* เลือกประเภทรถขนส่งที่ต้องการ</h3>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'cartype_valid')
                    ->checkBoxList(
                        $this->context->getCarTypes(),
                        ['itemOptions' => $model->cartypeToArray(), 'separator' => '<br>']
                    )
                    ->hint(Html::a('คลิกดูประเภทรถ', ['car-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
            ?>
        </div>
    </div>




    <?= $form
        ->field($model, 'deliver_date')
        ->widget(DatePicker::classname(), [
            'language' => 'th',
            'dateFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                'changeMonth' => true,
                'changeYear' => true,
            ],
            'options' => ['class' => 'form-control'],
        ])
    ?>


    <?= $form->field($model, 'pickup_location')->textInput(['maxlength' => true]) ?>

    <hr>

    <h3>ต้นทาง</h3>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_province')
                    ->label('* จังหวัดต้นทาง', ['class' => '', 'style' => 'color:red'])
                    ->dropdownList($provinces, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด',
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_district')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-amphur'],
                        'data' => $amphur,
                        'pluginOptions' => [
                            'depends' => ['ddl-province'],
                            'placeholder' => 'เลือกอำเภอ...',
                            'url' => Url::to(['/job/get-amphur']),
                        ],
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'pickup_subdistrict')
                    ->widget(DepDrop::classname(), [
                        'data' => $district,
                        'pluginOptions' => [
                            'depends' => ['ddl-province', 'ddl-amphur'],
                            'placeholder' => 'เลือกตำบล...',
                            'url' => Url::to(['/job/get-district']),
                        ],
                    ]);
            ?>
        </div>


    </div>

    <hr>

    <h3>ปลายทาง</h3>


    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_province')
                    ->label('* จังหวัดปลายทาง', ['class' => '', 'style' => 'color:red'])
                    ->dropdownList($provinces, [
                        'id' => 'ddl-dprovince',
                        'prompt' => 'เลือกจังหวัด',
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_district')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-damphur'],
                        'data' => $damphur,
                        'pluginOptions' => [
                            'depends' => ['ddl-dprovince'],
                            'placeholder' => 'เลือกอำเภอ...',
                            'url' => Url::to(['/job/get-amphur']),
                        ],
                    ]);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'deliver_subdistrict')
                    ->widget(DepDrop::classname(), [
                        'data' => $ddistrict,
                        'pluginOptions' => [
                            'depends' => ['ddl-dprovince', 'ddl-damphur'],
                            'placeholder' => 'เลือกตำบล...',
                            'url' => Url::to(['/job/get-district']),
                        ],
                    ]);
            ?>
        </div>
    </div>
    <hr>

    <?= $form->field($model, 'deliver_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s'),
    ]) ?>

    <?= $form->field($model, 'updated_date')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s'),
    ]) ?>


    <?= $form->field($model, 'is_job_taken')->textInput() ?>

    <?= $form->field($model, 'is_period_job')->textInput() ?>

    <?= $form->field($model, 'is_job_close')->textInput() ?>

    <?= $form->field($model, 'is_job_appear')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>






    <?php ActiveForm::end(); ?>

</div>