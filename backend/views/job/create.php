<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogJobPost */

$this->title = 'เพิ่มข้อมูลงานขนส่ง';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลงานขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-job-post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphur' => [],
        'district' => [],
        'damphur' => [],
        'ddistrict' => [],
        'cardes' => [],
    ]) ?>

</div>
