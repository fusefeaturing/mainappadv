<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogJobPost */

$this->title = 'แก้ไขข้อมูลงานขนส่ง : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลงานขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-job-post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphur' => $amphur,
        'district' => $district,
        'damphur' => $damphur,
        'ddistrict' => $ddistrict,
        'cardes' => $cardes,
        
    ]) ?>

</div>
