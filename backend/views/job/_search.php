<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchRogJobPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-job-post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'job_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'contact_person') ?>

    <?= $form->field($model, 'contact_person_pickup') ?>

    <?php // echo $form->field($model, 'contact_person_delivery') ?>

    <?php // echo $form->field($model, 'short_description') ?>

    <?php // echo $form->field($model, 'full_description') ?>

    <?php // echo $form->field($model, 'photos') ?>

    <?php // echo $form->field($model, 'cartype_valid') ?>

    <?php // echo $form->field($model, 'deliver_date') ?>

    <?php // echo $form->field($model, 'pickup_location') ?>

    <?php // echo $form->field($model, 'pickup_province') ?>

    <?php // echo $form->field($model, 'pickup_district') ?>

    <?php // echo $form->field($model, 'pickup_subdistrict') ?>

    <?php // echo $form->field($model, 'deliver_location') ?>

    <?php // echo $form->field($model, 'deliver_province') ?>

    <?php // echo $form->field($model, 'deliver_district') ?>

    <?php // echo $form->field($model, 'deliver_subdistrict') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <?php // echo $form->field($model, 'is_job_taken') ?>

    <?php // echo $form->field($model, 'is_period_job') ?>

    <?php // echo $form->field($model, 'is_job_close') ?>

    <?php // echo $form->field($model, 'is_job_appear') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ท', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
