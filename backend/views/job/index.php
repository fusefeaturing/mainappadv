<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchRogJobPost */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลงานขนส่ง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-job-post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_id',
            //'user_id',
            [
                'attribute' => 'user_id',
                'value' => 'user.username',
            ],
           
            'title',
            'contact_person',
            'contact_person_pickup',
            'contact_person_delivery',
            'short_description',
            //'full_description',
            //'photos:ntext',
           /* [
                'attribute' =>'photos',
                'format' =>'html',    
                'value' => function($data) {
                   return Html::img(\Yii::$app->request->BaseUrl.'/uploads/'.$data->photos,['width'=> 70]);
                }
            ],*/
            //'cartype_valid',
            //'deliver_date',
            //'pickup_location',
            //'pickup_province',
            [
                'attribute' => 'pickup_province',
                'value' => 'provinces.name_th',
            ],
            //'pickup_district',
            //'pickup_subdistrict',
            //'deliver_location',
            //'deliver_province',
            [
                'attribute' => 'deliver_province',
                'value' => 'dprovinces.name_th',
            ],
            
            'job_search_level',
            //'deliver_district',
            //'deliver_subdistrict',
            //'created_date',
            //'updated_date',
            //'is_job_taken',
            //'is_period_job',
            //'is_job_close',
            //'is_job_appear',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
