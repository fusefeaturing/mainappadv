<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RogJobPost */

Yii::$app->params['og_title']['content'] = $model->title;
Yii::$app->params['og_description']['content'] = $model->getOGDesc();
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ประกาศหารถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-job-post-view">

<h1><?=Html::encode($this->title)?></h1>

<p>
        <?=Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success'])?>
        <?=Html::a('แก้ไข', ['update', 'id' => $model->job_id], ['class' => 'btn btn-primary'])?>
        <?=Html::a('ลบ', ['delete', 'id' => $model->job_id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
        'method' => 'post',
    ],
])?>
    </p>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <p><?=$model->getPhotosViewerback()?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>รายละเอียดงานขนส่ง / การขึ้น ลงสินค้า</strong> : <?=$model->full_description?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ประเภทสินค้า / รายละเอียด</strong> : <?=$model->short_description?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ประเภทรถขนส่งที่ต้องการ</strong><br><?=$model->cartype_text()?></p>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <!-- Your share button code -->
            <div class="fb-share-button"
                data-size="large"
                data-href="<?='https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>"
                data-layout="button_count">
            </div>
            <div class="line-it-button"
                data-lang="en"
                data-type="share-a"
                data-ver="2"
                data-url="<?='https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>"
                style="display: none;">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ต้นทาง</strong></p>
            <ul>
                <li><strong>สถานที่ : </strong><?=$model->pickup_location?></li>
                <li><?=$model->location_text($model->pickup_province, $model->pickup_district, $model->pickup_subdistrict)?></li>
                <li><strong>ผู้ติดต่อ : </strong><?=$model->contact_person_pickup?></li>
            </ul>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ปลายทาง</strong></p>
            <ul>
                <li><strong>สถานที่</strong> : <?=$model->deliver_location?></li>
                <li><?=$model->location_text($model->deliver_province, $model->deliver_district, $model->deliver_subdistrict)?></li>
                <li><strong>ผู้ติดต่อ : </strong><?=$model->contact_person_delivery?></li>
            </ul>
        </div>
    </div>

    <?='' /* DetailView::widget([
'model' => $model,
'attributes' => [
//'job_id',
//'user_id',
'title',
'full_description',
'contact_person',
//'cartype_valid',
[
'format'=>'raw',
'attribute'=>'cartype_valid',
'value'=>$model->cartype_text(),
],
'short_description',
[
'format'=>'raw',
'attribute'=>'photos',
'value'=>$model->getPhotosViewer(),
],

'pickup_location',
'pickup_province',
'pickup_district',
'pickup_subdistrict',
'contact_person_pickup',

'deliver_location',
'deliver_province',
'deliver_district',
'deliver_subdistrict',
'contact_person_delivery',

'is_period_job',
'is_job_taken',
'is_job_close',
'is_job_appear',

'deliver_date',
'created_date',
'updated_date',
],
])*/?>

</div>
