<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogTypeWheel */

$this->title = 'แก้ไขข้อมูลชนิดรถ: ' . $model->type_wheel_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลชนิดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->type_wheel_name, 'url' => ['view', 'id' => $model->type_wheel_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-type-wheel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
