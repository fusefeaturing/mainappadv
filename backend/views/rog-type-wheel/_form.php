<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RogTypeWheel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-type-wheel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_wheel_name')->textInput(['maxlength' => true]) ?>

    <?=
        $form
            ->field($model, 'type_wheel_level')
            ->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
            //->input('', ['placeholder' => ""])
            ->label()
    ?>

    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_date')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>