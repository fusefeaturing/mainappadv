<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogTypeWheel */

$this->title = 'เพิ่มข้อมูลชนิดรถ';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลชนิดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-type-wheel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
