<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Provinces */

$this->title = 'แก้ไขข้อมูล : ' . $model->name_th;
$this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_th, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="provinces-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
