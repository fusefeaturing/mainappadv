<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Geographies;

/* @var $this yii\web\View */
/* @var $model common\models\Provinces */
/* @var $form yii\widgets\ActiveForm */

$geographies = ArrayHelper::map(Geographies::find()->asArray()->all(), 'id', 'name');
?>

<div class="provinces-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_th')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>




    <?=
                $form
                    ->field($model, 'geography_id')
                    ->label('ชื่อภูมิภาค')
                    ->dropdownList($geographies, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกภูมิภาค'
                    ])
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
