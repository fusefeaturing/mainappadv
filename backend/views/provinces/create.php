<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Provinces */

$this->title = 'เพิ่มข้อมูลจังหวัด';
$this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provinces-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
