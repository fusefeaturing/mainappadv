<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchProvinces */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ระดับจังหวัด';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provinces-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'code',
            'name_th',
            'name_en',
            [
                'attribute' => 'geography_id',
                'value' => 'geographies.name',
            ],
           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
