<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Amphures */

$this->title = 'แก้ไขข้อมูล : ' . $model->name_th;
$this->params['breadcrumbs'][] = ['label' => 'ระดับอำเภอ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_th, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="amphures-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
