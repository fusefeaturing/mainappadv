<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\SearchAmphures */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ระดับอำเภอ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amphures-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'code',
            'name_th',
            'name_en',
            [
                'attribute' => 'province_id',
                'value' => 'provinces.name_th',
            ],
           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
