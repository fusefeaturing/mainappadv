<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Provinces;

/* @var $this yii\web\View */
/* @var $model common\models\Amphures */
/* @var $form yii\widgets\ActiveForm */
$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
?>

<div class="amphures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_th')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    

    <?=
                $form
                    ->field($model, 'province_id')
                    ->label('ชื่อจังหวัด')
                    ->dropdownList($provinces, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
