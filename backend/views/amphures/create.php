<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Amphures */

$this->title = 'เพิ่มข้อมูลอำเภอ';
$this->params['breadcrumbs'][] = ['label' => 'ระดับอำเภอ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amphures-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
