<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\controllers\SearchRogUserCar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-user-car-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'car_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'car_type') ?>

    <?= $form->field($model, 'carry_weight') ?>

    <?= $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'car_services') ?>

    <?php // echo $form->field($model, 'car_service_tos') ?>

    <?php // echo $form->field($model, 'pic_front') ?>

    <?php // echo $form->field($model, 'pic_side') ?>

    <?php // echo $form->field($model, 'pic_back') ?>

    <?php // echo $form->field($model, 'pic_top') ?>

    <?php // echo $form->field($model, 'manufacturer') ?>

    <?php // echo $form->field($model, 'manufacturer_txt') ?>

    <?php // echo $form->field($model, 'plate_number') ?>

    <?php // echo $form->field($model, 'plate_province') ?>

    <?php // echo $form->field($model, 'chassis_number') ?>

    <?php // echo $form->field($model, 'engine_number') ?>

    <?php // echo $form->field($model, 'insurance_company') ?>

    <?php // echo $form->field($model, 'insured_amount') ?>

    <?php // echo $form->field($model, 'is_insured') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
