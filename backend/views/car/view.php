<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserCar */



$this->title = $model->car_id;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการรถของผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>



<div class="rog-user-car-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรถร่วมขนส่ง', ['create', 'id' => $model->car_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->car_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->car_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]); ?>
        <?= Html::a('กิจการทั้งหมด', ['carcompany', 'id' => $model->user_id, 'id2' => $model->user_id], ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_id',
            'car_search_level',
            [
                'attribute' => 'user_id',
                'value' => $model->user->username
            ],
            /*[
                'attribute' => 'car_type',
                'value' => $model->cartype->name
            ],*/
            //'type_wheel',
            //'car_des',
            [
                'attribute' => 'type_wheel',
                'value' => $model->cardes->typewheel->type_wheel_name,
            ],

            [
                'attribute' => 'car_des',
                'value' => $model->cardes->car_des_name,
            ],


            'carry_weight',
            //'color',
            [
                'attribute' => 'color',
                'value' => '<button class="btn btn-default btn-lg" style="background-color:' . $model->color . '"></button>',
                'format' => 'raw',
            ],


            'car_services:ntext',
            'car_service_tos:ntext',

            [
                'attribute' => 'pic_car',
                'value' => $model->getPhotosViewerUpdateback(),
                'format' => 'raw'
            ],


            'car_explanation',


            //'pic_front',
            //'pic_side',
            //'pic_back',
            //'pic_top',            
            /*[
                'attribute' => 'pic_front',
                'value' => '../../common/web/uploads/' . $model->pic_front  ,
                'format' => ['image',['wigth' => '100','height'=>'100']],
                
            ],            
            [
                'attribute' => 'pic_side',
                'value' => '@web/uploads/' . $model->pic_side ,
                'format' => ['image',['wigth' => '100','height'=>'100']]
            ],
            [
                'attribute' => 'pic_back',
                'value' => '@web/uploads/' . $model->pic_back ,
                'format' => ['image',['wigth' => '100','height'=>'100']]
            ],
            [
                'attribute' => 'pic_top',
                'value' => '@web/uploads/' . $model->pic_top ,
                'format' => ['image',['wigth' => '100','height'=>'100']]
            ], */

            'manufacturer',
            'manufacturer_txt',
            'plate_number',
            'plate_province',
            /*[
                'attribute' => 'current_car_province',
                'value' => @$model->currentcar->name_th
            ],
            [
                'attribute' => 'current_car_amphure',
                'value' => @$model->currentcara->name_th
            ],
            */
            'chassis_number',
            'engine_number',
            'insurance_company',
            'insured_amount',
            'is_insured',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>