<?php

use common\models\RogCarDescription;
use common\models\RogTypeWheel;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = $model->user->company_name;
$this->title = 'ชื่อบริษัท';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['og_title']['content'] = $this->title;
//Yii::$app->params['og_description']['content'] = $model->company_explanation;
//Yii::$app->params['og_image']['content'] = $model->user->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$user_id = Yii::$app->getRequest()->getQueryParam('id2');
?>


<div class="rog-user-car-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'car_id',

            [
                'attribute' => 'user_id',
                'value' => 'user.username',
            ],

            [
                'attribute' => 'user_id',
                'value' => 'user.company_name',
                'label' => 'ชื่อกิจการ'
            ],
            /*[
                'attribute' => 'car_type',
                'value' => 'cartype.name',
            ],*/
            //'type_wheel',
            //'car_des',
            /*[
                'attribute' => 'type_wheel',
                'value' => 'typewheel.type_wheel_name',
            ],
            [
                'attribute' => 'car_des',
                'value' => 'cardes.car_des_name',
            ],*/
            [
                'attribute' => 'type_wheel',
                'value' => 'cardes.typewheel.type_wheel_name',
                'filter' => ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name')
            ],
            [
                'attribute' => 'car_des',
                'value' => 'cardes.car_des_name',
                'filter' => ArrayHelper::map(RogCarDescription::find()->asArray()->all(), 'car_des_id', 'car_des_name'),
            ],

            [
                'attribute' => 'pic_car',
                'format' => ['image', ['width' => '100', 'height' => '70']],

                'value' => function ($data) {
                    return $data->firstPhotoURLBack;
                },

            ],

            //'carry_weight',
            //'car_explanation',
            //'color',
            //'car_services:ntext',

            //'car_service_tos:ntext',
            /*[
                'attribute' =>'pic_front',
                'format' =>'html',    
                'value' => function($data) {
                   return Html::img(\Yii::$app->request->BaseUrl.'/uploads/'.$data->pic_front,['width'=> 70]);
                }
            ],
            [
                'attribute' =>'pic_side',
                'format' =>'html',    
                'value' => function($data) {
                   return Html::img(\Yii::$app->request->BaseUrl.'/uploads/'.$data->pic_side,['width'=> 70]);
                }
            ],
            [
                'attribute' =>'pic_back',
                'format' =>'html',    
                'value' => function($data) {
                    return Html::img(\Yii::$app->request->BaseUrl.'/uploads/'.$data->pic_back,['width'=> 70]);
                }
            ],
            [
                'attribute' =>'pic_top',
                'format' =>'html',    
                'value' => function($data) {
                   return Html::img(\Yii::$app->request->BaseUrl.'/uploads/'.$data->pic_top,['width'=> 70]);
                }
            ],*/
            //'pic_front',
            //'pic_side',
            //'pic_back',
            //'pic_top',
            //'manufacturer',
            //'manufacturer_txt',
            //'plate_number',
            //'plate_province',
            //'car_search_level',

            /*[
                'attribute' => 'pickup_province',
                'value' => 'pprovince.name_th',
            ],*/

            //'chassis_number',
            //'engine_number',
            //'insurance_company',
            //'insured_amount',
            //'is_insured',
            //'created_date',
            //'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



</div>