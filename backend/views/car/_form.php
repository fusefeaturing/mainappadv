<?php

use common\models\Provinces;
use common\models\RogCarType;
use common\models\RogTypeWheel;
use common\models\RogUser;
use kartik\color\ColorInput;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserCar */
/* @var $form yii\widgets\ActiveForm */
$id = yii::$app->user->identity->id;
$typewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$roguser = ArrayHelper::map(RogUser::find()->asArray()->all(), 'user_id', 'username');
$rogcartype = ArrayHelper::map(RogCarType::find()->asArray()->all(), 'car_type_id', 'name');
$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
$car_des = $this->context->getCarSearch();
?>

<div class="rog-user-car-form">

    <?php $form = ActiveForm::begin();?>

    <?=
$form
->field($model, 'car_search_level')
->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
//->input('', ['placeholder' => ""])
->label()
?>

    <?=
$form
->field($model, 'user_id')
->label('ชื่อผู้ใช้งาน')
->dropdownList($roguser, [
    'id' => 'ddl-roguser',
    'prompt' => 'เลือกผู้ใช้งาน',
])
?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
$form
->field($model, 'car_des')
->widget(Select2::classname(), [
    'data' => $car_des,
    'model' => $model,
    'attribute' => 'car_des',
    'language' => 'th',
    'options' => ['placeholder' => 'เลือกประเภทรถ'],
    'pluginOptions' => [
        'allowClear' => true,
        //'multiple' => true,
    ],
])
->label('เลือกประเภทรถ', ['class' => '', 'style' => 'color:red'])
?>
    </div>


    <!--<div class="col-xs-12 col-sm-6 col-md-4">
            <?=
$form
->field($model, 'type_wheel')
->label('ชนิดรถ')
->dropdownList($typewheel, [
    'id' => 'ddl-type_wheel',
    'prompt' => 'เลือกชนิดรถ',
])
?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
$form
->field($model, 'car_des')
->widget(DepDrop::classname(), [
    'options' => ['id' => 'ddl-cardes'],
    'data' => $cardes,
    'pluginOptions' => [
        'depends' => ['ddl-type_wheel'],
        'placeholder' => 'เลือกรายละเอียด...',
        'url' => Url::to(['/car/get-cardes']),
    ],
]);
?>
    </div>-->

</div>



    <?='' //$form->field($model, 'car_type')->textInput() ?>

    <?=$form->field($model, 'carry_weight')->textInput(['maxlength' => true])?>


            <?=
$form
->field($model, 'color')
->widget(ColorInput::classname(), [
    'options' =>
    ['placeholder' => 'Select color ...'],
])
?>


    <?=$form->field($model, 'car_services')->textarea(['rows' => 6])?>

    <?=$form->field($model, 'car_explanation')->textarea(['rows' => 6])?>

    <?=$form->field($model, 'car_service_tos')->textarea(['rows' => 6])?>


    <!--<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <?=
$form
->field($model, 'pic_side')
->label('ภาพรถด้านหน้าเฉียง', ['class' => '', 'style' => 'color:red'])
->fileInput()?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="well text-center">
                        <?=Html::img($model->getPhotoViewerSideback(), ['style' => 'height:100px;', 'class' => 'img-rounded']);?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <?=$form->field($model, 'pic_front')->fileInput()?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="well text-center">
                        <?=Html::img($model->getPhotoViewerFrontback(), ['style' => 'height:100px;', 'class' => 'img-rounded']);?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <?=$form->field($model, 'pic_back')->fileInput()?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="well text-center">
                        <?=Html::img($model->getPhotoViewerBackback(), ['style' => 'height:100px;', 'class' => 'img-rounded']);?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <?=$form->field($model, 'pic_top')->fileInput()?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="well text-center">
                        <?=Html::img($model->getPhotoViewerTopback(), ['style' => 'height:100px;', 'class' => 'img-rounded']);?>
                    </div>
                </div>
            </div>
        </div>
    </div>-->



    <?=$form->field($model, 'pic_car[]')
->fileInput(['multiple' => true])
->label('รูปภาพรถหน้าเฉียง หลังเฉียงของฉัน  (เลือกได้ 2 ภาพเท่านั้น)', ['class' => '', 'style' => 'color:red'])
?>
        <div class="well">
            <?=$model->getPhotosViewerUpdate();?>
        </div>

    <br>

    <?='' //$form->field($model, 'manufacturer')->textInput() ?>

    <?=$form->field($model, 'manufacturer_txt')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'plate_number')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'plate_province')->textInput(['maxlength' => true])?>



    <?=$form->field($model, 'chassis_number')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'engine_number')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'insurance_company')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'insured_amount')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'is_insured')->textInput()?>

    <?=$form->field($model, 'created_date')->textInput([
    'readonly' => true,
    'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s'),
])?>

    <?=$form->field($model, 'updated_date')->textInput([
    'readonly' => true,
    'value' => date('Y-m-d H:i:s'),
])?>


    <div class="form-group">
        <?=Html::submitButton('บันทึก', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
