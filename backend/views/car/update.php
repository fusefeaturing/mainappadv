<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserCar */

$this->title = 'แก้ไขข้อมูลรถของผู้ใช้งาน : ' . $model->car_id;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการรถของผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->car_id, 'url' => ['view', 'id' => $model->car_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-car-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardes' => $cardes,
        'cartypes' => $cartypes,
    ]) ?>

</div>
