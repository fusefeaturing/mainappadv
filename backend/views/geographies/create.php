<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Geographies */

$this->title = 'เพิ่มข้อมูล';
$this->params['breadcrumbs'][] = ['label' => 'ระดับภูมิภาค', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geographies-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
