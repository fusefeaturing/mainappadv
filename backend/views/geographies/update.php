<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Geographies */

$this->title = 'แก้ไขข้อมูล : ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ระดับภูมิภาค', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="geographies-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
