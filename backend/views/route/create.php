<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserRoute */

$this->title = 'เพิ่มข้อมูลเส้นทางขนส่ง';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลเส้นทางขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-route-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphur' => [],
    ]) ?>

</div>
