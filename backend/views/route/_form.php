<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Amphures;
use common\models\Provinces;
use common\models\RogUser;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;

$provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');

$roguser = ArrayHelper::map(RogUser::find()->asArray()->all(), 'user_id', 'username');

/* @var $this yii\web\View */
/* @var $model common\models\RogUserRoute */
/* @var $form yii\widgets\ActiveForm */
$id = yii::$app->user->identity->id;
?>

<div class="rog-user-route-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>



    <?= '' //$form->field($model, 'user_id')->textInput(['maxlength' => true]) 
    ?>


    <?=
        $form
            ->field($model, 'user_id')
            ->label('ชื่อผู้ใช้งาน')
            ->dropdownList($roguser, [
                'id' => 'ddl-roguser',
                'prompt' => 'เลือกผู้ใช้งาน'
            ])
    ?>


    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'province')
                    ->label('* จังหวัดต้นทาง', ['class' => '', 'style' => 'color:red'])
                    ->dropdownList($provinces, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'amphur')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-amphur'],
                        'data' => $amphur,
                        'pluginOptions' => [

                            'depends' => ['ddl-province'],
                            'placeholder' => 'เลือกอำเภอ...',
                            'url' => Url::to(['/route/get-amphur'])
                        ]
                    ]);
            ?>
        </div>
    </div>



    <?= '' //$form->field($model, 'created_date')->textInput() 
    ?>

    <?= '' //$form->field($model, 'updated_date')->textInput() 
    ?>

    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_date')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>