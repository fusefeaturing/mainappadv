<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserRoute */

$this->title = 'แก้ไขข้อมูลเส้นทางขนส่ง : ' . $model->route_id;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลเส้นทางขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->route_id, 'url' => ['view', 'id' => $model->route_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
        'amphur' => $amphur,
      
        
    ]) ?>

</div>
