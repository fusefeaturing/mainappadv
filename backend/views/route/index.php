<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchRoute */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลเส้นทางขนส่ง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-route-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'route_id',
            
            [
                'attribute' => 'user_id',
                'value' => 'user.username',
            ],
           
            [
                'attribute' => 'province',
                'value' => 'provinces.name_th',
            ],
           
            [
                'attribute' => 'amphur',
                'value' => 'amphures.name_th',
            ],
            //'created_date',
            //'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
