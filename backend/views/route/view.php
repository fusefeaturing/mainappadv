<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RogUserRoute */

$this->title = $model->route_id;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลเส้นทางขนส่ง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-user-route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->route_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->route_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'route_id',

            [
                'attribute' => 'user_id',
                'value' => $model->user->username,
            ],

            [
                'attribute' => 'province',
                'value' => $model->provinces->name_th,
            ],

            [
                'attribute' => 'amphur',
                'value' => $model->amphures->name_th,
            ],

            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>