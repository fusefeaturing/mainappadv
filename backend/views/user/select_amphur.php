<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$id = yii::$app->user->identity->id;
$this->title = "เลือกอำเภอที่ให้บริการ";
?>

<div class="rog-car-post-form">

    <div class="card2">
        <div class="card-body">
            <?= Html::beginForm(['/user/add-route', 'id' => $id], 'POST'); ?>

            <?= Html::hiddenInput('step', 'amphurselector'); ?>
            <?= Html::hiddenInput('user_id', $id); ?>

            <?php
            foreach ($provinces as $provin) {
                echo '<h2>' . $provin . '</h2>';
                echo '<hr>';
                $pov = $this->context->getAmphuresByProvinceName($provin);
                //echo var_dump($pov);
                echo Html::checkboxList('selectedamphur', $selectedamphur, $pov, ['class' => 'test']) . '<br><hr>';
            }
            ?>

            <div class="form-group">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']); ?>
            </div>
            <?= Html::endForm(); ?>

        </div>
    </div>

</div>