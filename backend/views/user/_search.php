<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'company_address') ?>

    <?php // echo $form->field($model, 'company_pic') ?>

    <?php // echo $form->field($model, 'pic_idcard') ?>

    <?php // echo $form->field($model, 'pic_carlic') ?>

    <?php // echo $form->field($model, 'pic_self') ?>

    <?php // echo $form->field($model, 'facebook_id') ?>

    <?php // echo $form->field($model, 'facebook_token') ?>

    <?php // echo $form->field($model, 'citizen_id') ?>

    <?php // echo $form->field($model, 'carlic_id') ?>

    <?php // echo $form->field($model, 'prefix') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'surname') ?>

    <?php // echo $form->field($model, 'middlename') ?>

    <?php // echo $form->field($model, 'dob') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'email_contact') ?>

    <?php // echo $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'line_id') ?>

    <?php // echo $form->field($model, 'tel_1') ?>

    <?php // echo $form->field($model, 'tel_2') ?>

    <?php // echo $form->field($model, 'is_user_findcar') ?>

    <?php // echo $form->field($model, 'is_user_car_owner') ?>

    <?php // echo $form->field($model, 'is_user_verified') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ท', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
