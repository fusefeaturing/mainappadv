<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RogUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rog-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'company_explanation')->textarea(['rows' => 6]) ?>

    


    <?= $form->field($model, 'company_pic[]')->fileInput(['multiple' => true]) ?>
    <div class="well">
        <?= $model->getPhotosViewerFrontback() ?>
    </div>

    <?= $form->field($model, 'pic_idcard')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pic_carlic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pic_self')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'citizen_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carlic_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prefix')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middlename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_contact')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_user_findcar')->textInput() ?>

    <?= $form->field($model, 'is_user_car_owner')->textInput() ?>

    <?= $form->field($model, 'is_user_verified')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
