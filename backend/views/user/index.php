<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลผู้ใช้งาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'user_id',
            'username',
            //'password',
            'company_name',
            'company_address:ntext',
            //'company_explanation',
            //'company_pic',
            //'pic_idcard',
            //'pic_carlic',
            //'pic_self',
            //'facebook_id',
            //'facebook_token',
            //'citizen_id',
            //'carlic_id',
            //'prefix',
            'name',
            //'surname',
            //'middlename',
            //'dob',
            'email:email',
            //'email_contact:email',
            //'website',
            'line_id',
            'tel_1',
            //'tel_2',
            //'is_user_findcar',
            //'is_user_car_owner',
            //'is_user_verified',
            //'created_date',
            //'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
