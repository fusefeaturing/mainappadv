<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RogUser */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('+ ระบุพื้นที่ฉันให้บริการ', ['add-route', 'id' => $model->user_id], ['class' => 'btn btn-info ']); ?>
        <?= Html::a('+ เพิ่มรถร่วมขนส่ง', ['car/create', 'id' => $model->user_id], ['class' => 'btn btn-success ']); ?>
        <?= Html::a('+ เพิ่มงานขนส่ง', ['job/create', 'id' => $model->user_id], ['class' => 'btn btn-success ']); ?>
    </p>




    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'user_id',
            'username',
            'password',
            'company_name',
            'company_address:ntext',
            //'company_pic:html',

            [
                'attribute' => 'company_pic',
                'value' => $model->getPhotosViewerFrontback(),
                'format' => 'html',
            ],
            'company_explanation',
            'pic_idcard',
            'pic_carlic',
            'pic_self',
            'facebook_id',
            'facebook_token',
            'citizen_id',
            'carlic_id',
            'prefix',
            'name',
            'surname',
            'middlename',
            'dob',
            'email:email',
            'email_contact:email',
            'website',
            'line_id',
            'tel_1',
            'tel_2',
            'is_user_findcar',
            'is_user_car_owner',
            'is_user_verified',
            'created_date',
            'updated_date',
        ],
    ]) ?>

    <div class="card2">
        <div class="card-body">
            <div class="">

                <h2>พื้นที่ให้บริการ</h2>

                <hr>

                <?php

                $provtran = [];
                foreach ($serviceroutes as $key => $servicearea) {
                    $provtran[$provinces[$servicearea]][$key] = $amphures[$key];
                }
                if (!empty($serviceroutes)) {
                } else {
                    echo ' <div class="alert alert-danger" role="alert">';
                    echo "ไม่มีข้อมูลพื้นที่ให้บริการ !!";
                    echo '</div>';
                }
                foreach ($provtran as $key => $protran) {
                    echo '<h4>' . $key . '</h4>';
                    echo '<p>';
                    foreach ($provtran[$key] as $distran) {
                        echo $distran . ' ';
                    }
                    echo '</p>';
                    echo '<hr>';
                }
                ?>

            </div>
        </div>
    </div>

</div>