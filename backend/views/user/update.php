<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogUser */

$this->title = 'แก้ไขข้อมูลผู้ใช้งาน : ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
