<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarType */

$this->title = 'แก้ไขข้อมูลประเภทรถ : ' . $model->typeWheel->type_wheel_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->typeWheel->type_wheel_name, 'url' => ['view', 'id' => $model->car_type_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-car-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardes' => $cardes,

    ]) ?>

</div>