<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarType */


$this->title = $model->typeWheel->type_wheel_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-car-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->car_type_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->car_type_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '
                คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <h1><?= Html::img($model->getPhotoViewerback(),['class'=>'img-thumbnail','style'=>'height:200px;'])?></h1>
           
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p>ประเภทรถ <strong><?= $model->typeWheel->type_wheel_name?></strong></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p>ลักษณะบรรทุก <strong><?= $model->carDes->car_des_name ?></strong></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <hr>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p>ค่าขนส่งเฉลี่ยต่อกิโลเมตร <?= $model->fee?> บาท/กิโลเมตร</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <hr>
        </div>
        
    </div>

</div>
