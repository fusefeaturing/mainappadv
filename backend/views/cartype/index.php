<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\SearchCartype */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลประเภทรถ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    // echo Yii::getAlias('@common/web/uploads') . '/' . $model->picture;
    ?>


<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model)
                {
                   
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<h4>' . $model->typeWheel->type_wheel_name . ' </h4>';
                        $htmlcard .= '<p>ลำดับจัดเรียง : ' . $model->car_type_level . '</p>';
                        $htmlcard .= '<p>ประเภทรถ : ' .$model->typeWheel->type_wheel_name . '</p>';
                        $htmlcard .= '<p>ลักษณะบรรทุก : ' . $model->carDes->car_des_name . '</p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        //$htmlcard .= Yii::getAlias($model->photoViewerback) ;
                        $htmlcard .= Html::img($model->getPhotoViewerback(), ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= '<div class="row">';
                        
                        /*$htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('ดูรายละเอียดเพิ่มเติม', 
                        ['cartype/view', 'id' => $model->car_type_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';*/

                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('แก้ไข', 
                        ['update', 'id' => $model->car_type_id], 
                        ['class' => 'btn btn-primary glyphicon glyphicon-pencil', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= Html::a('ลบ', ['delete', 'id' => $model->car_type_id], 
                        [
                            'class' => 'btn btn-danger glyphicon glyphicon-remove', 'style' => 'width: 100%;',
                            'data' => 
                            [
                                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                'method' => 'post',
                            ],
                        ]);
                        $htmlcard .= '</div>';

                        $htmlcard .= '</div>';
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]); ?>
</div>