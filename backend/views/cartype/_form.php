<?php

use common\models\RogCarDescription;
use common\models\RogTypeWheel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\RogCarType */
/* @var $form yii\widgets\ActiveForm */
$rogtypewheel = ArrayHelper::map(RogTypeWheel::find()->orderBy(['type_wheel_level' => SORT_ASC])->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
$rogcardes = ArrayHelper::map(RogCarDescription::find()->asArray()->all(), 'car_des_id', 'car_des_name');
?>

<div class="rog-car-type-form">

    <?php $form = ActiveForm::begin(); ?>

    

    <?='' //$form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= 
        $form
            ->field($model, 'car_type_level')
            ->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
            //->input('', ['placeholder' => ""])
            ->label() 
    ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'name')
                ->label('ชนิดรถ')
                ->dropdownList($rogtypewheel, [
                    'id' => 'name',
                    'prompt' => 'เลือกชนิดรถ'
                ])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'description')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'description'],
                    'data' => $cardes,
                    'pluginOptions' => [
                        'depends' => ['name'],
                        
                        'placeholder' => 'เลือกลักษณะรถ',
                        'url' => Url::to(['//cartype/get-cardes'])
                    ]
                ]);
        ?>
    </div>   
</div>


    <?= $form->field($model, 'fee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wheel')->textInput() ?>


    <div class="row">
        <div class="col-md-3">
            <div class="well text-center">
                <?= Html::img('/images'. '/' .$model->getPhotoViewerback(), ['style' => 'height:100px;', 'class' => 'img-rounded']); ?>
            </div>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'picture')->fileInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_date')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>