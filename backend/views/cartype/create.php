<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarType */

$this->title = 'เพิ่มข้อมูล';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cardes' => [],
    ]) ?>

</div>