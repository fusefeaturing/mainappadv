<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarDescription */

$this->title = 'เพิ่มข้อมูลรายระเอียดรถ';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลรายละเอียดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-description-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
