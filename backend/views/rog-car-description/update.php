<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarDescription */

$this->title = 'แก้ไขรายละเอียดรถ: ' . $model->car_des_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลรายละเอียดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->car_des_name, 'url' => ['view', 'id' => $model->car_des_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="rog-car-description-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
