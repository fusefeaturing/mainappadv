<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchRogCarDescription */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลรายละเอียดรถ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rog-car-description-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'car_des_id',
            [
                'attribute' => 'type_wheel_id',
                'value' => 'typewheel.type_wheel_name',
            ],
            'car_des_name',
            'car_des_level',
            //'type_wheel_id',
           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
