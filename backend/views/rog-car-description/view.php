<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RogCarDescription */

$this->title = $model->car_des_name;

$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลรายละเอียดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rog-car-description-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>   
        <?= Html::a('แก้ไข', ['update', 'id' => $model->car_des_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->car_des_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_des_id',
            'car_des_name',
            'car_des_level',
            //'type_wheel_id',
            [
                'attribute' => 'type_wheel_id',
                'value' => $model->typewheel->type_wheel_name
            ],
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
