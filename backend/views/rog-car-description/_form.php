<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\RogTypeWheel;
/* @var $this yii\web\View */
/* @var $model common\models\RogCarDescription */
/* @var $form yii\widgets\ActiveForm */
$typewheel = ArrayHelper::map(RogTypeWheel::find()->asArray()->all(), 'type_wheel_id', 'type_wheel_name');
?>

<div class="rog-car-description-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= 
        $form
            ->field($model, 'car_des_level')
            ->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
            //->input('', ['placeholder' => ""])
            ->label() 
    ?>

    <?=
        $form
            ->field($model, 'type_wheel_id')
            ->label('ชื่อชนิดรถ')
            ->dropdownList($typewheel, [
                'id' => 'ddl-province',
                'prompt' => 'เลือกชนิดรถ'
            ])
    ?>

    <?= $form->field($model, 'car_des_name')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'type_wheel_id')->textInput() ?>




    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_date')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
