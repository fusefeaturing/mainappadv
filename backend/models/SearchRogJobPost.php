<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogJobPost;

/**
 * SearchRogJobPost represents the model behind the search form of `common\models\RogJobPost`.
 */
class SearchRogJobPost extends RogJobPost
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'is_job_taken', 'is_period_job', 'is_job_close', 'is_job_appear'], 'integer'],
            [['title', 'user_id', 'contact_person', 'contact_person_pickup', 'contact_person_delivery', 'short_description', 'full_description', 'photos', 'cartype_valid'
            , 'deliver_date', 'pickup_location', 'pickup_province', 'pickup_district', 'pickup_subdistrict'
            , 'deliver_location', 'deliver_province', 'deliver_district', 'deliver_subdistrict', 'created_date'
            , 'updated_date', 'job_search_level'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogJobPost::find();
        $query->joinWith(['user']);
        $query->joinWith(['provinces']);
        $query->joinWith(['provinces']);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['job_search_level' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
         
            'deliver_date' => $this->deliver_date,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
            'is_job_taken' => $this->is_job_taken,
            'is_period_job' => $this->is_period_job,
            'is_job_close' => $this->is_job_close,
            'is_job_appear' => $this->is_job_appear,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'contact_person_pickup', $this->contact_person_pickup])
            ->andFilterWhere(['like', 'contact_person_delivery', $this->contact_person_delivery])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'full_description', $this->full_description])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'cartype_valid', $this->cartype_valid])
            ->andFilterWhere(['like', 'pickup_location', $this->pickup_location])
            ->andFilterWhere(['like', 'provinces.name_th', $this->pickup_province])
            ->andFilterWhere(['like', 'pickup_district', $this->pickup_district])
            ->andFilterWhere(['like', 'pickup_subdistrict', $this->pickup_subdistrict])
            ->andFilterWhere(['like', 'deliver_location', $this->deliver_location])
            ->andFilterWhere(['like', 'provinces.name_th', $this->deliver_province])
            ->andFilterWhere(['like', 'deliver_district', $this->deliver_district])
            ->andFilterWhere(['like', 'deliver_subdistrict', $this->deliver_subdistrict])
            ->andFilterWhere(['like', 'rog_user.username', $this->user_id]);


        return $dataProvider;
    }
}
