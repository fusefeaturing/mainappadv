<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogUserCar;

/**
 * SearchRogUserCar represents the model behind the search form of `common\models\RogUserCar`.
 */
class SearchRogUserCar extends RogUserCar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'user_id', 'car_type', 'manufacturer', 'is_insured', 'car_search_level'], 'integer'],
            [['type_wheel', 'car_des', 'color', 'car_services', 'car_service_tos', 'pic_car', 'pic_front', 'pic_side', 'pic_back', 'pic_top', 'manufacturer_txt', 'plate_number', 'plate_province', 'current_car_province', 'current_car_amphure', 'pickup_province', 'deliver_province', 'chassis_number', 'engine_number', 'insurance_company', 'created_date', 'updated_date', 'car_explanation'], 'safe'],
            [['carry_weight', 'insured_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogUserCar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['car_search_level' => SORT_ASC]]
        ]);

        $this->load($params);
        $query->joinWith(['user']);
        $query->joinWith(['cartype']);
        $query->joinWith(['typewheel']);
        $query->joinWith(['cardes']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_id' => $this->car_id,
            'car_search_level' => $this->car_search_level,
            'carry_weight' => $this->carry_weight,
            'pic_car' => $this->pic_car,
            'manufacturer' => $this->manufacturer,
            'insured_amount' => $this->insured_amount,
            'is_insured' => $this->is_insured,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'car_services', $this->car_services])
            ->andFilterWhere(['like', 'car_service_tos', $this->car_service_tos])
            ->andFilterWhere(['like', 'pic_front', $this->pic_front])
            ->andFilterWhere(['like', 'pic_side', $this->pic_side])
            ->andFilterWhere(['like', 'pic_back', $this->pic_back])
            ->andFilterWhere(['like', 'pic_top', $this->pic_top])
            ->andFilterWhere(['like', 'pic_car', $this->pic_car])
            ->andFilterWhere(['like', 'manufacturer_txt', $this->manufacturer_txt])
            ->andFilterWhere(['like', 'plate_number', $this->plate_number])
            ->andFilterWhere(['like', 'plate_province', $this->plate_province])
            ->andFilterWhere(['like', 'car_explanation', $this->car_explanation])
            ->andFilterWhere(['like', 'pickup_province', $this->pickup_province])
            ->andFilterWhere(['like', 'deliver_province', $this->deliver_province])
            ->andFilterWhere(['like', 'chassis_number', $this->chassis_number])
            ->andFilterWhere(['like', 'engine_number', $this->engine_number])
            ->andFilterWhere(['like', 'insurance_company', $this->insurance_company])
            ->andFilterWhere(['like', 'insurance_company', $this->insurance_company])
            ->andFilterWhere(['like', 'rog_user.username', $this->user_id])
            ->andFilterWhere(['like', 'rog_car_type.name', $this->car_type])
            ->andFilterWhere(['like', 'type_wheel', $this->type_wheel])
            ->andFilterWhere(['like', 'car_des', $this->car_des]);

        return $dataProvider;
    }
}
