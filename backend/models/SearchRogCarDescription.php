<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogCarDescription;

/**
 * SearchRogCarDescription represents the model behind the search form of `common\models\RogCarDescription`.
 */
class SearchRogCarDescription extends RogCarDescription
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_des_id'], 'integer'],
            [['car_des_name', 'created_date', 'updated_date', 'type_wheel_id', 'car_des_level'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogCarDescription::find();
        $query->joinWith(['typewheel']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['car_des_level' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_des_id' => $this->car_des_id,
            
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'car_des_name', $this->car_des_name])
            ->andFilterWhere(['like', 'rog_type_wheel.type_wheel_name', $this->type_wheel_id]);

        return $dataProvider;
    }
}
