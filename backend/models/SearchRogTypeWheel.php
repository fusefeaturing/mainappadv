<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogTypeWheel;

/**
 * SearchRogTypeWheel represents the model behind the search form of `common\models\RogTypeWheel`.
 */
class SearchRogTypeWheel extends RogTypeWheel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_wheel_id', 'type_wheel_level'], 'integer'],
            [['type_wheel_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogTypeWheel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['type_wheel_level' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'type_wheel_id' => $this->type_wheel_id,
            'type_wheel_level' => $this->type_wheel_level,
        ]);

        $query->andFilterWhere(['like', 'type_wheel_name', $this->type_wheel_name]);

        return $dataProvider;
    }
}
