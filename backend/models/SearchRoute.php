<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogUserRoute;

/**
 * SearchRoute represents the model behind the search form of `common\models\RogUserRoute`.
 */
class SearchRoute extends RogUserRoute
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['route_id'], 'integer'],
            [['user_id', 'province' ,'amphur' ,'created_date', 'updated_date' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogUserRoute::find();
        $query->joinWith(['user']);
        $query->joinWith(['provinces']);
        $query->joinWith(['amphures']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'route_id' => $this->route_id,           
            
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'rog_user.username', $this->user_id])
            ->andFilterWhere(['like', 'provinces.name_th', $this->province])
            ->andFilterWhere(['like', 'amphures.name_th', $this->amphur]);

        return $dataProvider;
    }
}
