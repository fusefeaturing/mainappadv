<?php

namespace backend\controllers;

use Yii;
use common\models\RogCarType;
use backend\models\SearchCartype;
use common\models\RogCarDescription;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * CartypeController implements the CRUD actions for RogCarType model.
 */
class CartypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RogCarType models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $searchModel = new SearchCartype();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RogCarType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RogCarType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = new RogCarType();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->picture = $model->upload($model, 'picture');
            $model->save();
            return $this->redirect(['view', 'id' => $model->car_type_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogCarType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = $this->findModel($id);

        $cardes = ArrayHelper::map($this->getCardes($model->name), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->picture = $model->upload($model, 'picture');
            $model->save();
            return $this->redirect(['view', 'id' => $model->car_type_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'cardes' => $cardes,
        ]);
    }

    /**
     * Deletes an existing RogCarType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogCarType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RogCarType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (($model = RogCarType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetCardes()
    {
        $out = [];
        if (Yii::$app->request->isAjax && Yii::$app->request->post('depdrop_parents')) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cardes = $parents[0];
                $out = $this->getCardes($cardes);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }



    protected function getCardes($id)
    {
        $datas = RogCarDescription::find()->where(['type_wheel_id' => $id])->all();
        return $this->MapData($datas, 'car_des_id', 'car_des_name');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }


}
