<?php

namespace backend\controllers;

use Yii;
use common\models\RogTypeWheel;
use backend\models\SearchRogTypeWheel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RogTypeWheelController implements the CRUD actions for RogTypeWheel model.
 */
class RogTypeWheelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RogTypeWheel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchRogTypeWheel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RogTypeWheel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function getDefaultLevel() {
        $model = new RogTypeWheel();

        if($model->type_wheel_level) {
            echo $model->type_wheel_level;
         } else {
            echo '0';
         }  

         
    }

    /**
     * Creates a new RogTypeWheel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RogTypeWheel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->type_wheel_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogTypeWheel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->type_wheel_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RogTypeWheel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogTypeWheel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RogTypeWheel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RogTypeWheel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getwheelTypes()
    {
        if (($model = RogTypeWheel::find()) !== null) {
            return ArrayHelper::map(
                RogTypeWheel::find()
                ->orderBy([
                    'type_wheel_level' => SORT_ASC
                ])
                ->asArray()
                ->all(),
                'type_wheel_id',

                function ($model) {
                    if ($model['type_wheel_name'] ) return $model['type_wheel_name'];
                    else return $model['type_wheel_name'];
                }
            );
        }
        return array(-1, 'No car data');
    }
}
