<?php

namespace backend\controllers;

use Yii;
use common\models\RogUserCar;
use common\models\RogCarType;
use common\models\RogUser;


use yii\helpers\ArrayHelper;
use common\models\RogTypeWheel;
use backend\models\SearchRogUserCar;
use common\models\RogCarDescription;
use common\models\RogUserCarSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * CarController implements the CRUD actions for RogUserCar model.
 */
class CarController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all RogUserCar models.
     * @return mixed
     */
    public function actionIndex()

    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $searchModel = new SearchRogUserCar();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCarcompany($id)
    {
        $model = RogUserCar::findOne($id);

        $user_id = Yii::$app->user->id;


        /*var_dump($id);
        die();*/

        $request = Yii::$app->request;
        $users_id = $request->post('user_id');

        /*var_dump($id);
        die();*/

        $searchModel = new RogUserCarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->andFilterWhere(['in', 'user_id', $id]);


        return $this->render('car_company', [
            'model' => $model,


            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    /**
     * Displays a single RogUserCar model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RogUserCar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = new RogUserCar();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->pic_car = $model->uploadMultiple($model, 'pic_car');
            }

            $model->save();

            //return $this->redirect(['view', 'id' => $model->car_id]);
            return $this->redirect(['view', 'id' => $model->car_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'cartypes' => $this->getCarTypes(),
        ]);
    }

    /**
     * Updates an existing RogUserCar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = $this->findModel($id);


        $cardes = ArrayHelper::map($this->getCardes($model->type_wheel), 'id', 'name');
        $user = ArrayHelper::map($this->getUser($model->user_id), 'user_id', 'username');
        $cartype = ArrayHelper::map($this->getUser($model->car_type), 'car_type_id', 'name');

        $oldimage = $model->pic_car;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {

                $model->pic_car = $model->uploadMultiple($model, 'pic_car');


                $photo = $model->pic_car;

                //echo $photo;
                //die();

                if ($photo != $oldimage) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->car_id]);
                    }
                } else if ($photo == $oldimage) {

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->car_id]);
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
            'cartype' => $cartype,
            'cartypes' => $this->getCarTypes(),
            'cardes' => $cardes,


        ]);
    }

    /**
     * Deletes an existing RogUserCar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = RogUserCar::findOne($id);

        $images = explode(",", $model->pic_car);
        if ($model->pic_car != null) {

            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }

            $this->findModel($id)->delete();
        } else {

            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogUserCar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RogUserCar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = RogUserCar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetUser()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $user_id = $parents[0];
                $out = $this->getUser($user_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getUser($id)
    {
        $datas = RogUser::find()->where(['user_id' => $id])->all();
        return $this->MapData($datas, 'user_id', 'username');
    }

    public function actionGetCartypess()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $user_id = $parents[0];
                $out = $this->getUser($user_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getCartypess($id)
    {
        $datas = RogUser::find()->where(['car_type' => $id])->all();
        return $this->MapData($datas, 'car_type_id', 'name');
    }


    public function getCarTypes()
    {
        if (($model = RogCarType::find()) !== null) {
            return ArrayHelper::map(
                RogCarType::find()->orderBy(['index_sort' => SORT_ASC])->asArray()->all(),
                'car_type_id',

                function ($model) {
                    if ($model['name'] == $model['description']) return $model['name'];
                    else return $model['name'] . ' - ' . $model['description'];
                }
            );
        }

        return array(-1, 'No car data');
    }


    public function actionGetCardes()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $typewheel = $parents[0];
                $out = $this->getCardes($typewheel);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getCardes($id)
    {
        $datas = RogCarDescription::find()->where(['type_wheel_id' => $id])->all();
        return $this->MapData($datas, 'car_des_id', 'car_des_name');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    /*public function getCarTypes()
    {
        if (($model = RogCarType::find()) !== null) {
            return ArrayHelper::map(
                RogCarType::find()->orderBy(['index_sort' => SORT_ASC])->asArray()->all(),
                'car_type_id',
                function ($model) {
                    if ($model['name'] == $model['description']) return $model['name'];
                    else return $model['name'] . ' - ' . $model['description'];
                }
            );
        }

        return array(-1, 'No car data');
    }*/

    public function getwheelTypes()
    {
        if (($model = RogTypeWheel::find()) !== null) {
            return ArrayHelper::map(
                RogTypeWheel::find()
                    ->orderBy([
                        'type_wheel_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'type_wheel_id',

                function ($model) {
                    if ($model['type_wheel_name']) return $model['type_wheel_name'];
                    else return $model['type_wheel_name'];
                }
            );
        }
        return array(-1, 'No car data');
    }

    public static function getCarSearch()
    {
        $options = [];

        $parents = RogTypeWheel::find()->orderBy('type_wheel_id')->all();
        //$parents = MhJobSubPt::find()->where('job_pt_id')->orderBy('job_pt_id')->all();
        foreach ($parents as $id => $p) {
            $children = RogCarDescription::find()->where('type_wheel_id=:type_wheel_id', [':type_wheel_id' => $p->type_wheel_id])->all();
            $child_options = [];
            foreach ($children as $child) {
                $child_options[$child->type_wheel_id] = $child->car_des_name;
            }
            //$options[$p->jobPt->job_pt_name] = $child_options;
            $options[$p->type_wheel_name] = ArrayHelper::map($children, 'car_des_id', 'car_des_name');
        }
        return $options;
    }
}
