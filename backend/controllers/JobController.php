<?php

namespace backend\controllers;

use Yii;
use common\models\RogJobPost;
use backend\models\SearchRogJobPost;
use common\models\RogUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Amphures;
use common\models\Districts;
use common\models\RogCarDescription;
use common\models\RogCarType;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
/**
 * JobController implements the CRUD actions for RogJobPost model.
 */
class JobController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all RogJobPost models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $searchModel = new SearchRogJobPost();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RogJobPost model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RogJobPost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = new RogJobPost();


        if ($model->load(Yii::$app->request->post())) {
            if ($model->cartype_valid) {
                $model->cartype_valid = implode(",", $model->cartype_valid);
            }

            if ($model->validate()) {
                $model->photos = $model->uploadMultiple($model, 'photos');
            }

            $model->save();
            //return $this->redirect(['view', 'id' => $model->job_id]);
            return $this->redirect(['view', 'id' => $model->job_id]);
        }
       
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogJobPost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = $this->findModel($id);

        $user = ArrayHelper::map($this->getUser($model->user_id), 'user_id', 'username');
        $amphur = ArrayHelper::map($this->getAmphur($model->pickup_province), 'id', 'name');
        $district = ArrayHelper::map($this->getDistrict($model->pickup_district), 'id', 'name');
        $cardes = ArrayHelper::map($this->getCardes($model->type_wheel), 'id', 'name');
        $damphur = ArrayHelper::map($this->getAmphur($model->deliver_province), 'id', 'name');
        $ddistrict = ArrayHelper::map($this->getDistrict($model->deliver_district), 'id', 'name');

        $oldimage = $model->photos;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {
            $model->photos = $model->uploadMultiple($model, 'photos');

            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->cartype_valid) {
                $model->cartype_valid = implode(",", $model->cartype_valid);
            }

            if ($model->validate()) {
                
                $model->photos = $model->uploadMultiple($model, 'photos');
                
                $photo = $model->photos;
               
                //echo $photo;
                //die();

                if ( $photo != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
               
                } else if ($photo == $oldimage) {

                    if ($model->save()) {
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                }
            }

        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
            'amphur' => $amphur,
            'district' => $district,
            'damphur' => $damphur,
            'ddistrict' => $ddistrict,
            'cardes' => $cardes,

        ]);
    }

    /**
     * Deletes an existing RogJobPost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        
        $model = RogJobPost::findOne($id);
        $images = explode(",", $model->photos);


        if ($model->photos != null) {
            
            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }
            
            $this->findModel($id)->delete();
        } else {
           
            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogJobPost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RogJobPost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (($model = RogJobPost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetUser()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $user_id = $parents[0];
                $out = $this->getUser($user_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }   

    protected function getUser($id)
    {
        $datas = RogUser::find()->where(['user_id' => $id])->all();
        return $this->MapData($datas, 'user_id', 'username');
    }


    public function actionGetAmphur()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphur($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }  

    public function actionGetDistrict()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphur_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getDistrict($amphur_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetCardes()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $typewheel = $parents[0];
                $out = $this->getCardes($typewheel);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getCardes($id)
    {
        $datas = RogCarDescription::find()->where(['type_wheel_id' => $id])->all();
        return $this->MapData($datas, 'car_des_id', 'car_des_name');
    }

    protected function getAmphur($id)
    {
        $datas = Amphures::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'id', 'name_th');
    }

    protected function getDistrict($id)
    {
        $datas = Districts::find()->where(['amphure_id' => $id])->all();
        return $this->MapData($datas, 'id', 'name_th');
    }






    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getCarTypes()
    {
        if (($model = RogCarDescription::find()) !== null) {
            return ArrayHelper::map(
                RogCarDescription::find()
                ->orderBy([
                    'car_des_level' => SORT_ASC
                ])
                
                ->all(),
                'car_des_id',
                function ($model) {
                    if ( $model['type_wheel_id'] == $model['car_des_name']) return $model['type_wheel_id'];
                    else return $model->typewheel->type_wheel_name . ' - ' . $model->car_des_name;
                }
            );
        }

        return array(-1, 'No car data');
    }
}