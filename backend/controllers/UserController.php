<?php

namespace backend\controllers;

use Yii;
use common\models\RogUser;
use backend\models\SearchUser;
use common\models\Amphures;
use common\models\Provinces;
use common\models\RogUserRoute;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * UserController implements the CRUD actions for RogUser model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RogUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $searchModel = new SearchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $provinces = ArrayHelper::map(Provinces::find()->asArray()->all(), 'id', 'name_th');
        $model = RogUserRoute::findOne($id);
        //$model = $this->findModel($id);
        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedprovinces = $request->post('selectedprovinces');
            $selectedamphur = $request->post('selectedamphur');
            $user_id = $request->post('user_id');

            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphur';

                $provinces = ArrayHelper::map(
                    Provinces::find()
                        ->where(['in', 'id', $selectedprovinces])
                        ->asArray()
                        ->all(),
                    'id',
                    'name_th'
                );

                $amphures = ArrayHelper::map(
                    Amphures::find()
                        ->where(['in', 'province_id', $selectedprovinces])
                        ->asArray()
                        ->all(),
                    'id',
                    'name_th'
                );

                $amphIDs = ArrayHelper::map(
                    Amphures::find()
                        ->asArray()
                        ->all(),
                    'id',
                    'id'
                );

                $selectedamphur = $amphIDs;
            }

            if ($request->post('step') == 'amphurselector') {
                //save

                $userid = Yii::$app->user->id;
                $model = RogUserRoute::findOne($id);
                //$model = $this->findModel($id);
                $this->saveUserRoute($user_id, $selectedamphur);
                //redirect
                //return $this->redirect(['car/route']);
                /*print_r($id);
                die();*/
                return $this->redirect(['user/view', 'id' => $userid]);
            }

            return $this->render($redirect, [
                'provinces' => $provinces,
                'amphures' => $amphures,
                'selectedprovinces' => $selectedprovinces,
                'selectedamphur' => $selectedamphur,
                'model' => $model,
            ]);
        } else {
            $selectedprovinces = [];

            return $this->render('select_province', [
                'provinces' => $provinces,
                'selectedprovinces' => $selectedprovinces,
            ]);
        }
    }

    protected function saveUserRoute($user_id, $amphurids)
    {
        //delete old
        $old_user_route = RogUserRoute::find()
            ->where(['in', 'user_id', $user_id])
            ->all();

        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }

        //find all
        $amphures = Amphures::find()
            ->where(['in', 'id', $amphurids])
            ->all();

        //add new
        foreach ($amphures as $amphur) {
            $new_route = new RogUserRoute();
            $new_route->user_id = $user_id;
            $new_route->province = $amphur->province_id;
            $new_route->amphur = $amphur->id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            $new_route->save();

            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }

    protected function findUserRoute($user_id)
    {
        if (($model = RogUserRoute::find()
            ->where(['in', 'user_id', $user_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphuresByProvinceID($provinceID)
    {
        $amphures = ArrayHelper::map(
            Amphures::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $amphures;
    }

    public function getAmphuresByProvinceName($provinceName)
    {
        $province = Provinces::find()
            ->where(['in', 'name_th', $provinceName])
            ->one();

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->where(['in', 'province_id', $province->id])
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        return $amphures;
    }

    protected function getUserPermitData($id)
    {
        if (($model = RogUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Displays a single RogUser model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $serviceroutes = ArrayHelper::map(
            RogUserRoute::find()
                ->where(['in', 'user_id', $id])
                ->orderBy(['province' => SORT_ASC])
                ->asArray()
                ->all(),
            'amphur',
            'province'
        );

        $provinces = ArrayHelper::map(
            Provinces::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );

        $amphures = ArrayHelper::map(
            Amphures::find()
                ->asArray()
                ->all(),
            'id',
            'name_th'
        );



        return $this->render('view', [
            'model' => $this->findModel($id),
            'provinces' => $provinces,
            'amphures' => $amphures,
            'serviceroutes' => $serviceroutes,


        ]);
    }

    /**
     * Creates a new RogUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = new RogUser();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->company_pic = $model->uploadMultiple($model, 'company_pic');
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }

            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RogUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->company_pic = $model->uploadMultiple($model, 'company_pic');
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }

            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateService($id)
    {
        $model = $this->findModel($id);

        $user_id = Yii::$app->user->id;
        if ($model->user_id != $user_id) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->company_pic = $model->uploadMultiple($model, 'company_pic');
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }

            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RogUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RogUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RogUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }
        if (($model = RogUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
