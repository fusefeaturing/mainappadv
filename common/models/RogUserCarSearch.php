<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogUserCar;
use common\models\RogUserRoute;
use yii\helpers\ArrayHelper;


/**
 * RogUserCarSearch represents the model behind the search form of `frontend\models\RogUserCar`.
 */
class RogUserCarSearch extends RogUserCar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'user_id', 'manufacturer'], 'integer'],
            [['carry_weight'], 'number'],
            [['type_wheel', 'car_des' ,'color', 'plate_number' ,'current_car_province', 'current_car_amphure' ,'pickup_province', 'deliver_province', 'car_type', 'chassis_number', 'engine_number', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    public $pickup_province = '';
    public $deliver_province = '';
    public $current_car_province = '';
    public $typewheel = '';
    public $cardes = '';
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    
    public function search($params)
    {
        $query = RogUserCar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['car_search_level' => SORT_ASC]],
            
            'pagination' => [
                'pageSize' => 27,
            ],
        ]);


        $this->load($params);


        $arrcarty = (isset($params['RogUserCarSearch']['car_type']))
            ? $params['RogUserCarSearch']['car_type']
            : null;
            
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_id' => $this->car_id,
            //'user_id' => $this->user_id,
            //'car_type' => $this->car_type,
            'carry_weight' => $this->carry_weight,
            'manufacturer' => $this->manufacturer,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'plate_number', $this->plate_number])
            ->andFilterWhere(['like', 'chassis_number', $this->chassis_number])
            ->andFilterWhere(['like', 'engine_number', $this->engine_number])
            
            ->andFilterWhere(['like', 'type_wheel', $this->type_wheel])
            ->andFilterWhere(['like', 'car_des', $this->car_des]);

        $arrcarty = (isset($params['RogUserCarSearch']['car_type']))
            ? $params['RogUserCarSearch']['car_type']
            : [];
        
        $typewheel = (isset($params['RogUserCarSearch']['type_wheel']))
            ? $params['RogUserCarSearch']['type_wheel']
            : [];

        $cardes = (isset($params['RogUserCarSearch']['car_des']))
            ? $params['RogUserCarSearch']['car_des']
            : [];


        $pickup_province = (isset($params['RogUserCarSearch']['pickup_province']))
            ? $params['RogUserCarSearch']['pickup_province']
            : null;

        $deliver_province = (isset($params['RogUserCarSearch']['deliver_province']))
            ? $params['RogUserCarSearch']['deliver_province']
            : null;




        if ($typewheel != null) {
            $typewheel = RogUserCar::find()
                ->where(['type_wheel' => $typewheel])
                ->select(['user_id'])
                ->distinct()
                ->all();
                 foreach ($typewheel as $currentpickprouser) {
                $curpicuid[] = $currentpickprouser->user_id;
            }
                //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($curpicuid)) {
                $query->andFilterWhere(['user_id' => $curpicuid]);
            }
        }
        
        if ($cardes != null) {
            $cardes = RogUserCar::find()
                ->where(['car_des' => $cardes])
                ->select(['user_id'])
                ->distinct()
                ->all();
                 foreach ($cardes as $cardespickprouser) {
                $curpicuid[] = $cardespickprouser->user_id;
            }
                //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($curpicuid)) {
                $query->andFilterWhere(['user_id' => $curpicuid]);
            }
        }       
            
        if ($pickup_province != null) {
            $pickprousers = RogUserRoute::find()
                ->where(['province' => $pickup_province])
                ->select(['user_id'])
                ->distinct()
                ->all();

            foreach ($pickprousers as $pickprouser) {
                $picuid[] = $pickprouser->user_id;
            }
            //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($picuid)) {
                $query->andFilterWhere(['user_id' => $picuid]);
            }
        }

        if ($deliver_province != null) {
            $delprousers = RogUserRoute::find()
                ->where(['province' => $deliver_province])
                ->select(['user_id'])
                ->distinct()
                ->all();

            foreach ($delprousers as $delprouser) {
                $deluid[] = $delprouser->user_id;
            }

            if (isset($deluid)) {
                $query->andFilterWhere(['user_id' => $deluid]);
            }
        }

        //deliver_province

        if (!empty($arrcarty)) {
            foreach ($arrcarty as $cartyp) {
                $carr[] = $cartyp;
            }

            if (isset($carr)) {
                $query->andFilterWhere(['car_type' => $carr]);
            }
        }

        //$query->andFilterWhere(['-1' => '']);

        //echo '<pre>'.var_dump($arrcarty).'</pre>'; exit();

        return $dataProvider;
    }
}
