<?php

namespace common\models;

use yii;
use \yii\web\UploadedFile;

/**
 * This is the model class for table "rog_car_type".
 *
 * @property int $car_type_id
 * @property int $index_sort
 * @property string $name
 * @property string $description
 * @property string $fee
 * @property int $wheel
 * @property string $picture
 * @property string $created_date
 * @property string $updated_date
 */
class RogCarType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_car_type';
    }

    public $upload_foler = 'uploads';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fee'], 'number'],
            [['index_sort', 'wheel', 'car_type_level', 'name', 'description'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
    
            [
                ['picture'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'png,jpg,jpeg,gif',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_type_id' => 'Car Type ID',
            'index_sort' => 'ลำดับการจัดเรียง',
            'name' => 'ประเภทรถ',
            'description' => 'ลักษณะบรรทุก',
            'fee' => 'ค่าขนส่งเฉลี่ยต่อกิโลเมตร',
            'wheel' => 'ประเภท (ล้อ)',
            'car_type_level' => 'ลำดับประเภท',
            'picture' => 'รูปภาพรถ',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    public function getTypeWheel() {
        return $this->hasOne(RogTypeWheel::className(),['type_wheel_id'=>'name']);
    }

    public function getCarDes() {
        return $this->hasOne(RogCarDescription::className(),['car_des_id'=>'description']);
    }


    public function getDefaultLevel() {

        $result = '';
        if (($this->car_type_level != null) && ($this->car_type_level != '')) {
            $resultcoll = RogCarType::find()
                ->where(['car_type_level' => $this->car_type_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->car_type_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }



    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    //images frontend
    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }


    //images backend
    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }
}
