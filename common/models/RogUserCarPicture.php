<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_user_car_picture".
 *
 * @property string $user_car_picture_id
 * @property string $user_id
 * @property string $car_id
 * @property string $picture
 * @property string $created_date
 * @property string $updated_date
 */
class RogUserCarPicture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user_car_picture';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'car_id'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['picture'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_car_picture_id' => 'User Car Picture ID',
            'user_id' => 'User ID',
            'car_id' => 'Car ID',
            'picture' => 'Picture',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
