<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_user_route".
 *
 * @property string $route_id
 * @property string $user_id
 * @property int $province
 * @property int $amphur
 * @property string $created_date
 * @property string $updated_date
 */
class RogUserRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'province', 'amphur', 'created_date'], 'required'],
            [['user_id', 'province', 'amphur'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'route_id' => 'Route ID',
            'user_id' => 'รหัสผู้ใช้งาน',
            'province' => 'จังหวัด',
            'amphur' => 'อำเภอ',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    public function getProvinces() {
        return $this->hasOne(Provinces::className(),['id'=>'province']);
    }

    public function getAmphures() {
        return $this->hasOne(Amphures::className(),['id'=>'amphur']);
    }
    
    public function getUser() {
        return $this->hasOne(RogUser::className(),['user_id'=>'user_id']);
    }
    




}
