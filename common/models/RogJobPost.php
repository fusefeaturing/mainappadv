<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Provinces;
use common\models\Districts;
use common\models\Amphures;
use common\models\RogCarType;

/**
 * This is the model class for table "rog_job_post".
 *
 * @property int $job_id
 * @property int $user_id
 * @property string $title
 * @property string $contact_person
 * @property string $contact_person_pickup
 * @property string $contact_person_delivery
 * @property string $short_description
 * @property string $full_description
 * @property string $photos
 * @property string $cartype_valid
 * @property string $type_wheel
 * @property string $car_des
 * @property string $deliver_date
 * @property string $pickup_location
 * @property string $pickup_province
 * @property string $pickup_district
 * @property string $pickup_subdistrict
 * @property string $deliver_location
 * @property string $deliver_province
 * @property string $deliver_district
 * @property string $deliver_subdistrict
 * @property string $created_date
 * @property string $updated_date
 * @property int $is_job_taken
 * @property int $is_period_job
 * @property int $is_job_close
 * @property int $is_job_appear
 */
class RogJobPost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_job_post';
    }

    public $upload_foler = 'uploads';
    public $picture = 'none.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_job_taken', 'is_period_job', 'is_job_close', 'is_job_appear', 'type_wheel', 'car_des', 'job_search_level'], 'integer'],
            [['deliver_date', 'created_date', 'updated_date'], 'safe'],
            [['title', 'contact_person', 'contact_person_pickup', 'contact_person_delivery', 'short_description', 'pickup_location', 'pickup_province', 'pickup_district', 'pickup_subdistrict', 'deliver_location', 'deliver_province', 'deliver_district', 'deliver_subdistrict'], 'string', 'max' => 255],
            [['full_description', 'cartype_valid'], 'string', 'max' => 8000],            
            [
                ['photos'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'title' => 'หัวข้อ',
            'contact_person' => 'ผู้ประสานงาน',
            'contact_person_pickup' => 'ผู้ติดต่อ จุดรับของ',
            'contact_person_delivery' => 'ผู้ติดต่อ จุดส่งของ',
            'short_description' => 'ประเภทสินค้า / รายละเอียด (ถ้ามี)',
            'full_description' => 'รายละเอียดงานขนส่ง / การขึ้น ลงสินค้า',
            'photos' => 'รูปสินค้า / งานที่ต้องการขนส่ง (เลือกได้ 5 ภาพ)',
            'cartype_valid' => 'ประเภทรถขนส่งที่ต้องการ',
            'type_wheel' => 'ประเภทรถ',
            'car_des' => 'ลักษณะประเภทรถ',
            'job_search_level' => 'ลำดับผลการค้นหา',
            'deliver_date' => 'วันที่ใช้รถขนส่ง',
            'pickup_location' => 'ชื่อสถานที่รับสินค้า',
            'pickup_province' => 'จังหวัดต้นทาง',
            'pickup_district' => 'อำเภอ/เขตต้นทาง',
            'pickup_subdistrict' => 'ตำบล/แขวงต้นทาง',

            'deliver_location' => 'ชื่อสถานที่ส่งสินค้า',
            'deliver_province' => 'จังหวัดปลายทาง',
            'deliver_district' => 'อำเภอ/เขตปลายทาง',
            'deliver_subdistrict' => 'ตำบล/แขวงปลายทาง',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',

            'is_job_taken' => 'สถานะการรับงาน',
            'is_period_job' => 'งานประจำ',
            'is_job_close' => 'สถานะการจบงาน',
            'is_job_appear' => 'การแสดงงานบนหน้าค้นหา',
        ];
    }

    public function getCarType() {
        return $this->hasOne(RogCarType::className(),['car_type_id'=>'cartype_valid']);
    }

    public static function itemsAlias($key){

        $items = [
          

      ];
      
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
      }

    public function getItemSkill(){
        return self::itemsAlias('cartype_valid');
    }


    public function cartype_text()
    {
        $separator = '<br>';

        $cartypeRefArr = ArrayHelper::map(
            RogCarType::find()->all(),
            'car_type_id',
            function ($model) {
                if ($model['name'] == $model['description']) return $model['name'];
                else return $model->typeWheel->type_wheel_name . ' - ' . $model->carDes->car_des_name;
            }
        );

        $cartypeRowArr = $this->cartypeToArray();

        //echo '<pre>'.var_dump($cartypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($cartypeRowArr)) {
            foreach ($cartypeRowArr as $cartypesingleid) {
                if (isset($cartypeRefArr[$cartypesingleid])) {
                    $txtarr[] = $cartypeRefArr[$cartypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }

    public function cartypeToArray()
    {
        return $this->cartype_valid = explode(',', $this->cartype_valid);
    }

    public function getDefaultLevel() {

        $result = '';
        if (($this->job_search_level != null) && ($this->job_search_level != '')) {
            $resultcoll = RogJobPost::find()
                ->where(['job_search_level' => $this->job_search_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->job_search_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }

    public function pickup()
    {
        $result = '';
        if (($this->pickup_province != null) && ($this->pickup_province != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->pickup_province])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->deliver_province != null) && ($this->deliver_province != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->deliver_province])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function typewheel()
    {
        $result = '';
        if (($this->type_wheel != null) && ($this->type_wheel != '')) {
            $resultcoll = RogTypeWheel::find()
                ->where(['type_wheel_id' => $this->type_wheel])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->type_wheel_name;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function cardes()
    {
        $result = '';
        if (($this->car_des != null) && ($this->car_des != '')) {
            $resultcoll = RogCarDescription::find()
                ->where(['car_des_id' => $this->car_des])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->car_des_name;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }


    public function location_text($province, $amphure, $district)
    {
        $result = '';

        if ($province != null) {
            $pv = Provinces::find()
                ->where(['id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->name_th;
            }
        }
        if ($amphure != null) {
            $am = Amphures::find()
                ->where(['id' => $amphure])
                ->one();

            if ($am) {
                $result .= ' ' . $am->name_th;
            }
        }
        if ($district != null) {
            $ds = Districts::find()
                ->where(['id' => $district])
                ->one();

            if ($ds) {
                $result .= ' ' . $ds->name_th;
            }
        }

        return $result;
    }

    public function usercontact()
    {
        $result = '';

        $userdata = RogUser::find()
            ->where(['user_id' => $this->user_id])
            ->one();

        if ($userdata) {
            $result = $userdata->tel_1;
        }

        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }




    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail  commany-display-mobile', 'style' => '']);
        }
        return $img;
    }

    public function getPhotosViewerUpdate()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img('../' . $this->getUploadUrl() . $photo, ['class' => 'img-thumbnail  commany-display-mobile', 'style' => 'width: 300px; height: 225px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'width: 180px; height: 120px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-thumbnail rounded mx-auto d-block job-display-mobile', 'style' => '']);
        if (isset($photos[0])) {
            $img = Html::img('../'.$this->getUploadUrl() . $photos[0], ['class' => 'img-thumbnail rounded mx-auto d-block job-display-mobile', 'style' => '']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img ='../'. $this->getUploadUrl() . $photos[0];
        }
        return $img;
    }

    public function getFirstPhotoURLP()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img = $this->getUploadUrl() . $photos[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($photos[0])) {
            $img = Html::img($this->getUploadUrlback() . $photos[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $photos = $this->photos ? @explode(',', $this->photos) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img = $this->getUploadUrlback() . $photos[0];
        }
        return $img;
    }

    public function getOGDesc()
    {
        $result = '';

        //$result .= $this->title . ' ';
        $result .= $this->pickup() . ' - ';
        $result .= $this->deliver();

        return $result;
    }

    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('photos') ? @explode(',', $this->getOldAttribute('photos')) : [];
    }

    public function getUser() {
        return $this->hasOne(RogUser::className(),['user_id'=>'user_id']);
    }

    public function getProvinces() {
        return $this->hasOne(Provinces::className(),['id'=>'pickup_province']);
    }
    public function getDprovinces() {
        return $this->hasOne(Provinces::className(),['id'=>'deliver_province']);
    }
    public function getTypewheel() {
        return $this->hasOne(RogTypeWheel::className(),['type_wheel_id'=>'type_wheel']);
    }
    public function getCardes() {
        return $this->hasOne(RogCarDescription::className(),['car_des_id'=>'car_des']);
    }
    
}
