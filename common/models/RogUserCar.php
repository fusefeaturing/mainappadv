<?php

namespace common\models;

use Yii;
use \yii\web\UploadedFile;
use common\models\RogUser;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "rog_user_car".
 *
 * @property string $car_id
 * @property string $user_id
 * @property int $car_type
 * @property string $type_wheel
 * @property string $car_des
 * @property int $car_search_level
 * @property string $carry_weight
 * @property string $color
 * @property string $car_services
 * @property string $car_service_tos
 * @property string $pic_front
 * @property string $pic_side
 * @property string $pic_back
 * @property string $pic_top
 * @property int $manufacturer
 * @property string $manufacturer_txt
 * @property string $plate_number
 * @property string $plate_province
 * @property string $pickup_province
 * @property string $deliver_province
 * @property string $chassis_number
 * @property string $engine_number
 * @property string $insurance_company
 * @property string $car_explanation คำอธิบายกิจการ
 * @property string $insured_amount
 * @property int $is_insured
 * @property string $created_date
 * @property string $updated_date
 */
class RogUserCar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user_car';
    }

    public $upload_foler = 'uploads';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'manufacturer', 'is_insured', 'car_type', 'car_search_level'], 'integer'],
            [['carry_weight', 'insured_amount'], 'number'],
            //[['car_des' ], 'required'],
            //[['car_services', 'car_service_tos'], 'required'],
            //[['pic_side'], 'required'],
            [['car_services', 'car_service_tos', 'car_explanation'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['type_wheel', 'car_des', 'color', 'plate_number', 'plate_province', 'chassis_number', 'engine_number', 'pickup_province', 'deliver_province', 'insurance_company', 'manufacturer_txt'], 'string', 'max' => 255],
            [
                ['pic_car'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 2,
                'extensions' => 'png,jpg,jpeg,gif',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'รหัสรถ',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'car_type' => 'ประเภทรถ',
            'type_wheel' => 'ประเภทรถ',
            'car_des' => 'ลักษณะบรรทุก',
            'car_search_level' => 'ลำดับผลการค้นหา',
            'carry_weight' => 'น้ำหนักบรรทุก (ตัน)',
            'color' => 'สี',
            'car_explanation' => 'คำอธิบายรถ',
            'car_services' => 'รายละเอียดการให้บริการ',
            'car_service_tos' => 'เงื่อนไขการให้บริการ เช่น การขึ้น/ลงสินค้า และ ราคา',
            'pic_car' => 'รูปภาพรถหน้าเฉียง หลังเฉียงของฉัน  (เลือกได้ 2 ภาพเท่านั้น)',
            'pic_front' => 'ภาพรถด้านหน้า (หน้าตรง)',
            'pic_side' => 'ภาพรถด้านหน้าเฉียง',
            'pic_back' => 'ภาพรถด้านหลังเฉียง',
            'pic_top' => 'ภาพรถด้านบน',
            'manufacturer' => 'ยี่ห้อ',
            'manufacturer_txt' => 'ยี่ห้อ',
            'plate_number' => 'ทะเบียนรถ',
            'plate_province' => 'ทะเบียนจังหวัด',
            'current_car_province' => 'รถประจำอยู่จังหวัด',
            'current_car_amphure' => 'รถประจำอยู่อำเภอ',
            'pickup_province' => 'จังหวัดต้นทาง',
            'deliver_province' => 'จังหวัดปลายทาง',
            'chassis_number' => 'เลขตัวถัง',
            'engine_number' => 'เลขเครื่องยนต์',
            'insurance_company' => 'บริษัทประกันภัย',
            'insured_amount' => 'ทุนประกัน (บาท)',
            'is_insured' => 'มีประกันภัยสินค้า',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    public function getDefaultLevel()
    {

        $result = '';
        if (($this->car_search_level != null) && ($this->car_search_level != '')) {
            $resultcoll = RogUserCar::find()
                ->where(['car_search_level' => $this->car_search_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->car_search_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
    }


    public function plate()
    {
        $result = '';
        if (($this->plate_province != null) && ($this->plate_province != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->plate_province])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function pickup()
    {
        $result = '';
        if (($this->pickup_province != null) && ($this->pickup_province != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->pickup_province])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->deliver_province != null) && ($this->deliver_province != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->deliver_province])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function typewheel()
    {
        $result = '';
        if (($this->type_wheel != null) && ($this->type_wheel != '')) {
            $resultcoll = RogTypeWheel::find()
                ->where(['type_wheel_id' => $this->type_wheel])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->type_wheel_name;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function cardes()
    {
        $result = '';
        if (($this->car_des != null) && ($this->car_des != '')) {
            $resultcoll = RogCarDescription::find()
                ->where(['car_des_id' => $this->car_des])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->car_des_name;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function location_text($province, $amphure, $district)
    {
        $result = '';

        if ($province != null) {
            $pv = Provinces::find()
                ->where(['id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->name_th;
            }
        }
        if ($amphure != null) {
            $am = Amphures::find()
                ->where(['id' => $amphure])
                ->one();

            if ($am) {
                $result .= ' ' . $am->name_th;
            }
        }
        if ($district != null) {
            $ds = Districts::find()
                ->where(['id' => $district])
                ->one();

            if ($ds) {
                $result .= ' ' . $ds->name_th;
            }
        }

        return $result;
    }
    public function location_text2($province)
    {
        $result = '';

        if ($province != null) {
            $pv = Provinces::find()
                ->where(['id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->name_th;
            }
        }
    }

    public function usercontact()
    {
        $result = '';

        $userdata = RogUser::find()
            ->where(['user_id' => $this->user_id])
            ->one();

        if ($userdata) {
            $result = $userdata->tel_1;
        }

        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();

        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }



    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();

        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('pic_car') ? @explode(',', $this->getOldAttribute('pic_car')) : [];
    }

    public function getPhotosViewer()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail  commany-display-mobile', 'style' => 'max-width: 100%; height:520px']);
        }
        return $img;
    }

    public function getPhotosViewerUpdate()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img('../' . $this->getUploadUrl() . $photo, ['class' => 'img-thumbnail commany-display-mobile', 'style' => 'width: 300px; height: 225px;']);
        }
        return $img;
    }


    public function getPhotoViewerFront()
    {
        return empty($this->pic_front) ? Yii::getAlias('../common/web') . '/img/image-default.png' : $this->getUploadUrl() . $this->pic_front;
    }

    public function getPhotoViewerSide()
    {
        return empty($this->pic_side) ? Yii::getAlias('../common/web') . '/img/image-default.png' : $this->getUploadUrl() . $this->pic_side;
    }

    public function getPhotoViewerSideUpdate()
    {
        return empty($this->pic_side) ? Yii::getAlias('../common/web') . '/img/image-default.png' : '../' . $this->getUploadUrl() . $this->pic_side;
    }

    public function getFirstPhotos()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-thumbnail rounded mx-auto d-block car-display-mobile', 'style' => 'max-width:200px;']);
        if (isset($photos[0])) {
            $img = Html::img('../' . $this->getUploadUrl() . $photos[0], ['class' => 'img-thumbnail rounded mx-auto d-block car-display-mobile', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img = '../' . $this->getUploadUrl() . $photos[0];
        }
        return $img;
    }

    public function getFirstPhotoURLP()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img = $this->getUploadUrl() . $photos[0];
        }
        return $img;
    }

    public function getPhotoViewerBack()
    {
        return empty($this->pic_back) ? Yii::getAlias('../common/web') . '/img/image-default.png' : $this->getUploadUrl() . $this->pic_back;
    }

    public function getPhotoViewerBackUpdate()
    {
        return empty($this->pic_back) ? Yii::getAlias('../common/web') . '/img/image-default.png' : '../' . $this->getUploadUrl() . $this->pic_back;
    }

    public function getPhotoViewerTop()
    {
        return empty($this->pic_top) ? Yii::getAlias('../common/web') . '/img/image-default.png' : $this->getUploadUrl() . $this->pic_top;
    }



    //images backend

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotosViewerUpdateback()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img('../' . $this->getUploadUrl() . $photo, ['class' => 'img-thumbnail', 'style' => 'width: 180px; height: 120px;']);
        }
        return $img;
    }

    public function getPhotoViewerFrontback()
    {
        return empty($this->pic_front) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrlback() . $this->pic_front;
    }

    public function getPhotoViewerSideback()
    {
        return empty($this->pic_side) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrlback() . $this->pic_side;
    }

    public function getPhotoViewerBackback()
    {
        return empty($this->pic_back) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrlback() . $this->pic_back;
    }

    public function getPhotoViewerTopback()
    {
        return empty($this->pic_top) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrlback() . $this->pic_top;
    }

    public function getFirstPhotoURLBack()
    {
        $photos = $this->pic_car ? @explode(',', $this->pic_car) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($photos[0])) {
            $img =  $this->getUploadUrlback() . $photos[0];
        }
        return $img;
    }
    //$img = Html::img('../' . $this->getUploadUrl() . $photos[0], ['class' => 'img-thumbnail rounded mx-auto d-block car-display-mobile', 'style' => 'max-width:200px;']);




    public function getUser()
    {
        return $this->hasOne(RogUser::className(), ['user_id' => 'user_id']);
    }
    public function getCartype()
    {
        return $this->hasOne(RogCarType::className(), ['car_type_id' => 'car_type']);
    }
    public function getTypewheel()
    {
        return $this->hasOne(RogTypeWheel::className(), ['type_wheel_id' => 'type_wheel']);
    }
    public function getCardes()
    {
        return $this->hasOne(RogCarDescription::className(), ['car_des_id' => 'car_des']);
    }
    public function getPprovince()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'pickup_province']);
    }
    public function getDprovince()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'deliver_province']);
    }
}
