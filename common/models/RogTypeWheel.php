<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_type_wheel".
 *
 * @property int $type_wheel_id
 * @property string $type_wheel_name
 * @property int $type_wheel_level ลำดับประเภท
 * @property string $created_date
 * @property string $updated_date
 */
class RogTypeWheel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_type_wheel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_wheel_name', 'created_date'], 'required'],
            [['type_wheel_level'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['type_wheel_name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type_wheel_id' => 'รหัสชนิดรถ',
            'type_wheel_name' => 'ชื่อชนิดรถ',
            'type_wheel_level' => 'ลำดับประเภท',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    public function getDefaultLevel() {

         $result = '';
         if (($this->type_wheel_level != null) && ($this->type_wheel_level != '')) {
             $resultcoll = RogTypeWheel::find()
                 ->where(['type_wheel_level' => $this->type_wheel_level])
                 ->one();
 
             if ($resultcoll) {
                 $result = $resultcoll->type_wheel_level;
             } else {
                 $result = '100';
             }
         } else {
             $result = '100';
         }
         return $result;
         
    }
}
