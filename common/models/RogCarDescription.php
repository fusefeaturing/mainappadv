<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_car_description".
 *
 * @property int $car_des_id รหัสรายละเอียดรถ
 * @property string $car_des_name ชื่อรายละเอียดรถ
 * @property string $type_wheel_id
 * @property string $created_date วันเวลาสร้าง
 * @property string $updated_date วันเวลาแก้ไข
 */
class RogCarDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_car_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_des_name', 'type_wheel_id', 'created_date'], 'required'],
            [['car_des_level'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['car_des_name', 'type_wheel_id'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_des_id' => 'รหัสรายละเอียดรถ',
            'car_des_name' => 'ชื่อรายละเอียดรถ',
            'car_des_level' => 'ลำดับการจัดเรียง',
            'type_wheel_id' => 'ชนิดรถ',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    public function getTypewheel() {
        return $this->hasOne(RogTypeWheel::className(),['type_wheel_id'=>'type_wheel_id']);
    }

    public function getDefaultLevel() {

        $result = '';
        if (($this->car_des_level != null) && ($this->car_des_level != '')) {
            $resultcoll = RogCarDescription::find()
                ->where(['car_des_level' => $this->car_des_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->car_des_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }
}
