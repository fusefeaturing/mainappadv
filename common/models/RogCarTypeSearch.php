<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RogCarType;

/**
 * RogCarTypeSearch represents the model behind the search form of `app\models\RogCarType`.
 */
class RogCarTypeSearch extends RogCarType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_type_id', 'index_sort', 'wheel'], 'integer'],
            [['name', 'description', 'picture', 'created_date', 'updated_date'], 'safe'],
            [['fee'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RogCarType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            
            'query' => $query,
            'sort'=> ['defaultOrder' => ['car_type_level' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_type_id' => $this->car_type_id,
            'index_sort' => $this->index_sort,
            'fee' => $this->fee,
            'wheel' => $this->wheel,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'picture', $this->picture]);

        return $dataProvider;
    }
}
