<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_rogistic_settings".
 *
 * @property int $setting_id
 * @property string $web_title
 * @property string $job_fee
 * @property string $premium_subscribe_fee
 * @property string $subscribe_fee
 * @property string $commission_fee_percentage
 * @property string $created_date
 * @property string $updated_date
 */
class RogRogisticSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_rogistic_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_fee', 'premium_subscribe_fee', 'subscribe_fee', 'commission_fee_percentage'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
            [['web_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'web_title' => 'Web Title',
            'job_fee' => 'Job Fee',
            'premium_subscribe_fee' => 'Premium Subscribe Fee',
            'subscribe_fee' => 'Subscribe Fee',
            'commission_fee_percentage' => 'Commission Fee Percentage',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
