<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "rog_user".
 *
 * @property string $user_id
 * @property string $username
 * @property string $password
 * @property string $company_name
 * @property string $company_address
 * @property string $company_pic
 * @property string $company_explanation คำอธิบายกิจการ
 * @property string $pic_idcard
 * @property string $pic_carlic
 * @property string $pic_self
 * @property string $facebook_id
 * @property string $facebook_token
 * @property string $citizen_id
 * @property string $carlic_id
 * @property string $prefix
 * @property string $name
 * @property string $surname
 * @property string $middlename
 * @property string $dob
 * @property string $email
 * @property string $email_contact
 * @property string $website
 * @property string $line_id
 * @property string $tel_1
 * @property string $tel_2
 * @property int $is_user_findcar
 * @property int $is_user_car_owner
 * @property int $is_user_verified
 * @property string $created_date
 * @property string $updated_date
 */
class RogUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user';
    }

    public $upload_foler = 'uploads';
    public $picture = 'image-default.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['company_address'], 'required'], 
            [['company_address', 'company_explanation'], 'string'],
            [['province_id', 'amphure_id', 'district_id', 'zipcode_id', 'facebook_id', 'is_user_findcar', 'is_user_car_owner', 'is_user_verified'], 'integer'],
            [['dob', 'created_date', 'updated_date'], 'safe'],
            [['username'], 'string', 'max' => 50],
            [['password', 'company_name', 'pic_idcard', 'pic_carlic', 'pic_self', 'facebook_token', 'prefix', 'name', 'surname', 'middlename', 'email', 'email_contact', 'website', 'line_id', 'tel_1', 'tel_2'], 'string', 'max' => 255],
            [['citizen_id', 'carlic_id'], 'string', 'max' => 25],

            [
                ['company_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'ชื่อผู้ใช้งาน',
            'password' => 'รหัสผ่าน',
            'company_name' => 'ชื่อกิจการ',
            'company_address' => 'ที่อยู่กิจการ',
            'company_pic' => 'ภาพกิจการ (1 - 4 ภาพ)',
            'company_explanation' => 'คำอธิบายกิจการ',
            'province_id' => 'จังหวัดที่ตั้งกิจการ',
            'amphure_id' => 'อำเภอที่ตั้งกิจการ',
            'district_id' => 'ตำบลตั้งกิจการ',
            'zipcode_id' => 'รหัสไปรษณีย์',
            'pic_idcard' => 'ภาพบัตรประชาชน',
            'pic_carlic' => 'ภาพใบขับขี่',
            'pic_self' => 'ภาพถ่ายหน้าตรงถือใบขับขี่',
            'facebook_id' => 'Facebook ID',
            'facebook_token' => 'Facebook ',
            'citizen_id' => 'เลขประจำตัวประชาชน',
            'carlic_id' => 'เลขใบขับขี่',
            'prefix' => 'คำนำหน้า',
            'name' => 'ชื่อ',
            'surname' => 'นามสกุล',
            'middlename' => 'ชื่อกลาง / ชื่อเล่น',
            'dob' => 'วันเกิด',
            'email' => 'Email',
            'email_contact' => 'Email',
            'website' => 'Website',
            'line_id' => 'Line ID',
            'tel_1' => 'เบอร์ติดต่อ 1',
            'tel_2' => 'เบอร์ติดต่อ 2',
            'is_user_findcar' => 'ใช้บริการค้นหารถขนส่ง',
            'is_user_car_owner' => 'ให้บริการรถขนส่งของฉัน',
            'is_user_verified' => 'ยืนยันตัวตน',
            'created_date' => 'วันเวลาที่เริ่มใช้งาน',
            'updated_date' => 'วันเวลาที่แก้ไขข้อมูล',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user = self::findOne(['user_id' => $id]);
        if ($user) {
            //return new static($user->user_id);
            return $user;
        } else return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = self::findOne(['facebook_id' => $token]);
        if ($user) {
            return new static($user);
        } else return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->facebook_id;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->facebook_id === $authKey;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this;
    }


    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function tel_1()
    {
        $result = '';
        if (($this->tel_1 != null) && ($this->tel_1 != '')) {
            $resultcoll = RogUser::find()
                ->where(['user_id' => $this->user_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->tel_1;
            } else {
                $result = 'กรุณาระบุเบอร์โทรศัพท์';
            }
        } else {
            $result = 'กรุณาระบุเบอร์โทรศัพท์';
        }
        return $result;
    }

    public function line_id()
    {
        $result = '';
        if (($this->line_id != null) && ($this->line_id != '')) {
            $resultcoll = RogUser::find()
                ->where(['user_id' => $this->user_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->line_id;
            } else {
                $result = 'กรุณาระบุ Line ID';
            }
        } else {
            $result = 'กรุณาระบุ Line ID';
        }
        return $result;
    }

    public function pickup()
    {
        $result = '';
        if (($this->province_id != null) && ($this->province_id != '')) {
            $resultcoll = Provinces::find()
                ->where(['id' => $this->province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->amphure_id != null) && ($this->amphure_id != '')) {
            $resultcoll = Amphures::find()
                ->where(['id' => $this->amphure_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function district()
    {
        $result = '';
        if (($this->district_id != null) && ($this->district_id != '')) {
            $resultcoll = Districts::find()
                ->where(['id' => $this->district_id])

                ->one();

            if ($resultcoll) {
                $result = $resultcoll->name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function zipcode()
    {
        $result = '';
        if (($this->district_id != null) && ($this->district_id != '')) {
            $resultcoll = Districts::find()
                ->where(['id' => $this->district_id])

                ->one();

            if ($resultcoll) {
                $result = $resultcoll->zip_code;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function usercontact()
    {
        $result = '';

        if (($this->user_id != null) && ($this->user_id != '')) {
            $userdata = RogUser::find()
                ->where(['user_id' => $this->user_id])
                ->one();

            if ($userdata) {
                $result = $userdata->tel_1;
            } else {
                $result = 'กรุณาระบุเบอร์โทรศัพท์ ';
            }
        } else {
            $result = 'กรุณาระบุเบอร์โทรศัพท์ ';
        }



        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerFront()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerFrontback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/image-default.png' : $this->getUploadUrlback() . $this->picture;
    }

    public function uploadMultiple($model, $attribute)
    {
        $company_pic  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $company_pic !== null) {
            $filenames = [];
            foreach ($company_pic as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewerFront()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = '';
        foreach ($company_pic as $pic) {
            $img .= '' . Html::img($this->getUploadUrl() . $pic, ['class' => 'img-thumbnail', 'style' => 'width:345px; height:250px;']);
        }
        return $img;
    }

    public function getPhotosViewerFrontDetail()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = '';
        foreach ($company_pic as $pic) {
            $img .= '' . Html::img('../' . $this->getUploadUrl() . $pic, ['class' => 'img-thumbnail', 'style' => 'width:345px; height:250px;']);
        }
        return $img;
    }

    public function getPhotosViewerFrontback()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = '';
        foreach ($company_pic as $pic) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $pic, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/image-default.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($company_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $company_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/image-default.png';
        if (isset($company_pic[0])) {
            $img = $this->getUploadUrl() . $company_pic[0];
        }
        return $img;
    }

    public function getFirstPhotosback()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/image-default.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($company_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $company_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $company_pic = $this->company_pic ? @explode(',', $this->company_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/image-default.png';
        if (isset($company_pic[0])) {
            $img = $this->getUploadUrlback() . $company_pic[0];
        }
        return $img;
    }

    public function getOGDesc()
    {
        $result = '';

        //$result .= $this->title . ' ';
        $result .= $this->pickup() . ' - ';
        $result .= $this->deliver();

        return $result;
    }

    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('company_pic') ? @explode(',', $this->getOldAttribute('company_pic')) : [];
    }

    public function getProvince()
    {
        return $this->hasOne(Provinces::className(), ['id' => 'province_id']);
    }
    public function getAmphure()
    {
        return $this->hasOne(Amphures::className(), ['id' => 'amphure_id']);
    }
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['id' => 'district_id']);
    }
    public function getZipcode()
    {
        return $this->hasOne(Districts::className(), ['id' => 'zipcode_id']);
    }
}
