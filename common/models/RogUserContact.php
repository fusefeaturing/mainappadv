<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_user_contact".
 *
 * @property string $contact_id
 * @property string $user_id
 * @property int $contact_type
 * @property string $contact_value
 * @property int $contactable
 * @property string $created_date
 * @property string $updated_date
 */
class RogUserContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'contact_type', 'contactable'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['contact_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'user_id' => 'User ID',
            'contact_type' => 'Contact Type',
            'contact_value' => 'Contact Value',
            'contactable' => 'Contactable',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
