<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rog_user_address".
 *
 * @property string $address_id
 * @property string $user_id
 * @property int $address_type
 * @property string $address
 * @property string $mu
 * @property string $road
 * @property string $subdistrict
 * @property string $district
 * @property string $province
 * @property string $postalcode
 * @property string $country
 * @property string $tel
 * @property string $created_date
 * @property string $updated_date
 */
class RogUserAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rog_user_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'address_type'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['address', 'mu', 'road', 'subdistrict', 'district', 'province', 'country'], 'string', 'max' => 255],
            [['postalcode'], 'string', 'max' => 10],
            [['tel'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'address_id' => 'Address ID',
            'user_id' => 'User ID',
            'address_type' => 'ประเภทที่อยู่',
            'address' => 'ที่อยู่',
            'mu' => 'หมู่',
            'road' => 'ถนน',
            'subdistrict' => 'ตำบล/แขวง',
            'district' => 'อำเภอ/เขต',
            'province' => 'จังหวัด',
            'postalcode' => 'รหัสไปรษณีย์',
            'country' => 'ประเทศ',
            'tel' => 'โทรศัพท์',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }
}
