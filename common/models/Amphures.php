<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "amphures".
 *
 * @property int $id
 * @property string $code
 * @property string $name_th
 * @property string $name_en
 * @property int $province_id
 */
class Amphures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amphures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name_th', 'name_en'], 'required'],
            [['province_id'], 'integer'],
            [['code'], 'string', 'max' => 4],
            [['name_th', 'name_en'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name_th' => 'ชื่ออำเภอภาษาไทย',
            'name_en' => 'ชื่ออำเภอภาษาอังกฤษ',
            'province_id' => 'ชื่อจังหวัด',
        ];
    }
    public function getProvinces() {
        return $this->hasOne(Provinces::className(),['id'=>'province_id']);
    }
}
